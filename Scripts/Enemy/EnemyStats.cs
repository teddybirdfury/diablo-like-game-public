﻿using FJL;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
    {
        public enum STATS : int
        {
            HEALTH = 0,
            EXPERIENCE,
            ATTACK,
            DEFENSE,
            LEVEL,
        }

        public string m_name = "";

        [SerializeField]
        public List<Stats> m_baseStats = new List<Stats>();

        [SerializeField]
        public List<Stats> m_currentStats = new List<Stats>();

        void Start()
        {
            InitializeStats();
        }

        public void InitializeStats()
        {
            foreach(Stats _stats in m_baseStats)
            {
                Stats.SetStatByName(m_currentStats, _stats.m_statName, _stats.m_statValue);
            }
        }

        private void Dead(GameObject _player)
        {
            EnemyMovement m_enemyMovement = GetComponent<EnemyMovement>();
            if (m_enemyMovement != null)
            {
                m_enemyMovement.m_isMove = false;
            }
            //For now the player handles all the rewards
            GameObject.FindGameObjectWithTag(StringConstants.PLAYER).GetComponent<PlayerActions>().EnemyKilled(transform, m_baseStats);
        }

        public void TakeDamage(GameObject _player, float _damage)
        {
            float m_health = Stats.GetStatValueByName(m_currentStats, STATS.HEALTH.ToString());
            float m_finalValue = m_health - _damage;
            float m_maxHealth = Stats.GetStatValueByName(m_baseStats, STATS.HEALTH.ToString());
            Stats.SetStatByName(m_currentStats, STATS.HEALTH.ToString(), m_finalValue);
            InGameUIController.Instance.UpdateEnemyHealth(m_maxHealth, m_finalValue);
            if (m_finalValue <= 0)
            {
                Dead(_player);
            }
        }
    }
