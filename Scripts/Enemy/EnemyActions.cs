﻿using FJL;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActions : MonoBehaviour {

    private Transform m_target;
    public Inventory m_inventory;
    private EnemyStats m_enemyStats;
    private EnemyMovement m_enemyMovement;

    private float m_distanceToInteract = 1f;

    private bool m_isAttackOnCooldown = false;

    void Start()
    {
        m_target = GameObject.FindGameObjectWithTag(StringConstants.PLAYER).transform; //for now only player
        m_enemyStats = GetComponent<EnemyStats>();
        m_enemyMovement = GetComponent<EnemyMovement>();
        m_enemyMovement.m_target = m_target.gameObject;
    }

    void Update()
    {
        if(Utility.IsDistanceClose(transform.position, m_target.position, m_distanceToInteract))
        {
            m_enemyMovement.m_enableMovement = false;
            if (!m_isAttackOnCooldown)
            {
                StartCoroutine(Attack());
            }
        } else
        {
            m_enemyMovement.m_enableMovement = true;
        }
    }

    private IEnumerator Attack()
    {
        m_isAttackOnCooldown = true;
        yield return new WaitForSeconds(0.3f);
        float m_attackDamage = Stats.GetStatValueByName(m_enemyStats.m_currentStats, EnemyStats.STATS.ATTACK.ToString());
        m_target.SendMessage(CharacterActions.ADD_TAKE_DAMAGE, m_attackDamage);
        yield return new WaitForSeconds(1.1f);
        m_isAttackOnCooldown = false;
    }

    public void TargetOnUI()
    {
        InGameUIController.Instance.SetEnemyAttributes(m_enemyStats.m_name);
        float m_maxHealth = Stats.GetStatValueByName(m_enemyStats.m_baseStats, EnemyStats.STATS.HEALTH.ToString());
        float m_health = Stats.GetStatValueByName(m_enemyStats.m_currentStats, EnemyStats.STATS.HEALTH.ToString());
        InGameUIController.Instance.UpdateEnemyHealth(m_maxHealth, m_health);
    }

    public void DropLootable()
    {

    }

}
