﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using FJL;

public class CharacterActions : MonoBehaviour
{

    protected CharacterAttributes m_characterAttributes;
    protected CharacterBehaviour m_characterBehaviour;
    public CharacterStats m_characterStats;
    public Inventory m_inventory;

    public bool m_isSurvivalMode = true;
    private bool m_isDead = false;

    public delegate void PlayerHealthHandler();

    public event PlayerHealthHandler PlayerHealthHandle;

    #region message functions names
    public const string ADD_TAKE_DAMAGE = "TakeOrAddDamage";
    #endregion

    public void PlayerHealthChange()
    {
        if (PlayerHealthHandle != null)
        {
            PlayerHealthHandle.Invoke();
        }
    }

    public virtual void Start()
    {
        Initialize();
    }

    public virtual void Initialize()
    {
        m_characterBehaviour = GetComponent<CharacterBehaviour>();
        m_characterStats = new CharacterStats(this);
        m_inventory = new Inventory(this);
        m_characterStats.InitializePlayerStats();
        InitializePlayerAttributes();
        InitializeHandlers();
    }


    private void InitializeHandlers()
    {
        m_characterBehaviour.InteractiveItemHandle += () => {
            ItemHandle();
        };

        m_characterBehaviour.InteractiveNPCHandle += () => {
            NpcHandle();
        };

        m_characterBehaviour.InteractiveEnemyHandle += () => {
            AttackHandle();
        };

        m_characterBehaviour.InteractiveLootHandle += () => {
            LootHandle();
        };

        m_characterBehaviour.InteractiveStructureHandle += () => {
            StructureHandle();
        };
    }

    public void Attack()
    {
        m_characterBehaviour.Attack();
    }

    private void AttackHandle()
    {
        Transform m_enemy = m_characterBehaviour.m_currentlyInteracted;
        EnemyStats m_stats = m_enemy.GetComponent<EnemyStats>();
        float m_attack = Stats.GetStatValueByName(m_characterStats.m_finalStats, PlayerStats.STATS.ATTACK.ToString());
        m_characterBehaviour.m_currentlyInteracted.GetComponent<EnemyStats>().TakeDamage(gameObject, m_attack);
    }

    public void ItemHandle()
    {
        Transform m_item = m_characterBehaviour.m_currentlyInteracted;
        InventoryItem m_inventoryItem = m_item.GetComponent<Item>().GetItem();
        AddInventoryItem(m_inventoryItem);
        m_characterBehaviour.RemoveInteractiveFromList(m_item);
        GameObject.Destroy(m_item.gameObject);
    }

    public void NpcHandle()
    {
        Transform m_npc = m_characterBehaviour.m_currentlyInteracted;
        NPC m_npcScript = m_npc.GetComponent<NPC>();
        m_npcScript.StartDialogue();
    }

    public void LootHandle()
    {
        Transform m_item = m_characterBehaviour.m_currentlyInteracted;
        Loot m_loot = m_item.GetComponent<Loot>();
        m_loot.m_inventory.SetPlayer(this);
        InventoryUIController.Instance.DisableUI();
        LootUIController.Instance.Reinitialize(m_loot.m_inventory);
        BaseUIController.Instance.EnableMenu(5);
    }

    public void StructureHandle()
    {
        InteractiveStructure m_structure = m_characterBehaviour.m_currentlyInteracted.GetComponent<InteractiveStructure>();
        if (m_structure.m_hasConvertResource)
        {
            BaseUIController.Instance.EnableMenu(6);
            ResourceConverterUIController.Instance.Reinitialize(m_structure.m_upgradeableStructure);
        }
        else if (m_structure.m_hasUpgrade)
        {
            BaseUIController.Instance.EnableMenu(7);
            StructureUpgradeUIController.Instance.Reinitialize(m_structure.m_upgradeableStructure);
        }
    }

    public virtual void EnemyKilled(Transform _enemy, List<Stats> _enemyStats)
    {
        //Get rewards from enemy via enemyStats
        RemoveInteractiveFromList(_enemy);
        GameObject.Destroy(_enemy.gameObject);
    }

    public void RemoveInteractiveFromList(Transform _interactive)
    {
        m_characterBehaviour.RemoveInteractiveFromList(_interactive);
    }

    public virtual void InitializePlayerAttributes()
    {
        m_characterAttributes = new CharacterAttributes("Shadow");
        InventoryUIController.Instance.SetName(m_characterAttributes.m_playerName);
    }

    public virtual void Interact()
    {
        m_characterBehaviour.Interact();
    }

    public virtual void AddInventoryItem(InventoryItem _item)
    {
        m_inventory.AddInventoryItem(_item);
    }

    public virtual void TakeOrAddDamage(int _damage)
    {
        TakeOrAddDamage(_damage, false);
    }

    public void TakeOrAddDamage(int _damage, bool _isHeal = false)
    {
        if (_isHeal)
        {
            _damage = -_damage;
        }
        float m_currentHealth = m_characterStats.GetStatValue(m_characterStats.m_currentStats, PlayerStats.STATS.HEALTH);
        float m_finalHealth = m_currentHealth - _damage;

        if (m_currentHealth <= 0)
        {
            InGameUIController.Instance.ToggleDeathUI(true);
            PlayerHealthHandle();
            m_isDead = true;
            return;
        }
        m_characterStats.SetStats(m_characterStats.m_currentStats, PlayerStats.STATS.HEALTH, (int)m_finalHealth);
        PlayerHealthHandle();
    }

    public void UseItem(InventoryItem _item)
    {
        m_characterStats.UseItem(_item);
    }

   
}
