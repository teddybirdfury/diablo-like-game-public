﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DynamicUI : MonoBehaviour {

    //Used for Blueprint UI like template - Scrollable Y
    protected void AdjustContentPanelSize(RectTransform _contentRect, RectTransform _prefab, int _rows, int _minRows, float _offsetYPerPrefab)
    {
        if (_rows >= _minRows)
        {
            RectTransform m_contentRect = _contentRect.GetComponent<RectTransform>();
            float m_prefabSize = _prefab.sizeDelta.y;
            float m_adjustedHeight = (_rows * m_prefabSize) + (_offsetYPerPrefab * _rows);
            m_contentRect.sizeDelta = new Vector2(m_contentRect.sizeDelta.x, m_adjustedHeight);
            m_contentRect.position = new Vector2(m_contentRect.position.x, m_contentRect.position.y - m_adjustedHeight);
        }
    }

    //Used for Build UI like template - Scrollable X - 1 row
    protected void AdjustContentPanelSize(RectTransform _contentRect, RectTransform _prefab, int _itemCount, float _offsetXPerPrefab)
    {
        RectTransform m_contentRect = _contentRect.GetComponent<RectTransform>();
        float m_prefabSize = _prefab.sizeDelta.x;
        float m_adjustedWidth = ((m_prefabSize + _offsetXPerPrefab) * _itemCount) + _offsetXPerPrefab;
        m_contentRect.sizeDelta = new Vector2(m_adjustedWidth, m_contentRect.sizeDelta.y);
        m_contentRect.position = new Vector2(m_adjustedWidth, m_contentRect.position.y);
    }

    protected Transform GetComponentInChildren(Transform _transform)
    {
        Image[] m_images = _transform.GetComponentsInChildren<Image>();
        for (int i = 0; i < m_images.Length; i++)
        {
            if (m_images[i].transform != _transform.transform)
            {
                return m_images[i].transform;
            }

        }
        throw new UnityException("Cannot find any Image in transform");
    }


}
