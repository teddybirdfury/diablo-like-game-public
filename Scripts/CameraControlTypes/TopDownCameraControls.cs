﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownCameraControls : MonoBehaviour {
    
    public float dragSpeed = 6f;

    float horizontalMovement = 0;
    float verticalMovement = 0;

    void Start()
    {
        transform.eulerAngles = new Vector3(60, 0, 0);
    }

    void Update () {
         if (Input.GetMouseButton(1))
         {
            horizontalMovement = -Input.GetAxis("Mouse X") * Time.deltaTime * dragSpeed;
            verticalMovement = -Input.GetAxis("Mouse Y") * Time.deltaTime * dragSpeed;
            transform.Translate(horizontalMovement, 0, verticalMovement, Space.World);
         }
    }

  
}
