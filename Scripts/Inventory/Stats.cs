﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FJL{ 

    [Serializable]
    public class Stats
    {
        public string m_statName = "";
        public float m_statValue = 0;

        public Stats() { }

        public Stats(string _name)
        {
            m_statName = _name;
            m_statValue = 0;
        }

        public Stats(string _name, float _value)
        {
            m_statName = _name;
            m_statValue = _value;
        }

        public Stats(Stats _stats)
        {
            m_statName = _stats.m_statName;
            m_statValue = _stats.m_statValue;
        }

        public static Stats GetStatByName(List<Stats> _list, string _name)
        {
            foreach(Stats _stats in _list)
            {
                if(_stats.m_statName == _name)
                {
                    return _stats;
                }
            }
            return null;
        }

        public static void SetStatByName(List<Stats> _list, string _name, float _value)
        {
            for(int i = 0; i < _list.Count; i++)
            {
                if (_list[i].m_statName == _name)
                {
                    _list[i].m_statValue = _value;
                    return;
                }
            }
        }

        public static float GetStatValueByName(List<Stats> _list, string _name)
        {
            return GetStatByName(_list, _name).m_statValue;
        }
    }
}
