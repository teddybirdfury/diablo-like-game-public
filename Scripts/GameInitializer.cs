﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameInitializer : MonoBehaviour {

    public static GameInitializer Instance;
    
    public InventoryItem[] m_testItems;

    public List<GameObject> m_resources = new List<GameObject>();
    public List<GameObject> m_enemies = new List<GameObject>();

    public bool m_isSpawn = false;

    public Transform m_spawnArea;

    void Awake()
    {
        Application.targetFrameRate = 80;
        Instance = this;
    }

	void Start () {
        if (SceneManager.GetActiveScene().name == "LocalLevel" && m_isSpawn)
        {
            StartCoroutine(Spawn());
        }
    }

    IEnumerator Spawn()
    {
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < 5; i++)
        {
           // Vector3 m_pos = new Vector3(Random.Range(-m_spawnArea.localScale.x / 2, m_spawnArea.localScale.x / 2), 0, Random.Range(-m_spawnArea.localScale.z / 2, m_spawnArea.localScale.z / 2));
            GameObject m_resource = GameObject.Instantiate(m_resources[0]);
            m_resource.transform.position = getRandomPositionSpawn();

           // m_pos = new Vector3(Random.Range(-m_spawnArea.localScale.x / 2, m_spawnArea.localScale.x / 2), 0, Random.Range(-m_spawnArea.localScale.z / 2, m_spawnArea.localScale.z / 2));
            GameObject m_enemy = GameObject.Instantiate(m_enemies[0]);
            m_enemy.transform.position = getRandomPositionSpawn();
        }
    }

    public Vector3 getRandomPositionSpawn()
    {
        Mesh planeMesh = m_spawnArea.GetComponent<MeshFilter>().mesh;
        Bounds bounds = m_spawnArea.GetComponent<Renderer>().bounds;
        float minX = m_spawnArea.transform.position.x - (bounds.size.x * 0.5f);
        float maxX = m_spawnArea.transform.position.x + (bounds.size.x * 0.5f);
        float minZ = m_spawnArea.transform.position.z - (bounds.size.z * 0.5f);
        float maxZ = m_spawnArea.transform.position.z + (bounds.size.z * 0.5f);
        Vector3 newVec = new Vector3(Random.Range(minX, maxX),
                                 0,
                                 Random.Range(minZ, maxZ));
        return newVec;
    }
}
