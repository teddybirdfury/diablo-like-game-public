﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectArea : BaseInteractiveDetectArea {

    [HideInInspector]
    public bool m_hasTargetChanged = false;
    private Transform m_lastTarget;

    public override void OnTriggerEnter(Collider _collider)
    {
        Transform m_detectedInteractive = _collider.transform;
       // Debug.Log("enemy : " + m_detectedInteractive.parent.name + " Tag: " + m_detectedInteractive.tag + " type: " );
        if (m_detectedInteractive.tag == StringConstants.INTERACTIVE
            && m_detectedInteractive.GetComponentInParent<InteractiveObject>().m_type == InteractiveObject.InteractiveTypes.ENEMY)
        {
            if (!m_detectedInteractives.Contains(m_detectedInteractive))
            {
                m_detectedInteractives.Add(m_detectedInteractive);
                m_closestDetectedInteractive = base.GetClosesDetectedObject(m_detectedInteractive);
                InteractiveDetected();
            }
        }
    }

    public override void OnTriggerStay(Collider _collider)
    {
        Transform m_transform = _collider.transform;
        if ((m_closestDetectedInteractive != null)|| (m_isChangeTarget && m_detectedInteractives.Count > 0))
        {
            m_isChangeTarget = false;
            m_closestDetectedInteractive = GetClosesDetectedObject();
            if(m_lastTarget != m_closestDetectedInteractive)
            {
                m_hasTargetChanged = true;
            } else
            {
                m_hasTargetChanged = false;
            }
            m_lastTarget = m_closestDetectedInteractive;
            InteractiveDetected();
           
        }
        else if (m_closestDetectedInteractive == null 
            && m_transform.tag == StringConstants.INTERACTIVE
            && m_transform.GetComponentInParent<InteractiveObject>().m_type == InteractiveObject.InteractiveTypes.ENEMY)
        {
            m_hasTargetChanged = true;
            m_lastTarget = _collider.transform;
            m_closestDetectedInteractive = _collider.transform;
            InteractiveDetected();
        }
    }

    public override void RemoveDetectedFromList(Transform _transform)
    {
        m_detectedInteractives.Remove(_transform);
        m_closestDetectedInteractive = GetClosesDetectedObject();
        if(m_closestDetectedInteractive == null)
        {
            InGameUIController.Instance.ToggleEnemyPanel(false);
        }
        m_isChangeTarget = true;
    }
}
