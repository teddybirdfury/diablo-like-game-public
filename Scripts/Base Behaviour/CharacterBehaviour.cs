﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class CharacterBehaviour : MonoBehaviour
{

    protected EnemyDetectArea m_enemyDetectArea;
    protected InteractiveDetectArea m_interactiveDetectArea;
    protected CharacterMovement m_characterMovement;

    public Vector3 m_targetInteractivePosition = Vector3.zero;
    public Vector3 m_targetEnemyPosition = Vector3.zero;
    public Transform m_currentlyInteracted;

    public bool m_enableControls = true;

    protected float m_distanceToInteract = 1f;
    protected bool m_isAttack = false;
    protected bool m_isInteractAction = false;
    protected bool m_attackOnCooldown = false;

    #region Handlers
    public delegate void InteractItemHandler();

    public event InteractItemHandler InteractiveItemHandle;

    public void InteractItem()
    {
        if (InteractiveItemHandle != null)
        {
            InteractiveItemHandle.Invoke();
        }
    }

    public delegate void InteractNPCHandler();

    public event InteractNPCHandler InteractiveNPCHandle;

    public void InteractNPC()
    {
        if (InteractiveNPCHandle != null)
        {
            InteractiveNPCHandle.Invoke();
        }
    }

    public delegate void EnemyHandler();

    public event EnemyHandler InteractiveEnemyHandle;

    public void InteractEnemy()
    {
        if (InteractiveEnemyHandle != null)
        {
            InteractiveEnemyHandle.Invoke();
        }
    }

    public delegate void LootHandler();

    public event LootHandler InteractiveLootHandle;

    public void InteractLoot()
    {
        if (InteractiveLootHandle != null)
        {
            InteractiveLootHandle.Invoke();
        }
    }

    public delegate void StructureHandler();

    public event StructureHandler InteractiveStructureHandle;

    public void InteractStructure()
    {
        if (InteractiveStructureHandle != null)
        {
            InteractiveStructureHandle.Invoke();
        }
    }


    #endregion

    public virtual void Start()
    {
        m_characterMovement = GetComponent<CharacterMovement>();
        m_interactiveDetectArea = GetComponentInChildren<InteractiveDetectArea>();
        m_enemyDetectArea = GetComponentInChildren<EnemyDetectArea>();

        InteractiveDetection();
        EnemyDetection();
    }

    protected virtual void InteractiveDetection()
    {
        if (m_interactiveDetectArea != null)
        {
            m_interactiveDetectArea.InteractiveDetectedHandle += () =>
            {
                if (m_interactiveDetectArea.m_closestDetectedInteractive != null)
                {
                    m_targetInteractivePosition = m_interactiveDetectArea.m_closestDetectedInteractive.transform.position;
                }
            };
        }
    }

    public virtual void EnemyDetection()
    {
        if (m_enemyDetectArea != null)
        {
            m_enemyDetectArea.InteractiveDetectedHandle += () =>
            {
                if (m_enemyDetectArea.m_closestDetectedInteractive != null)
                {
                    m_targetEnemyPosition = m_enemyDetectArea.m_closestDetectedInteractive.transform.position;
                    if (m_enemyDetectArea.m_hasTargetChanged)
                    {
                        SetEnemyOnUI(m_enemyDetectArea.m_closestDetectedInteractive.transform);
                    }
                }
            };
        }
    }

    public virtual void Update()
    {
        if (!m_enableControls)
        {
            return;
        }

        if (m_isInteractAction)
        {
            if (m_isAttack)
            {
                InteractAction(m_targetEnemyPosition, m_enemyDetectArea.m_closestDetectedInteractive);
            }
            else
            {
                InteractAction(m_targetInteractivePosition, m_interactiveDetectArea.m_closestDetectedInteractive);
            }
        }
    }

    protected void SetEnemyOnUI(Transform _enemy)
    {
        _enemy = m_enemyDetectArea.m_closestDetectedInteractive;
        _enemy.GetComponent<EnemyActions>().TargetOnUI();
    }

    protected void InteractProcess()
    {
        if (m_currentlyInteracted == null)
        {
            return;
        }

        InteractiveObject m_currentlyInteractedObj = m_currentlyInteracted.GetComponent<InteractiveObject>();
        if (m_currentlyInteractedObj.m_type == InteractiveObject.InteractiveTypes.ITEM)
        {
            InteractItem();
        }
        else if (m_currentlyInteractedObj.m_type == InteractiveObject.InteractiveTypes.NPC)
        {
            InteractNPC();
        }
        else if (m_currentlyInteractedObj.m_type == InteractiveObject.InteractiveTypes.ENEMY)
        {
            InteractEnemy();
        }
        else if (m_currentlyInteractedObj.m_type == InteractiveObject.InteractiveTypes.LOOT)
        {
            InteractLoot();
        }
        else if (m_currentlyInteractedObj.m_type == InteractiveObject.InteractiveTypes.STRUCTURE)
        {
            InteractStructure();
        }
        m_currentlyInteracted = null;
    }

    public void RemoveInteractiveFromList(Transform _transform)
    {
        InteractiveObject m_currentlyInteractedObj = _transform.GetComponentInParent<InteractiveObject>();
        if (m_currentlyInteractedObj.m_type == InteractiveObject.InteractiveTypes.ITEM)
        {
            m_interactiveDetectArea.RemoveDetectedFromList(_transform);
            if (m_interactiveDetectArea.m_detectedInteractives.Count == 0)
            {
                m_targetInteractivePosition = Vector3.zero;
            }
        }
        else if (m_currentlyInteractedObj.m_type == InteractiveObject.InteractiveTypes.ENEMY)
        {
            m_enemyDetectArea.RemoveDetectedFromList(_transform);
            if (m_enemyDetectArea.m_detectedInteractives.Count == 0)
            {
                m_targetEnemyPosition = Vector3.zero;
            }

        }
    }

    private void InteractAction(Vector3 _positionToCheck, Transform _currentInteracted)
    {
        if (_positionToCheck != Vector3.zero
            && Utility.IsDistanceClose(transform.position, _positionToCheck, m_distanceToInteract))
        {
            m_isInteractAction = false;
            m_currentlyInteracted = _currentInteracted;
            ToggleMovement(false);
            InteractProcess();
        }
        else
        {
            ToggleMovement(true);
        }
    }

    private void ToggleMovement(bool _isEnable)
    {
        m_characterMovement.m_isMove = _isEnable;
    }

    public void Interact()
    {
        if (m_targetInteractivePosition != Vector3.zero)
        {
            m_isAttack = false;
            m_characterMovement.LookTowards(m_targetInteractivePosition);
            m_isInteractAction = true;
        }
    }

    public virtual void Attack()
    {
        if (m_targetEnemyPosition != Vector3.zero && !m_attackOnCooldown)
        {
            m_attackOnCooldown = true;
            m_isAttack = true;
            m_characterMovement.LookTowards(m_targetEnemyPosition);
            m_isInteractAction = true;
            StartCoroutine(AttackCooldown());

        }
    }

    protected IEnumerator AttackCooldown()
    {
        yield return new WaitForSeconds(0.1f);
        m_attackOnCooldown = false;
    }

    public void AttackLongRange()
    {
        m_currentlyInteracted = m_enemyDetectArea.m_closestDetectedInteractive.parent;
        InteractProcess();
    }


}
