﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalMapUIController : MonoBehaviour {

    public static GlobalMapUIController Instance;

    private GlobalMapControls m_globalMapControls;
    private RectTransform m_canvasRect;
    public Text m_driveTravelTime;
    public Text m_runTravelTime;
    public Text m_walkTravelTime;
    public Text m_currentTravelTime;
    public GameObject m_enterButton;
    public GameObject m_movePanel;

    private Vector2 m_enterButtonOffset = new Vector3(0, -80, 0);
    private Vector2 m_screenPoint = Vector3.zero;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        m_globalMapControls = GameObject.FindGameObjectWithTag("GameController").GetComponent<GlobalMapControls>();
        m_canvasRect = GetComponent<RectTransform>();
    }

    void Update()
    {
        m_screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, m_globalMapControls.m_currentAreaObject.transform.position);
        m_enterButton.GetComponent<RectTransform>().anchoredPosition = m_screenPoint - (m_canvasRect.sizeDelta / 2) + m_enterButtonOffset;
    }

    public void ToggleEnterButton(bool _isEnable)
    {
        m_enterButton.SetActive(_isEnable);
    }

    public void ToggleMovePanel(bool _isEnable)
    {
        m_movePanel.SetActive(_isEnable);
    }

    public void Enter()
    {
        m_globalMapControls.LoadLocalMap();
    }

    public void Walk()
    {
        m_movePanel.SetActive(false);
        m_globalMapControls.MoveToMap();
    }
}
