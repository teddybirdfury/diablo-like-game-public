﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FJL;

namespace FJL
{
    [Serializable]
    public class CharacterStats
    {

        protected CharacterActions m_character;

        public enum STATS : int
        {
            HEALTH = 0,
            HUNGER,
            THIRST,
            EXPERIENCE,
            ATTACK,
            DEFENSE,
            LEVEL,
        }

        //Created in order to save the List to a json, we don't really need to save the modifiers since those are from equipment but we might expand later on
        [Serializable]
        public class StatsContainer
        {
            public List<Stats> m_baseStats;
            public List<Stats> m_modifierStats;

            public StatsContainer(List<Stats> _baseStats, List<Stats> _modifierStats)
            {
                m_baseStats = _baseStats;
                _modifierStats = m_modifierStats;
            }
        }


        [SerializeField]
        public List<Stats> m_baseStats = new List<Stats>();

        [SerializeField]
        public List<Stats> m_modifierStats = new List<Stats>();

        [SerializeField]
        public List<Stats> m_finalStats = new List<Stats>();

        [SerializeField]
        public List<Stats> m_currentStats = new List<Stats>();

        public CharacterStats(CharacterActions _controller)
        {
            m_character = _controller;
        }

        public void InitializePlayerStats()
        {
            InitializeStats();
        }

        public void UpdateEquipmentStats()
        {
            List<Stats> m_totalStatsFromEquipment = m_character.m_inventory.GetTotalStatsFromEquipment();
            if (m_totalStatsFromEquipment.Count != 0)
            {
                foreach (Stats _stats in m_totalStatsFromEquipment)
                {
                    SetStats(m_modifierStats, _stats);
                    UpdateFinalStats(_stats);
                }
            }
            else
            {
                ResetModifierEquipmentStats();
            }
        }

        public void UpdateFinalStats(Stats _modifier = null)
        {
            if (_modifier != null)
            {
                string m_statName = _modifier.m_statName;
                Stats m_modifier = Stats.GetStatByName(this.m_modifierStats, m_statName);
                Stats m_base = Stats.GetStatByName(m_baseStats, m_statName);
                Stats m_final = Stats.GetStatByName(m_finalStats, m_statName);
                float m_statModifier = PlayerStatModifiers.GetModifier(m_statName, m_modifier.m_statValue);
                float m_finalValue = m_base.m_statValue * m_statModifier;
                SetStats(m_finalStats, m_statName, m_finalValue);
            }
            else
            {
                foreach (Stats _baseStats in m_baseStats)
                {
                    Stats m_modifier = Stats.GetStatByName(m_modifierStats, _baseStats.m_statName);
                    float m_finalValue = _baseStats.m_statValue * m_modifier.m_statValue;
                    Stats m_final = new Stats(_baseStats.m_statName, m_finalValue);
                    SetStats(m_finalStats, m_final);
                }
            }
        }

        private void InitializeStats()
        {
            string[] m_statNames = Enum.GetNames(typeof(STATS));
            for (int i = 0; i < m_statNames.Length; i++)
            {
                Stats m_newStat = new Stats(m_statNames[i]);

                if (i == (int)STATS.HEALTH
                    || i == (int)STATS.HUNGER
                    || i == (int)STATS.THIRST)
                {
                    m_baseStats.Add(new Stats(m_statNames[i], 100));
                    m_currentStats.Add(new Stats(m_statNames[i], 100));
                }
                else if (i == (int)STATS.ATTACK)
                {
                    m_baseStats.Add(new Stats(m_statNames[i], 20));
                    m_currentStats.Add(new Stats(m_statNames[i], 20));
                }
                else if (i == (int)STATS.DEFENSE)
                {
                    m_baseStats.Add(new Stats(m_statNames[i], 5));
                    m_currentStats.Add(new Stats(m_statNames[i], 5));
                }
                else
                {
                    m_baseStats.Add(new Stats(m_statNames[i], 100));
                    m_currentStats.Add(new Stats(m_statNames[i], 100));
                }

                m_modifierStats.Add(new Stats(m_statNames[i], 1));
                m_finalStats.Add(m_newStat);
            }
            UpdateFinalStats();
            UpdateEquipmentStats();
        }

        private void ResetModifierEquipmentStats()
        {
            SetStats(m_modifierStats, STATS.ATTACK, 0);
            SetStats(m_modifierStats, STATS.DEFENSE, 0);
        }

        public void UseItem(InventoryItem _item)
        {
            Stats m_stat = Stats.GetStatByName(_item.m_itemStats, PlayerStats.STATS.HUNGER.ToString());
            int m_statValue = 0;
            for (int i = 0; i < 2; i++)
            {
                if (i == 1)
                {
                    m_stat = Stats.GetStatByName(_item.m_itemStats, PlayerStats.STATS.THIRST.ToString());
                }
                if (m_stat != null)
                {
                    m_statValue = (int)m_stat.m_statValue;
                    if (m_statValue != 0)
                    {
                        AddStats(m_finalStats, m_stat);
                    }
                }
            }
        }

        public void AddStats(List<Stats> _statsToAddTo, Stats _stats)
        {
            Stats.GetStatByName(_statsToAddTo, _stats.m_statName).m_statValue += _stats.m_statValue;
        }

        public void SetStats(List<Stats> _statsToSet, STATS _stats, float _value)
        {
            Stats.GetStatByName(_statsToSet, _stats.ToString()).m_statValue = _value;
        }

        public void SetStats(List<Stats> _statsToSet, string _statName, float _value)
        {
            Stats.GetStatByName(_statsToSet, _statName).m_statValue = _value;
        }

        public void SetStats(List<Stats> _statsToSet, Stats _stats)
        {
            for (int i = 0; i < m_modifierStats.Count; i++)
            {
                if (_statsToSet[i].m_statName == _stats.m_statName)
                {
                    _statsToSet[i] = _stats;
                }
            }
        }

        public float GetStatValue(List<Stats> _statsToSet, STATS _stats)
        {
            return Stats.GetStatByName(_statsToSet, _stats.ToString()).m_statValue;
        }

        public void SaveStats()
        {
            StatsContainer m_statsContainer = new StatsContainer(m_baseStats, m_modifierStats);
            string m_jsonStats = JsonUtility.ToJson(m_statsContainer);
            PlayerPrefs.SetString(StringConstants.PLAYER_STATS, m_jsonStats);
        }

        /*  private void SaveGameGoogleCloud(ISavedGameMetadata game, byte[] savedData, TimeSpan totalPlaytime)
          {
               ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

               SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
               builder = builder
                   .WithUpdatedPlayedTime(totalPlaytime)
                   .WithUpdatedDescription("Saved game at " + DateTime.Now());
               if (savedImage != null)
               {
                   // This assumes that savedImage is an instance of Texture2D
                   // and that you have already called a function equivalent to
                   // getScreenshot() to set savedImage
                   // NOTE: see sample definition of getScreenshot() method below
                   byte[] pngData = savedImage.EncodeToPNG();
                   builder = builder.WithUpdatedPngCoverImage(pngData);
               }
               SavedGameMetadataUpdate updatedMetadata = builder.Build();
               savedGameClient.CommitUpdate(game, updatedMetadata, savedData, OnSavedGameWritten);
          }*/

        public bool LoadStats()
        {
            string m_jsonStats = PlayerPrefs.GetString(StringConstants.PLAYER_STATS);
            StatsContainer m_stats = JsonUtility.FromJson<StatsContainer>(m_jsonStats);
            this.m_baseStats = m_stats.m_baseStats;
            this.m_modifierStats = m_stats.m_modifierStats;
            return false;
        }

        void OnDestroy()
        {
            SaveStats();
        }
    }
}
