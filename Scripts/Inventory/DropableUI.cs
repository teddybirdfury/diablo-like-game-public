﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DropableUI : MonoBehaviour {

    public int m_itemSlotNum = -1;
    private bool m_callOnce = false;

    void Update()
    {
        if (!m_callOnce)
        {
            if (transform.localPosition.y < -850)
            {
                m_callOnce = true;
                StartCoroutine(DropItem());
            }
        }
    }

    IEnumerator DropItem()
    {
        if (SceneManager.GetActiveScene().name == StringConstants.MAIN_SCENE)
        {
            InventoryUIController.Instance.SpawnDroppedItem(m_itemSlotNum);
        }
        yield return new WaitForEndOfFrame();
        InventoryUIController.Instance.Delete(m_itemSlotNum);
    }

}
