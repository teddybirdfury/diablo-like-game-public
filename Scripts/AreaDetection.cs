﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AreaDetection : MonoBehaviour {

    float m_timerBeforeExit = 2.0f;

    public void OnTriggerEnter(Collider _collider)
    {
        if (_collider.transform.tag == StringConstants.PLAYER)
        {
            StopAllCoroutines();
        }
    }

    public void OnTriggerExit(Collider _collider)
    {
        if(_collider.transform.tag == StringConstants.PLAYER)
        {
            StartCoroutine(MoveToGlobalMap());
        }
    }

    private IEnumerator MoveToGlobalMap()
    {
        yield return new WaitForSeconds(m_timerBeforeExit);
        InventoryUIController.Instance.SaveItemPositions();
        SceneManager.LoadScene(StringConstants.GLOBAL_MAP);
    }

}
