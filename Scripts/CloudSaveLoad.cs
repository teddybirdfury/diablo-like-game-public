﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using System;
using UnityEngine.SceneManagement;

public class CloudSaveLoad : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //InitializeGPGServices();
        InitializeSignIn();
    }

    private void InitializeSignIn()
    {
        SceneManager.LoadScene(1);  //uncomment later when using google play
        /*Social.localUser.Authenticate((bool success) => {
            SceneManager.LoadScene(1);
        });*/
    }

    /*
    private void InvitationCallBack(Invitation invitation, bool shouldAutoAccept)
    {
        throw new NotImplementedException();
    }

    private void MatchCallBack(TurnBasedMatch match, bool shouldAutoLaunch)
    {
        throw new NotImplementedException();
    }

    private void InitializeGPGServices()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        // enables saving game progress.
        .EnableSavedGames()
        // registers a callback to handle game invitations received while the game is not running.
        .WithInvitationDelegate(InvitationCallBack)
        // registers a callback for turn based match notifications received while the
        // game is not running.
        .WithMatchDelegate(MatchCallBack)
        // requests the email address of the player be available.
        // Will bring up a prompt for consent.
        .RequestEmail()
        // requests a server auth code be generated so it can be passed to an
        //  associated back end server application and exchanged for an OAuth token.
        .RequestServerAuthCode(false)
        // requests an ID token be generated.  This OAuth token can be used to
        //  identify the player to other services such as Firebase.
        .RequestIdToken()
        .Build();

        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();
    }
    */
}
