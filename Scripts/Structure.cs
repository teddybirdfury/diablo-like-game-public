﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Structure {

    public GameObject m_prefab;
    public Sprite m_itemSprite = null;
    public string m_name = "";
    public string m_description = "";

    public List<InventoryItem> m_requiredResources = new List<InventoryItem>();
}
