﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CraftableItem {

    public int m_id
    {
        get { return m_item.m_id; }
    }

    public Sprite m_itemSprite
    {
        get { return m_item.m_itemSprite; }
    }

    public string m_name
    {
        get { return m_item.m_name; }
    }

    public string m_description
    {
        get { return m_item.m_description; }
    }

    public int m_timeToBuild = 5;

    public InventoryItem m_item;

    public List<InventoryItem> m_requiredResources = new List<InventoryItem>();

}
