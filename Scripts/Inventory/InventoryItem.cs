﻿using UnityEngine;
using System;
using System.Collections.Generic;
using FJL;

[Serializable]
public class InventoryItem {

    public enum ITEM_TYPE
    {
        NONE,
        RESOURCE,
        USEABLE,
        EQUIPMENT
    }

    public enum EQUIPMENT_TYPE
    {
        NONE = -1,
        WEAPON = 0,
        BAG,
        POUCH,
        UNKNOWN,
        HEAD,
        CHEST,
        LOWER,
        SHOES,
    }

    public int m_id = 0;
    public Sprite m_itemSprite = null;
    public string m_name = "";
    public string m_description = "";
    public int m_quantity = 0;
    public ITEM_TYPE m_itemType;
    public EQUIPMENT_TYPE m_equipmentType;
    public List<Stats> m_itemStats = new List<Stats>();

    public InventoryItem()
    {
        m_itemType = ITEM_TYPE.NONE;
    }

    public float GetStatsFromItem(PlayerStats.STATS _statName)
    {
        foreach(Stats _stat in m_itemStats)
        {
            if(_stat.m_statName == _statName.ToString())
            {
                return _stat.m_statValue;
            }
        }
        Debug.LogError("Stat type does not exist: " + _statName);
        return 0;
    }

    public InventoryItem(InventoryItem _item)
    {
        m_id = _item.m_id;
        m_itemSprite = SetDefaultSprite(_item.m_itemSprite);
        m_name = _item.m_name;
        m_quantity = _item.m_quantity;
        m_itemType = _item.m_itemType;
        m_itemStats = _item.m_itemStats;
    }

    public Sprite SetDefaultSprite(Sprite _itemSprite)
    {
        if (_itemSprite == null)
        {
            Debug.Log("Item sprite is null");
        }
        return _itemSprite;
    }

    public static bool IsEmptySprite(InventoryItem _item)
    {
        if (_item.m_itemSprite == null)
        {
            return true;
        }
        return false;
    }

    public InventoryItem(EQUIPMENT_TYPE _equipmentType)
    {
        m_name = "Unequipped";
        m_itemType = ITEM_TYPE.EQUIPMENT;
        m_equipmentType = _equipmentType;

    }
}
