﻿using FJL;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PlayerActions : CharacterActions {

    private PlayerActions m_playerBehaviour;
    private List<AllyActions> m_allies = new List<AllyActions>();
    public GameObject m_itemSpawn;

    public override void Start()
    {
        base.Start();
        foreach (InventoryItem m_testItem in GameInitializer.Instance.m_testItems)
        {
            AddInventoryItem(m_testItem);
        }

        InGameUIController.Instance.Initialize(this);
        InventoryUIController.Instance.Initialize(this);//Load Inventory on the cloud later

        if (SceneManager.GetActiveScene().name == StringConstants.LOCAL_LEVEL)
        {
            LootUIController.Instance.Initialize(this);
            BlueprintsUIController.Instance.Initialize(this);
            BuildUIController.Instance.Initialize(this);
            ResourceConverterUIController.Instance.Initialize(this);
            StructureUpgradeUIController.Instance.Initialize(this);
        }
    }


    public override void Initialize()
    {
        base.Initialize();
        InitializeAllies();
        m_playerBehaviour = GetComponent<PlayerActions>();
        StartCoroutine(Hunger());
        StartCoroutine(Thirst());
    }

    public bool IsCraftable(CraftableItem _item)
    {
        return m_inventory.IsCraftable(_item);
    }

    public void Craft(CraftableItem _item)
    {
        m_inventory.CraftItem(_item);
    }

    public void DropItem(InventoryItem _item)
    {
        GameObject m_spawn = GameObject.Instantiate(m_itemSpawn);
        m_spawn.GetComponent<Item>().m_item = _item;
        m_spawn.transform.position = transform.position;
    }

    private void InitializeAllies()
    {
        foreach (GameObject _ally in GameObject.FindGameObjectsWithTag(StringConstants.ALLY))
        {
            m_allies.Add(_ally.GetComponent<AllyActions>());
        }
    }

    public override void EnemyKilled(Transform _enemy, List<Stats> _enemyStats)
    {
        RemoveInteractiveFromList(_enemy);
        foreach (AllyActions _ally in m_allies)
        {
            _ally.RemoveInteractiveFromList(_enemy);
        }
        GameObject.Destroy(_enemy.gameObject);
    }

    public void ToggleControls(bool _isEnable)
    {
        m_playerBehaviour.ToggleControls(_isEnable);
    }

    private IEnumerator Hunger()
    {
        do
        {
            yield return new WaitForSeconds(1.0f);
            Stats m_stats = Stats.GetStatByName(m_characterStats.m_finalStats, CharacterStats.STATS.HUNGER.ToString());
            float m_value = m_stats.m_statValue - 1;
            m_characterStats.SetStats(m_characterStats.m_currentStats, CharacterStats.STATS.HUNGER, (int)m_value);
            InventoryUIController.Instance.UpdateInventoryHungerText((int)m_value);
        } while (m_isSurvivalMode);
    }

    private IEnumerator Thirst()
    {
        do
        {
            yield return new WaitForSeconds(1.0f);
            float m_value = (int)Stats.GetStatByName(m_characterStats.m_finalStats, PlayerStats.STATS.THIRST.ToString()).m_statValue - 1;
            m_characterStats.SetStats(m_characterStats.m_currentStats, PlayerStats.STATS.THIRST, (int)m_value);
            InventoryUIController.Instance.UpdateInventoryThirstText((int)m_value);
        } while (m_isSurvivalMode);
    }
}
