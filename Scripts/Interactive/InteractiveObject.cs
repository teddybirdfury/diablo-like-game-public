﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObject : MonoBehaviour {

    public enum InteractiveTypes
    {
        ITEM,
        NPC,
        ENEMY,
        LOOT,
        STRUCTURE
    }

    public InteractiveTypes m_type;

    public delegate void InteractiveObjectHandler();

    public event InteractiveObjectHandler InteractiveObjectHandle;

    public void InteractObject()
    {
        if (InteractiveObjectHandle != null)
        {
            InteractiveObjectHandle.Invoke();
        }
    }

    public void SetType(InteractiveTypes _type)
    {
        m_type = _type;
    }
}
