﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using FJL;

public class InventoryUIController : BaseDraggableInventoryUIController
{

    public static InventoryUIController Instance;

    public Sprite m_defaultEmptySprite;
    public GameObject m_equipmentParent;
    
    public List<GameObject> m_equipmentSlotUI;

    private GameObject m_itemToMove;
    
    private bool m_isFromEquipment = false;

    public Text m_inventoryHungerValue;
    public Text m_inventoryThirstValue;

    public Text m_attackStats;
    public Text m_defenseStats;

    public Image m_healthImage;
    public Text m_playerName;
    public Text m_health;

    private List<int> m_equipmentSlotsOfSameEquipmentType = new List<int>();
    
    private int m_onDownSlotNumber = 0;

    void Awake()
    {
        Instance = this;
    }

    private Vector2 RandomizePosition()
    {
        float m_maxX = (base.m_playerItemSlotParent.transform.GetComponent<RectTransform>().sizeDelta.magnitude * 2) - m_randomizePositionXOffset;
        float m_minX = -(base.m_playerItemSlotParent.transform.GetComponent<RectTransform>().sizeDelta.magnitude * 2) + m_randomizePositionXOffset;
        float m_value = UnityEngine.Random.Range(m_minX, m_maxX);
        return new Vector2(m_value, m_itemSlotPrefab.transform.position.y);
    }

    protected override void TogglePopUp(bool _isEnable, string _text = "")
    {
        if (!_isEnable)
        {
            m_popUp.SetActive(false);
        }
        else if (m_chosenItemSlotNumber != -1)
        {
            InventoryItem m_item = null;
            if (m_isFromEquipment && !m_playerInventory.IsItemExistOnSlot(m_chosenItemSlotNumber, true))
            {
                return;
            } else if (m_isFromEquipment)
            {
                m_item = m_playerInventory.GetItemFromSlot(m_chosenItemSlotNumber, true);
            } else
            {
                m_item = m_playerInventory.GetItemFromSlot(m_chosenItemSlotNumber);
            }
            
            if(m_item.m_name.Length <= 0)
            {
                return;
            }

            m_popUp.transform.position = m_playerItemSlotUI[m_chosenItemSlotNumber].transform.position;
            m_popUp.GetComponentInChildren<Text>().text = GenerateText(m_item);
            m_popUp.SetActive(true);
        }
      
    }

    private void LoadItemPositions()
    {
        Vector2[] m_itemPositions = PlayerPrefsX.GetVector2Array(StringConstants.ITEM_POSITIONS);
        if (m_itemPositions.Length == 0)
        {
            return;
        }
        
        for (int i = 0; i < m_itemPositions.Length; i++)
        {
            if (i < m_playerItemSlotUI.Count)
            {
                m_playerItemSlotUI[i].transform.localPosition = m_itemPositions[i];
            }
        }
    }

    protected override IEnumerator InitializeProcess()
    {
        InitializeEquipmentUI();
        RefreshInventory();
        yield return new WaitForEndOfFrame();
        base.InitializeItemPositionsOnUI(m_playerItemSlotUI, base.m_playerItemSlotParent);
        InitializeHandlers();
        LoadItemPositions();
        RefreshCharacterStats();
    }

    private void InitializeHandlers()
    {
        m_player.PlayerHealthHandle += () =>
        {
           UpdateHealth();
        };
    }

    private void InitializeEquipmentUI()
    {
        EventTrigger[] m_triggers = m_equipmentParent.GetComponentsInChildren<EventTrigger>();
        for (int i = -1; i < m_triggers.Length; i++)
        {
            if(i == -1)
            {
                continue;
            }
            InventoryItem.EQUIPMENT_TYPE m_equipmentType = (InventoryItem.EQUIPMENT_TYPE)Enum.GetValues(typeof(InventoryItem.EQUIPMENT_TYPE)).GetValue(i);
            InitializeEvents(m_triggers[i].gameObject, (int)m_equipmentType, false);
            m_playerInventory.AddEquipmentItem(m_equipmentType);
        }
        
    }

    private void RefreshCharacterStats()
    {
        m_player.m_characterStats.UpdateEquipmentStats();

        if (m_player.m_characterStats.m_finalStats.Count > 0)
        {
            m_attackStats.text = "Attack: " + m_player.m_characterStats.GetStatValue(m_player.m_characterStats.m_modifierStats, PlayerStats.STATS.ATTACK).ToString();
            m_defenseStats.text = "Defense: " + m_player.m_characterStats.GetStatValue(m_player.m_characterStats.m_modifierStats, PlayerStats.STATS.DEFENSE).ToString();
        } else
        {
            m_attackStats.text = "0";
            m_defenseStats.text = "0";
        }
    }

    public void OnDown(PointerEventData data, int _slotNumber, bool _isPlayerInventory)
    {
        if(m_objectBeingDragged != null)
        {
            return;
        }
        m_isFromEquipment = !_isPlayerInventory;
        m_chosenItemSlotNumber = _slotNumber;

        //  Debug.Log("Slot " + _slotNumber + " Quantity: " + m_inventory.GetQuantity(_slotNumber) + " Is Equipment: " + _isEquipmentSlot);
        if (!m_playerInventory.IsItemExistOnSlot(_slotNumber, m_isFromEquipment))
        {
            Debug.Log("No item on inventory slot " + _slotNumber + " Is Equipment: " + m_isFromEquipment);
            m_objectBeingDragged = null;
            return;
        }

        if (m_playerInventory.IsEquipmentFromInventory(_slotNumber))
        {
            m_equipmentSlotsOfSameEquipmentType = new List<int>();
            m_equipmentSlotsOfSameEquipmentType = m_playerInventory.GetEquipmentSlotsWithType(_slotNumber);

        }

        //Should only be available on inventory items not on equipment
        if (!m_isFromEquipment)
        {
            m_useButton.interactable = false;
            m_splitButton.interactable = false;
            m_deleteButton.interactable = true;
            if (m_playerInventory.IsEquipmentFromInventory(_slotNumber))
            {
                m_useButton.GetComponentInChildren<Text>().text = "Equip";    //Equip
                m_splitButton.interactable = false;
                m_useButton.interactable = true;
            }
            else
            {
                m_useButton.GetComponentInChildren<Text>().text = "Use";
                if (m_playerInventory.IsItemUsable(_slotNumber))
                {
                    m_useButton.interactable = true;
                }
                if (m_playerInventory.IsItemCanSplit(_slotNumber) && m_playerInventory.GetQuantity(_slotNumber) > 1)
                { 
                    m_splitButton.interactable = true;
                }
            }
        } else
        {
            m_useButton.GetComponentInChildren<Text>().text = "Use";
            m_useButton.interactable = false;
            m_splitButton.interactable = false;
        }

        // Debug.Log("On Down");
        if (m_isFromEquipment) {
            m_objectBeingDragged = m_equipmentSlotUI[_slotNumber].GetComponentsInChildren<Transform>()[1].gameObject;
            m_itemToMove = GameObject.Instantiate(m_objectBeingDragged, m_playerItemSlotParent.transform.parent);
            m_itemToMove.GetComponent<Image>().sprite = m_objectBeingDragged.GetComponent<Image>().sprite;
            m_itemToMove.transform.position = m_objectBeingDragged.transform.position;
            m_itemToMove.GetComponent<Image>().raycastTarget = false;
            m_itemToMove.SetActive(true);

            m_objectBeingDragged.SetActive(false);
		}
		else
        {
            m_objectBeingDragged = m_playerItemSlotUI[_slotNumber];
			m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 0;
			m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;

        }

        m_onDownSlotNumber = _slotNumber;
    }

    public void OnDrag(PointerEventData data, int _slotNumber, bool _isPlayerInventory)
    {
		TogglePopUp(false);
      //  Debug.Log("On Drag: " + m_equipmentSlotsOfSameEquipmentType.Count);
        if (!m_playerInventory.IsItemExistOnSlot(_slotNumber, !_isPlayerInventory))
        {
            return;
        }

		if (!_isPlayerInventory)
		{
            m_itemToMove.transform.position = m_inputPosition;
        }
		else
		{
            m_objectBeingDragged.GetComponent<Image>().raycastTarget = false;
            m_objectBeingDragged.transform.position = m_inputPosition;
           // m_objectBeingDragged.transform.parent.SetSiblingIndex(1);
        }
    }

    public void OnBeginDrag(PointerEventData data, int _slotNumber, bool _isPlayerInventory)
    {
        if(m_objectBeingDragged == null)
        {
            return;
        }
        TogglePopUp(false);
        if (_isPlayerInventory)
        {
            m_oldParent = base.m_playerItemSlotParent.transform;
        } else
        {
            m_oldParent = m_objectBeingDragged.transform.parent;
        }

        Transform m_parentObj = m_objectBeingDragged.transform.parent.transform.parent;
        int m_targetSiblingIndex = m_objectBeingDragged.transform.parent.childCount - 2;
        m_objectBeingDragged.transform.SetParent(m_parentObj);
        
        m_objectBeingDragged.transform.SetSiblingIndex(m_targetSiblingIndex);

        if (m_equipmentSlotsOfSameEquipmentType.Count > 0)
        {
            foreach(int _value in m_equipmentSlotsOfSameEquipmentType)
            {
                m_equipmentSlotUI[_value].GetComponent<Button>().Select();
            }
            
        }
    }

	public void OnDragEnd(PointerEventData data, int _slotNumber, bool _isPlayerInventory = false)
    {
        m_eventSystem.SetSelectedGameObject(null);
        m_equipmentSlotsOfSameEquipmentType = new List<int>();
  
        if (m_objectBeingDragged == null)
        {
            return;
        }

        m_objectBeingDragged.transform.SetParent(m_oldParent);
        m_objectBeingDragged.GetComponent<Image>().raycastTarget = true;

        if (_isPlayerInventory) {
			m_objectBeingDragged.GetComponent<Rigidbody2D> ().gravityScale = 1;
			m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
		}
        else if (m_isFromEquipment)
        {
            m_playerInventory.AddInventoryItem(m_playerInventory.m_equipmentItems[_slotNumber]);
            m_playerInventory.RemoveEquipment(_slotNumber);
            CreateInventoryUIItem();
            RefreshInventory(); 
            RefreshCharacterStats();
        }

        m_objectBeingDragged.SetActive(true);
        if (m_itemToMove != null)
        {
            m_itemToMove.SetActive(false);
        }
        m_objectBeingDragged = null;
    }

    public void CreateInventoryUIItem(InventoryItem _item = null)
    {
        GameObject temp = GameObject.Instantiate(m_itemSlotPrefab);
        temp.transform.SetParent(base.m_playerItemSlotParent.transform, false);
        temp.transform.localPosition = RandomizePosition();
        m_playerItemSlotUI.Add(temp); 
        if(_item != null)
        {
            temp.GetComponentsInChildren<Image>()[1].sprite = _item.m_itemSprite;
        }
        InitializeEvents(temp, m_playerItemSlotUI.Count - 1, true);
    }

    public void OnDrop(PointerEventData data, int _slotNumber, bool _isPlayerInventory)
    {
        if (m_objectBeingDragged == null || m_isFromEquipment)
        {
            return;
        }
        StartCoroutine(ReindexEventArraySlots());
     
        // Debug.Log("On Drop, On down slot number: " + m_onDownSlotNumber + " Slot Number: " + _slotNumber + " Is Equipment: " + _isEquipmentSlot);
        DisableAllButtons();

		if (_isPlayerInventory) {
			m_objectBeingDragged.GetComponent<Rigidbody2D> ().gravityScale = 0;
			m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
		}
        MoveItem(m_onDownSlotNumber, _slotNumber, _isPlayerInventory);
        RefreshInventory();
        m_eventSystem.SetSelectedGameObject(null);
    }

    public void OnPointerUp(bool _isPlayerInventory = false)
    {
        if (m_objectBeingDragged != null)
        {
            m_objectBeingDragged.SetActive(true);
        }
        if (m_itemToMove != null)
        {
            m_itemToMove.SetActive(false);
        }

		if (_isPlayerInventory && m_objectBeingDragged != null) {
			m_objectBeingDragged.GetComponent<Rigidbody2D> ().gravityScale = 1;
			m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
		}
    }

    public void DisableAllButtons()
    {
        m_useButton.interactable = false;
        m_splitButton.interactable = false;
        m_deleteButton.interactable = false;
    }

    public void MoveItem(int _startSlotNum, int _droppedSlotNum, bool _isPlayerInventory = false)
    {
       // Debug.Log("Move pocket item, start: " + _startSlotNum + " drop: " + _droppedSlotNum + " Is from equipment: " + m_isFromEquipment);
        if (!_isPlayerInventory)
        {
            if (!m_playerInventory.IsItemEquipment(_startSlotNum))
            {
                return;
            }
            if(m_playerInventory.MoveItem(_startSlotNum, _droppedSlotNum, m_isFromEquipment, true))
            {
                UpdateItemSprite(_startSlotNum, _droppedSlotNum, m_isFromEquipment, true);
                RefreshCharacterStats();
            }
            GameObject.Destroy(m_objectBeingDragged);
            m_playerItemSlotUI.RemoveAt(_startSlotNum);
        }
        else
        {
            if (m_playerInventory.m_inventoryItems[_startSlotNum].m_id == m_playerInventory.m_inventoryItems[_droppedSlotNum].m_id
                && m_playerInventory.MoveItem(_startSlotNum, _droppedSlotNum, m_isFromEquipment))
            {
                GameObject.Destroy(m_objectBeingDragged);
                m_playerItemSlotUI.RemoveAt(_startSlotNum);
                m_playerInventory.RemoveItem(m_chosenItemSlotNumber);
                StartCoroutine(ReindexEventArraySlots());
                if (m_isFromEquipment) {
                    RefreshCharacterStats();
                }
                SetQuantityDisplay(m_playerItemSlotUI, m_playerInventory);;
            }
        }
       
    }

    private IEnumerator ReindexEventArraySlots()
    {
        for (int i = 0; i < m_playerItemSlotUI.Count; i++)
        {
            Destroy(m_playerItemSlotUI[i].GetComponent<EventTrigger>());
            m_playerItemSlotUI[i].AddComponent(typeof(EventTrigger));
            m_playerItemSlotUI[i].GetComponent<DropableUI>().m_itemSlotNum = i;
            yield return new WaitForEndOfFrame();
            InitializeEvents(m_playerItemSlotUI[i], i, true);
        }
    }

    public void UpdateItemSprite(int _startSlotNum, int _droppedSlotNum, bool _isFromEquipment, bool _isEquipmentDrop = false)
    {
        Sprite _temp = m_playerItemSlotUI[_startSlotNum].GetComponentsInChildren<Image>(true)[1].sprite;
        if (_isEquipmentDrop)
        {
            m_playerItemSlotUI[_startSlotNum].GetComponentsInChildren<Image>(true)[1].sprite = m_equipmentSlotUI[_droppedSlotNum].GetComponentsInChildren<Image>(true)[1].sprite;
            m_equipmentSlotUI[_droppedSlotNum].GetComponentsInChildren<Image>(true)[1].sprite = _temp;
            RefreshCharacterStats();
        } else
        {
            if (_isFromEquipment) {
                _temp = m_equipmentSlotUI[_startSlotNum].GetComponentsInChildren<Image>(true)[1].sprite; 
                m_equipmentSlotUI[_startSlotNum].GetComponentsInChildren<Image>(true)[1].sprite = m_playerItemSlotUI[_droppedSlotNum].GetComponentsInChildren<Image>(true)[1].sprite;
                m_playerItemSlotUI[_droppedSlotNum].GetComponentsInChildren<Image>(true)[1].sprite = _temp;
            } else
            {
                m_playerItemSlotUI[_startSlotNum].GetComponentsInChildren<Image>(true)[1].sprite = m_playerItemSlotUI[_droppedSlotNum].GetComponentsInChildren<Image>(true)[1].sprite;
                m_playerItemSlotUI[_droppedSlotNum].GetComponentsInChildren<Image>(true)[1].sprite = _temp;
            }
        }
    }

    public void RefreshInventory(bool _isEquipment = false)
    {
        if (_isEquipment)
        {
            RefreshCharacterStats();
        }
        else
        {
            RefreshInventory(m_playerItemSlotUI, m_playerInventory, m_playerItemSlotParent, true);
        }

        bool m_isClearEquipment = false;

        if(m_playerInventory.m_equipmentItems.Count == 0) {
            m_isClearEquipment = true;
        }
        
        for (int i = 0; i < m_equipmentSlotUI.Count; i++)
        {
            if (m_isClearEquipment)
            {
                m_equipmentSlotUI[i].GetComponentsInChildren<Image>()[1].sprite = m_defaultEmptySprite;
            }
            else if(m_playerInventory.m_equipmentItems.Count >= i)
            {
                m_equipmentSlotUI[i].GetComponentsInChildren<Image>()[1].sprite = m_playerInventory.m_equipmentItems[i].m_itemSprite;
            }   
        }
        
    }

    public void EmptyItemSlot(int _slotNumber)
    {
        m_playerItemSlotUI[_slotNumber].GetComponentsInChildren<Image>()[1].sprite = null;
    }

    public void ChangeItem(InventoryItem _item, int _slotNumber)
    {
        m_playerItemSlotUI[_slotNumber].GetComponentsInChildren<Image>()[1].sprite = _item.m_itemSprite;
    }

    public void UseOrEquipItem()
    {
        if (m_playerInventory.UseOrEquip(m_chosenItemSlotNumber)) 
        {
            //EmptyItemSlot(m_chosenItemSlotNumber);
            GameObject _toDestroy = m_playerItemSlotUI[m_chosenItemSlotNumber];
            m_playerItemSlotUI.RemoveAt(m_chosenItemSlotNumber);
            GameObject.Destroy(_toDestroy);
          
            m_useButton.interactable = false;
            m_splitButton.interactable = false;
            StartCoroutine(ReindexEventArraySlots());
        }
        SetQuantityDisplay(m_playerItemSlotUI, m_playerInventory);
        RefreshInventory();
        DisableAllButtons();
    }

    public void Split()
    {
        m_playerInventory.Split(m_chosenItemSlotNumber);
        if (m_playerInventory.GetQuantity(m_chosenItemSlotNumber) <= 1)
        {
            m_splitButton.interactable = false;
        }
        
        RefreshInventory();
        SetQuantityDisplay(m_playerItemSlotUI, m_playerInventory);;
    }

    public void Delete(int _itemSlotNum)
    {
        GameObject temp = m_playerItemSlotUI[_itemSlotNum];
        //m_itemSlotPrefabUI.RemoveAt(m_chosenItemSlotNumber);
        m_playerItemSlotUI.Remove(temp);
        m_playerInventory.RemoveItem(_itemSlotNum);
        GameObject.Destroy(temp);
        
        RefreshInventory();
        SetQuantityDisplay(m_playerItemSlotUI, m_playerInventory);;
        DisableAllButtons();
        StartCoroutine(ReindexEventArraySlots());
    }

    public void SpawnDroppedItem(int _slotNum)
    {
        InventoryItem _item = m_playerInventory.GetItemFromSlot(_slotNum);
        m_player.DropItem(_item);
    }

    protected override void InitializeEvents(GameObject _slotObj, int _slotNumber, bool _isPlayerInventory)
    {
        EventTrigger _trigger = _slotObj.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
		entry.callback.AddListener((data) => {
			OnDown((PointerEventData)data, _slotNumber, _isPlayerInventory);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.BeginDrag;
        entry.callback.AddListener((data) => {
			OnBeginDrag((PointerEventData)data, _slotNumber, _isPlayerInventory);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drag;
        entry.callback.AddListener((data) => {
            OnDrag((PointerEventData)data, _slotNumber, _isPlayerInventory);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drop;
        entry.callback.AddListener((data) => {
            OnDrop((PointerEventData)data, _slotNumber, _isPlayerInventory);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.EndDrag;
        entry.callback.AddListener((data) => {
			OnDragEnd((PointerEventData)data, _slotNumber, _isPlayerInventory);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((data) => {
			OnPointerUp(_isPlayerInventory);
        });
        _trigger.triggers.Add(entry);
    }

    public void UpdateInventoryHungerText(int _value)
    {
        m_inventoryHungerValue.text = _value.ToString();
    }

    public void UpdateInventoryThirstText(int _value)
    {
        m_inventoryThirstValue.text = _value.ToString();
    }

    private void UpdateHealth()
    {
        float m_currentHealth = Stats.GetStatValueByName(m_player.m_characterStats.m_currentStats, PlayerStats.STATS.HEALTH.ToString());
        float m_maxHealth = Stats.GetStatValueByName(m_player.m_characterStats.m_finalStats, PlayerStats.STATS.HEALTH.ToString());

        float m_healthPercent = m_currentHealth / m_maxHealth;
        m_health.text = m_currentHealth.ToString();
        m_healthImage.fillAmount = m_healthPercent;
    }

    public void SetName(string _value)
    {
        m_playerName.text = _value;
    }

}
