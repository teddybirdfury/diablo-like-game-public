﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringConstants {

    public const string PLAYER = "Player";
    public const string GRID_OBJECTS = "Grid Objects";

    public const string INTERACTIVE = "Interactive";
    public const string SPAWNABLE = "Spawnable";
    public const string MAP_OBJECT = "Map Object";
    public const string GRID_TILE = "Grid Tile";

    public const string PLAYER_STATS = "Player Stats";
    public const string GLOBAL_MAP_TIMER = "Global Map Timer";

    public const string GLOBAL_MAP = "Global Map";
    public const string CURRENT_AREA = "Current Area";

    public const string CANVAS = "Canvas";
    public const string JOYSTICK_AREA = "Joystick Area";

    public const string ALLY = "Ally";
    public const string ITEM_POSITIONS = "Item Positions";
    public const string UNTAGGED = "Untagged";

    public const string MAIN_SCENE = "main";
    public const string LOCAL_LEVEL = "LocalLevel";
    public const string CRAFTABLE_ITEM = "Craftable Item";
}
