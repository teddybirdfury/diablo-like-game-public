﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyBehaviour : CharacterBehaviour {
    
    private GameObject m_player;
    private AllyMovement m_allyMovement;

    private bool m_isMoveDelay = false;

    private float m_playerDistanceMin = 2;
    private float m_playerDistanceMax = 6;
    private float m_distanceDetectEnemy = 5;
    private float m_distanceToAttack = 2;
    public bool m_isAttacking = false;
    public bool m_followingPlayer = false;

    public override void Start()
    {
        base.Start();
        base.m_characterMovement.m_movementSpeed = 1;
       
        m_allyMovement = GetComponent<AllyMovement>();
        m_allyMovement.m_target = GameObject.FindGameObjectWithTag(StringConstants.PLAYER);//Only player for now
        m_player = m_allyMovement.m_target;
    }

    public override void Update()
    {
        if (IsDistanceFar(transform.position, m_player.transform.position, m_playerDistanceMax))
        {
            SetMovementTarget(m_player);
            m_allyMovement.m_enableMovement = true;
            m_isAttacking = false;
            m_followingPlayer = true;
        }
        else if (!m_isAttacking && Utility.IsDistanceClose(transform.position, m_player.transform.position, m_playerDistanceMin))
        {
            m_allyMovement.m_enableMovement = false;
            m_followingPlayer = false;
        }

        if (m_followingPlayer)
        {
            m_isAttacking = false;
            return;
        }

        if (base.m_enemyDetectArea.m_closestDetectedInteractive != null)
        {
            bool m_isEnemyWithinRange = Utility.IsDistanceClose(transform.position, base.m_enemyDetectArea.m_closestDetectedInteractive.transform.position, m_distanceDetectEnemy);
            if (!m_followingPlayer && !m_isAttacking && m_isEnemyWithinRange)
            {
                EnemyCloseBehaviour();
            }
            else if (!m_followingPlayer && m_isAttacking && Utility.IsDistanceClose(transform.position, base.m_enemyDetectArea.m_closestDetectedInteractive.transform.position, m_distanceToAttack))
            {
                Attack();
            } else if(!m_followingPlayer && m_isEnemyWithinRange)
            {
                m_allyMovement.m_enableMovement = true;
            }
        } else
        {
            m_followingPlayer = false;
            m_isAttacking = false;
        }
    }

    public override void Attack()
    {
        m_allyMovement.m_enableMovement = false;
        if (m_targetEnemyPosition != Vector3.zero && !m_attackOnCooldown)
        {
            m_attackOnCooldown = true;
            m_isAttack = true;
            m_characterMovement.LookTowards(m_targetEnemyPosition);
            m_isInteractAction = true;

            m_currentlyInteracted = base.m_enemyDetectArea.m_closestDetectedInteractive;
           
            InteractProcess();
            m_isAttacking = false;
            StartCoroutine(AttackCooldown());
        }
    }


    private void EnemyCloseBehaviour()
    {
        SetMovementTarget(base.m_enemyDetectArea.m_closestDetectedInteractive.gameObject);
        m_isAttacking = true;
    }

    private void SetMovementTarget(GameObject _target)
    {
        m_allyMovement.m_target = _target;
    }

    public static bool IsDistanceFar(Vector3 _positionStart, Vector3 _positionEnd, float _distanceToInteract)
    {
        Vector3 m_modifiedPositionStart = new Vector3(_positionStart.x, 0, _positionStart.z);
        Vector3 m_modifiedPositionEnd = new Vector3(_positionEnd.x, 0, _positionEnd.z);
        return (Vector3.Distance(m_modifiedPositionStart, m_modifiedPositionEnd) > _distanceToInteract);
    }
}
