﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using FJL;

public class PlayerBehaviour : CharacterBehaviour
{

    private PlayerControls m_characterControls;

    public override void Start()
    {
        base.Start();

        InGameUIController.Instance.ToggleEnemyPanel(true);
        m_characterControls = GetComponent<PlayerControls>();
    }

    public override void EnemyDetection()
    {
        if (m_enemyDetectArea != null)
        {
            m_enemyDetectArea.InteractiveDetectedHandle += () =>
            {
                if (m_enemyDetectArea.m_closestDetectedInteractive != null)
                {
                    m_targetEnemyPosition = m_enemyDetectArea.m_closestDetectedInteractive.transform.position;
                    InGameUIController.Instance.ToggleEnemyPanel(true);
                    if (m_enemyDetectArea.m_hasTargetChanged)
                    {
                        SetEnemyOnUI(m_enemyDetectArea.m_closestDetectedInteractive.transform);
                    }
                } else {
                    InGameUIController.Instance.ToggleEnemyPanel(false);
                }
            };
        }
    }

    public void ToggleControls(bool _isEnable)
    {
        m_enableControls = _isEnable;
        m_characterControls.m_enableControls = _isEnable;
        base.m_characterMovement.m_enableMovement = _isEnable;
    }
}
