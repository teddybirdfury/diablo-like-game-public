﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StructureUpgradeUIController : BaseDraggableInventoryUIController
{

    public static StructureUpgradeUIController Instance;

    public List<GameObject> m_resourceDropUISlots;

    public List<int> m_chosenResourcesSlotNumber = new List<int>();
    public Dictionary<int, int> m_placedResourcesSlotNumber = new Dictionary<int, int>();

    public UpgradeableStructure m_structureToFinalize;
    public GameObject m_structureToFinalizeSlotUI;

    public Sprite m_defaultEmptySprite;
    public Text m_timer;
    
    public Image m_structureIcon;
    public Text m_structureName;

    private int m_chosenResourceInventoryIndex = -1;
    private float m_currentBuildTimer = -1;
    public GameObject m_backToConvertScreen;

    private void Awake()
    {
        Instance = this;
    }

    public override void Initialize(PlayerActions _player)
    {
        m_player = _player;
        m_playerInventory = m_player.m_inventory;
        StartCoroutine(InitializeProcess());
        
       // Reinitialize(); //Remove later, only for testing
    }

    public void Reinitialize(UpgradeableStructure _chosenStructure)
    {
        m_structureToFinalize = _chosenStructure;
        StartCoroutine(base.ReindexEventArraySlots(m_playerItemSlotUI, true));
        StartCoroutine(ReindexEventArraySlots(m_resourceDropUISlots, true));
        //InitializeResourcesDropEvent();
        RefreshInventory(m_playerItemSlotUI, m_playerInventory, m_playerItemSlotParent, true);
        SetQuantityDisplay(m_playerItemSlotUI, m_playerInventory);
        RemoveDropableUIScript();

        if (m_placedResourcesSlotNumber.Count == 0)
        {
            InitializeSlotsTransparency();
        }
        InitializeSlotsQuantity();
        SetSpriteImages();
        m_isInputsEnabled = true;
        SetStructureDetails();

        if (m_structureToFinalize.GetComponent<InteractiveStructure>().m_hasConvertResource)
        {
            m_backToConvertScreen.SetActive(true);
        }
        else
        {
            m_backToConvertScreen.SetActive(false);
        }
    }

    public void ConvertScreen()
    {
        BaseUIController.Instance.EnableMenu(6);
        ResourceConverterUIController.Instance.Reinitialize(m_structureToFinalize);
    }

    private void InitializeSlotsQuantity()
    {
        for(int i = 0; i < m_resourceDropUISlots.Count; i++)
        {
            m_resourceDropUISlots[i].GetComponentInChildren<Text>().text = "";
        }

        for (int i = 0; i < m_structureToFinalize.m_requiredResources.Count; i++)
        {
            m_resourceDropUISlots[i].GetComponentsInChildren<Image>()[1].sprite = m_structureToFinalize.m_requiredResources[i].m_itemSprite;
            m_resourceDropUISlots[i].GetComponentInChildren<Text>().text = m_structureToFinalize.m_requiredResources[i].m_quantity.ToString();
            m_chosenResourceInventoryIndex = i;
        }
    }

    private void SetStructureDetails()
    {
        m_structureName.text = m_structureToFinalize.m_name;
        m_structureIcon.sprite = m_structureToFinalize.m_itemSprite;
    }

    private void SetSpriteImages()
    {
        m_structureToFinalizeSlotUI.GetComponentsInChildren<Image>()[1].sprite = m_structureToFinalize.m_itemSprite;
        for(int i = 0; i < m_structureToFinalize.m_requiredResources.Count; i++)
        {
            m_resourceDropUISlots[i].GetComponentsInChildren<Image>()[1].sprite = m_structureToFinalize.m_requiredResources[i].m_itemSprite;
            m_chosenResourceInventoryIndex = i;
        }
    }

    private void InitializeSlotsTransparency()
    {
        for(int i = 0; i < m_resourceDropUISlots.Count; i++)
        {
            m_chosenResourceInventoryIndex = i;
            ImageAlphaAdjustResourceDrop(i, true);
        }
        m_chosenResourceInventoryIndex = -1;
    }

    protected override void Update()
    {
        base.Update();
      /*  if (m_currentBuildTimer > 0)
        {
            m_currentBuildTimer -= Time.deltaTime;
            m_timer.text = m_currentBuildTimer.ToString("F2");
        }
        else if (m_currentBuildTimer <= 0 && m_chosenResourceInventoryIndex != -1)
        {
            if (m_structureToFinalize != null && m_structureToFinalize.m_id != -1 && m_placedResourcesSlotNumber.Count > 0)
            {
                int m_currentInventoryIndex = m_chosenResourceInventoryIndex;
                m_playerInventory.RemoveItem(m_currentInventoryIndex);
                m_structureToFinalize.Upgrade(false);
                //OnResourceEndDrag(null);
                Debug.Log("Null finalize");
                m_structureToFinalize = null;
                m_timer.text = "";
            }
        }
        else
        {
            m_timer.text = "";
        }*/
    }

    private bool CanCraftItem(int _inventoryItemIndex, int _slotNumber)
    {
        if (m_structureToFinalize.m_requiredResources[_slotNumber].m_id == m_playerInventory.m_inventoryItems[_inventoryItemIndex].m_id
            && m_playerInventory.CanCraft(m_structureToFinalize.m_requiredResources[_slotNumber])){
            return true;
        }

       /* if (m_player.m_inventory.CanCraft(m_itemToCraft.m_requiredResources))
        {
            m_canCraft = false;
            ImageAlphaAdjustResourceDrop(false);
        }
        else
        {
            m_canCraft = true;
            ImageAlphaAdjustResourceDrop(true);
        }

        CraftableItem m_itemToCraft = CraftableItemsList.Instance.m_list.FindItemWithExactRequirements(m_itemToCraft.m_id);

        if (m_itemToCraft != null && m_player.m_inventory.CanCraft(m_itemToCraft.m_requiredResources))
        {
            m_resourceDropObject.GetComponent<Button>().Select();
        }*/
        return false;
    }

    private void OnResourceDrop(int _slotNumber)
    {
        if (m_objectBeingDragged == null || !CanCraftItem(m_chosenResourceInventoryIndex, _slotNumber))
        {
            m_objectBeingDragged = null;
            return;
        }

        m_placedResourcesSlotNumber.Add(_slotNumber, m_chosenItemSlotNumber);
        m_resourceDropUISlots[_slotNumber].GetComponentsInChildren<Image>()[1].sprite = m_objectBeingDragged.GetComponentsInChildren<Image>()[1].sprite;
        m_playerItemSlotUI[m_chosenItemSlotNumber].SetActive(false);
        m_chosenResourceInventoryIndex = m_chosenItemSlotNumber;

        ImageAlphaAdjustResourceDrop(_slotNumber, false);
        m_chosenItemSlotNumber = -1;
    }

    public void FinalizeStructure()
    {
        List<InventoryItem> m_items = new List<InventoryItem>();
        List<InventoryItem> m_requiredItems = m_structureToFinalize.m_requiredResources;

        foreach (int _value in m_placedResourcesSlotNumber.Values)
        {
            m_items.Add(m_playerInventory.m_inventoryItems[_value]);
        }

        if (m_items.Count != m_requiredItems.Count)
        {
            return;
        } else {

            for (int i = 0; i < m_requiredItems.Count; i++)
            {
                bool m_hasResource = false;
                for (int j = 0; j < m_items.Count; j++)
                {
                    if(m_items[j].m_id == m_requiredItems[i].m_id && m_items[j].m_quantity >= m_requiredItems[i].m_quantity)
                    {
                        m_objectBeingDragged.SetActive(true);
                        InitializeItemPositionOnUI(m_objectBeingDragged, m_playerItemSlotParent);
                        m_hasResource = true;
                        break;
                    }
                }
                if (!m_hasResource)
                {
                    return;
                }
            }

            m_placedResourcesSlotNumber = new Dictionary<int, int>();
            m_playerInventory.RemoveFromInventory(m_requiredItems);
            m_structureToFinalize.Upgrade(true);
            m_structureToFinalize.GetComponent<InteractiveStructure>().m_hasConvertResource = true;
            ExitUIScreen();
            //ReturnToUpgradeScreen();
        }

    }

    public void ExitUIScreen()
    {
        BaseUIController.Instance.EnableMenu(0);
    }

    private void ReturnToUpgradeScreen()
    {
        BaseUIController.Instance.EnableMenu(4);
    }

    private void RemoveDropableUIScript()
    {
        foreach (GameObject _gameObject in m_playerItemSlotUI)
        {
            Destroy(_gameObject.GetComponent<DropableUI>());
        }
    }

    public void EmptyItemSlot(int _slotNumber)
    {
        m_playerItemSlotUI[_slotNumber].GetComponentsInChildren<Image>()[1].sprite = null;
    }

    private void OnResourceDown(PointerEventData data, int _slotNumber)
    {
        m_chosenResourceInventoryIndex = -1;
        if (m_placedResourcesSlotNumber.ContainsKey(_slotNumber))
        {
            m_chosenResourceInventoryIndex = m_placedResourcesSlotNumber[_slotNumber];
        }
        m_popUpFlag = true;
    }

    protected override void OnDown(PointerEventData data, GameObject _slotObj, int _slotNumber, bool _isPlayerInventory)
    {
        base.OnDown(data, _slotObj, _slotNumber, _isPlayerInventory);
        m_chosenResourceInventoryIndex = _slotNumber;
       /* InventoryItem m_resource = m_playerInventory.m_inventoryItems[_slotNumber];
        if (CraftableItemsList.Instance.m_list.FindItemWithExactRequirements(m_resource.m_id) != null)
        {
            m_resourceDropUISlots[m_chosenItemSlotNumber].GetComponent<Button>().Select();
        }*/
    }

    private void OnResourceBeginDrag(PointerEventData data)
    {
        if (m_chosenResourceInventoryIndex == -1 || m_objectBeingDragged == null)
        {
            return;
        }
        m_playerItemSlotUI[m_chosenResourceInventoryIndex].SetActive(true);

        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 0;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        m_objectBeingDragged = m_playerItemSlotUI[m_chosenResourceInventoryIndex];
    }

    private void OnResourceDrag(PointerEventData data)
    {
        if (m_chosenResourceInventoryIndex == -1 || m_objectBeingDragged == null)
        {
            return;
        }
        m_objectBeingDragged.transform.position = m_inputPosition;
    }

    private void OnResourceEndDrag(PointerEventData data, int _slotNumber)
    {
        if (m_chosenResourceInventoryIndex == -1 || m_objectBeingDragged == null)
        {
            return;
        }
        m_objectBeingDragged.transform.SetParent(m_playerItemSlotParent.transform);
        m_objectBeingDragged.GetComponent<Image>().raycastTarget = true;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 1;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        m_objectBeingDragged.SetActive(true);
        m_placedResourcesSlotNumber.Remove(_slotNumber);
       // m_resourceDropUISlots[_slotNumber].GetComponentsInChildren<Image>()[1].sprite = m_defaultEmptySprite;
        ImageAlphaAdjustResourceDrop(_slotNumber, true);

        m_eventSystem.SetSelectedGameObject(null);

        m_chosenResourceInventoryIndex = -1;
        m_currentBuildTimer = -1;
        m_timer.text = "";
        m_popUpFlag = false;
    }

    private void ImageAlphaAdjustResourceDrop(int _slotNumber, bool _isMakeHalfTransparent)
    {
        Color m_slotUIColor = m_resourceDropUISlots[_slotNumber].GetComponentsInChildren<Image>()[1].color;
        // m_resourceDropUISlots[m_chosenResourceInventoryIndex].GetComponentsInChildren<Image>()[1].color = 

        if (_isMakeHalfTransparent)
        {
            m_resourceDropUISlots[_slotNumber].GetComponentsInChildren<Image>()[1].color = new Color(m_slotUIColor.r, m_slotUIColor.g, m_slotUIColor.b, .5f);
        } else
        {
            m_resourceDropUISlots[_slotNumber].GetComponentsInChildren<Image>()[1].color = new Color(m_slotUIColor.r, m_slotUIColor.g, m_slotUIColor.b, 1);
        }
    }

    protected override IEnumerator ReindexEventArraySlots(List<GameObject> _sourceUI, bool _isPlayerInventory)
    {
        for (int i = 0; i < _sourceUI.Count; i++)
        {
            Destroy(_sourceUI[i].GetComponent<EventTrigger>());
            _sourceUI[i].AddComponent(typeof(EventTrigger));
            yield return new WaitForEndOfFrame();
        }
        InitializeResourcesDropEvent();
    }

    private void InitializeResourcesDropEvent()
    {
        for (int i = 0; i < m_structureToFinalize.m_requiredResources.Count; i++)
        {
            EventTrigger.Entry entry = new EventTrigger.Entry();
            int m_index = i;
            EventTrigger _trigger = m_resourceDropUISlots[m_index].GetComponent<EventTrigger>();

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.Drop;
            entry.callback.AddListener((data) =>
            {
                OnResourceDrop(m_index);
            });

            _trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            entry.callback.AddListener((data) =>
            {
                OnResourceDown((PointerEventData)data, m_index);
            });
            _trigger.triggers.Add(entry);


            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.BeginDrag;
            entry.callback.AddListener((data) =>
            {
                OnResourceBeginDrag((PointerEventData)data);
            });
            _trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.Drag;
            entry.callback.AddListener((data) =>
            {
                OnResourceDrag((PointerEventData)data);
            });
            _trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.EndDrag;
            entry.callback.AddListener((data) =>
            {
                OnResourceEndDrag((PointerEventData)data, m_index);
            });
            _trigger.triggers.Add(entry);
        }
    }
}
