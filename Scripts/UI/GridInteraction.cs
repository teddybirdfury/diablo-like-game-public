﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GridInteraction : MonoBehaviour {

    public static GridInteraction Instance;

    GridTerrain m_gridTerrain;
    GridData m_gridData;
    public float m_yOffset = 0;
    public bool m_isBuildMode = false;
    public GameObject m_previewObject;

    #region gridSelector

    private Material m_lineMaterial;
    private RaycastHit m_lastObjectSelected;
    private Color m_lastObjectColor;

    public GameObject m_objectToCreate;

    #endregion

    private LayerMask m_spawnableLayerMask;
    private LayerMask m_previewLayerMask;
    private LayerMask m_gridTile;
    
    private bool isPreviewMode = false;
    private bool hasSelected = false;

    GridTerrain.DIRECTION buildDirection = GridTerrain.DIRECTION.NONE;

    public Vector3 m_originalScale;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        m_spawnableLayerMask = 1 << LayerMask.NameToLayer(StringConstants.SPAWNABLE);
        m_gridTile = 1 << LayerMask.NameToLayer(StringConstants.GRID_TILE);

        //buildDirection = GridTerrain.DIRECTION.NORTH;
        m_gridTerrain = GetComponent<GridTerrain>();
        m_gridData = GetComponent<GridData>();
        //BuildPreviewInitialize(true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            buildDirection = GridTerrain.DIRECTION.NORTH;
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            buildDirection = GridTerrain.DIRECTION.SOUTH;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            buildDirection = GridTerrain.DIRECTION.WEST;
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            buildDirection = GridTerrain.DIRECTION.EAST;
        }

        if (m_gridTerrain == null || !m_isBuildMode)
        {
            return;
        }

        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        Vector3 m_position = Input.mousePosition;
        if (Input.touchCount > 0)
        {
            Touch m_tounchInput = Input.touches[0];
            if (m_tounchInput.phase == TouchPhase.Began
                || m_tounchInput.phase == TouchPhase.Moved
                || m_tounchInput.phase == TouchPhase.Stationary)
            {
                m_position = m_tounchInput.position;
                RaycastUIControls(m_position);
            }
        } else
        {
            RaycastUIControls(m_position);
        }

    }

    private void RaycastUIControls(Vector3 _position)
    {
        Vector3 m_originSize = m_objectToCreate.GetComponent<Renderer>().bounds.size;
        Vector3 m_offsetHeight = new Vector3(0, m_originSize.y / 2, 0);

        RaycastHit hitInfo;
        Ray ray = Camera.main.ScreenPointToRay(_position);

        int layer_mask = LayerMask.GetMask(StringConstants.GRID_TILE);
        // int isLayerMaskExcluded = ((1 << hitInfo.transform.gameObject.layer) & gridTile); //if 0 then excluded
        if (Physics.Raycast(ray, out hitInfo, 100, layer_mask))
        {
            Vector3 m_finalPosition = m_gridTerrain.GetNearestPointOnGrid(hitInfo.point, buildDirection) + m_offsetHeight;
            GridSelector(m_finalPosition, hitInfo);

            if (Input.GetMouseButtonDown(0))
            {
                int excludedLayer = ((1 << hitInfo.transform.gameObject.layer) & m_previewLayerMask); //if 0 then excluded
                if (excludedLayer == 0 && IsObjectExistsOnPosition(m_finalPosition) == null && BuildUIController.Instance.CreateStructure())
                {
                    ObjectPlacement(m_finalPosition);
                }
            }
            else if (Input.touchCount > 0)
            {
                Touch m_tounchInput = Input.touches[0];
                if (m_tounchInput.phase == TouchPhase.Ended)
                {
                    int excludedLayer = ((1 << hitInfo.transform.gameObject.layer) & m_previewLayerMask); //if 0 then excluded
                  
                    if (excludedLayer == 0 && IsObjectExistsOnPosition(m_finalPosition) == null && BuildUIController.Instance.CreateStructure())
                    {
                        ObjectPlacement(m_finalPosition);
                    }
                }
            }

            int isLayerMaskExcluded = ((1 << hitInfo.transform.gameObject.layer) & m_spawnableLayerMask); //if 0 then excluded
            if (isPreviewMode && !hasSelected && isLayerMaskExcluded == 0)
            {
                // finalPosition += grid.GetNearestPointOnGrid(hitInfo.point, buildDirection);
                BuildPreview(m_finalPosition);
            }
        }
    }

    public void DisableUI()
    {
        GameObject.Destroy(m_previewObject);
        m_previewObject = null;
    }

    public void BuildPreviewInitialize(bool isOn, GameObject objectToPreview = null)
    {
        if (isOn && objectToPreview != null)
        {
            GameObject createdObject = GameObject.Instantiate(objectToPreview);
            m_previewObject = createdObject;
            m_previewObject.layer = m_previewLayerMask;
        } else if(isOn && m_previewObject != null){

            m_previewObject = GameObject.Instantiate(m_previewObject);
            m_previewObject.layer = m_previewLayerMask;
        }
        m_previewObject.tag = StringConstants.UNTAGGED;
        isPreviewMode = isOn;
    }

    private void BuildPreview(Vector3 finalPosition)
    {
        if (m_previewObject == null)
        {
            return;
        }
        else if(isPreviewMode && m_previewObject != null)
        {
            Vector3 m_eulerRotation = GridTerrain.Instance.GetRotationDirection(buildDirection);
            m_previewObject.transform.position = finalPosition;
            m_previewObject.transform.localEulerAngles = m_eulerRotation;
          
           /* m_previewObject.transform.localScale = new Vector3(m_originalScale.x * m_gridTerrain.size,
                m_originalScale.y * m_gridTerrain.size,
                m_originalScale.z * m_gridTerrain.size);*/
            m_previewObject.transform.localScale = new Vector3(m_originalScale.x,
               m_originalScale.y,
               m_originalScale.z);
        }
    }

    private void GridSelector(Vector3 mousePosition, RaycastHit hitInfo)
    {
        int isLayerMaskExcluded = ((1 << hitInfo.transform.gameObject.layer) & m_gridTile); //if 0 then excluded
        if (isLayerMaskExcluded == 0 || !Utility.HasComponent<Renderer>(hitInfo.transform.gameObject))
            {
                return;
            }
     
            if (m_lastObjectSelected.transform != null)
            {
                m_lastObjectSelected.transform.GetComponent<Renderer>().material.SetColor("_Color", m_lastObjectColor);
            }
      
            m_lastObjectSelected = hitInfo;
            m_lastObjectColor = hitInfo.transform.GetComponent<Renderer>().material.GetColor("_Color");

            hitInfo.transform.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
    }

    private GameObject IsObjectExistsOnPosition(Vector3 _position)
    {
        Collider[] hitColliders = Physics.OverlapSphere(_position, 0.25f, m_spawnableLayerMask);
        int i = 0;
        while (i < hitColliders.Length)
        {
            return hitColliders[i].gameObject;
        }
        return null;
    }
    
    private void ObjectPlacement(Vector3 finalPosition)
    {
    
       /* GameObject obj = IsObjectExistsOnPosition(finalPosition);

        if (obj != null)
        {
            m_gridData.RemoveSpawnedObject(obj);
        }
        else
        {
            GameObject createdObject = GameObject.Instantiate(m_objectToCreate);
            Vector3 m_eulerRotation = GridTerrain.Instance.GetRotationDirection(buildDirection);
            createdObject.transform.position = finalPosition;
            createdObject.transform.localScale = new Vector3(m_originalScale.x * m_gridTerrain.size,
                m_originalScale.y * m_gridTerrain.size,
                m_originalScale.z * m_gridTerrain.size);
            createdObject.transform.eulerAngles = m_eulerRotation;
            GridData.Instance.AddSpawnedObject(createdObject);
        }*/

        GameObject createdObject = GameObject.Instantiate(m_objectToCreate);
        Vector3 m_eulerRotation = GridTerrain.Instance.GetRotationDirection(buildDirection);
        createdObject.transform.position = finalPosition;
      /*  createdObject.transform.localScale = new Vector3(m_originalScale.x * m_gridTerrain.size,
            m_originalScale.y * m_gridTerrain.size,
            m_originalScale.z * m_gridTerrain.size);*/
        createdObject.transform.localScale = new Vector3(m_originalScale.x,
          m_originalScale.y,
          m_originalScale.z);
        createdObject.transform.eulerAngles = m_eulerRotation;
        GridData.Instance.AddSpawnedObject(createdObject);
    }
}
