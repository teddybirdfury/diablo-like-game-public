﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BaseUIController : MonoBehaviour {

    public static BaseUIController Instance;

    public GameObject[] m_menus;
    private GameObject m_currentMenu;

    public enum MENU_TYPES
    {
        INVENTORY = 0,
        IN_GAME
    }

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        m_currentMenu = m_menus[0];
        if (SceneManager.GetActiveScene().name == StringConstants.MAIN_SCENE)
        {
            EnableMenu(0); //Load in game UI
           // EnableMenu(7); //For testing
        }
    }

    private void RepositionOutsideGameView(int _index, bool _isOffscreen)
    {
        if (_isOffscreen)
        {
            m_menus[_index].GetComponent<RectTransform>().anchoredPosition = new Vector2(5000, 0);
        } else
        {
            m_menus[_index].GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        }
    }

    public void EnableMenu(int _menuNum)
    {
        m_currentMenu = m_menus[_menuNum];
        for(int i = 0; i < m_menus.Length; i++)
        {
            if (i != _menuNum)
            {
                if (i == 1)
                {
                    RepositionOutsideGameView(i, true);
                }
                else
                {
                    m_menus[i].SetActive(false);
                }
            }
        }
        if(_menuNum == 1)
        {
            RepositionOutsideGameView(1, false);
        } else
        {
            m_menus[_menuNum].SetActive(true);
        }
      
    }

    public void EnableMenus(string _menuNums)
    {
        string[] m_menuStrings = _menuNums.Split(',');
        
        for (int i = 0; i < m_menus.Length; i++)
        {
            m_menus[i].SetActive(false);
        }

        for (int j = 0; j < m_menuStrings.Length; j++)
        {
            int m_val = -1;
            int.TryParse(m_menuStrings[j], out m_val);
            if (m_val == 1)
            {

                RepositionOutsideGameView(j, true);
            }
            m_menus[m_val].SetActive(true);
        }
    }

    public void DisableMenu(int _menuNum)
    {
        m_menus[_menuNum].SetActive(false);
    }

    public void DisableCurrentMenu()
    {
        m_currentMenu.SetActive(false);
    }
}
