﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseInteractiveDetectArea : MonoBehaviour
{

    public Transform m_closestDetectedInteractive;
    public List<Transform> m_detectedInteractives = new List<Transform>();
    protected bool m_isChangeTarget = false;
    protected List<InteractiveObject.InteractiveTypes> m_interactiveTypeToDetect = new List<InteractiveObject.InteractiveTypes>();

    #region Handlers

    public delegate void InteractiveDetectedHandler();

    public event InteractiveDetectedHandler InteractiveDetectedHandle;

    public void InteractiveDetected()
    {
        if (InteractiveDetectedHandle != null)
        {
            InteractiveDetectedHandle.Invoke();
        }
    }

    #endregion

    public virtual void RemoveDetectedFromList(Transform _transform)
    {
        m_detectedInteractives.Remove(_transform);
        m_closestDetectedInteractive = GetClosesDetectedObject();
        m_isChangeTarget = true;
    }

    protected Transform GetClosesDetectedObject(Transform m_detectedInteractive = null)
    {
        Transform m_currentClosest = null;
        int m_detectedCount = m_detectedInteractives.Count;

        if (m_detectedCount > 0)
        {
            for (int i = 0; i < m_detectedCount; i++)
            {
                if (m_currentClosest == null)
                {
                    m_currentClosest = m_detectedInteractives[i].transform;
                    continue;
                }

                float _distanceToDetectedObj = Vector3.Distance(transform.position, m_detectedInteractives[i].transform.position);
                float _distanceToLastClosestObject = Vector3.Distance(transform.position, m_currentClosest.transform.position);
                if (_distanceToDetectedObj < _distanceToLastClosestObject)
                {
                    m_currentClosest = m_detectedInteractives[i].transform;
                }
            }
        }
        else
        {
            m_currentClosest = m_detectedInteractive;
        }
        return m_currentClosest;
    }

    public virtual void OnTriggerEnter(Collider _collider)
    {
        Transform m_detectedInteractive = _collider.transform;
        if (m_detectedInteractive.tag == StringConstants.INTERACTIVE
            &&  m_interactiveTypeToDetect.Contains(m_detectedInteractive.GetComponentInParent<InteractiveObject>().m_type))
        {
            if (!m_detectedInteractives.Contains(m_detectedInteractive))
            {
                m_detectedInteractives.Add(m_detectedInteractive);
                m_closestDetectedInteractive = GetClosesDetectedObject(m_detectedInteractive);
                InteractiveDetected();
            }
        }
    }

    public virtual void OnTriggerStay(Collider _collider)
    {
        Transform m_transform = _collider.transform;
        if (m_closestDetectedInteractive != null || (m_isChangeTarget && m_detectedInteractives.Count > 0))
        {
            m_isChangeTarget = false;
            m_closestDetectedInteractive = GetClosesDetectedObject();
            InteractiveDetected();
        }
        else if (m_closestDetectedInteractive == null
            && m_transform.tag == StringConstants.INTERACTIVE
            && m_interactiveTypeToDetect.Contains(m_transform.GetComponentInParent<InteractiveObject>().m_type))
        {
            m_closestDetectedInteractive = _collider.transform;
            InteractiveDetected();
        }
    }

    public virtual void OnTriggerExit(Collider _collider)
    {
        Transform m_transform = _collider.transform;
        if (m_detectedInteractives.Count > 0 && m_detectedInteractives.Contains(m_transform))
        {
            m_detectedInteractives.Remove(m_transform);
        } else if (m_detectedInteractives.Count == 0)
        {
            m_closestDetectedInteractive = null;
        }
    }
}
