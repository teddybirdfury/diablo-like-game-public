﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UpgradeableStructure))]
public class InteractiveStructure : InteractiveObject {
    
    [HideInInspector]
    public UpgradeableStructure m_upgradeableStructure;
    public bool m_hasConvertResource = false;
    public bool m_hasUpgrade = true;

    void Start()
    {
        m_upgradeableStructure = GetComponent<UpgradeableStructure>();
        m_upgradeableStructure.UpgradeHandle += () => {
            if (m_upgradeableStructure.m_maxedOut) {
                m_hasUpgrade = false;
            } else
            {
                m_hasConvertResource = true;
                m_hasUpgrade = true;
            }
        };
    }
}
