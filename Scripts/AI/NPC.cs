﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour {

    public string m_dialogue = "";
    public Loot m_loot;

    public void StartDialogue()
    {
        InGameUIController.Instance.SetDialogue(m_dialogue);
        InGameUIController.Instance.ToggleDialogueUI(true);
    }

}
