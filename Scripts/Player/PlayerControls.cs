﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerControls : MonoBehaviour {

    private Image m_controlArea;
    private PlayerMovement m_playerMovement;
    private Vector2 m_touchOrigin = -Vector2.one;
    public bool m_enableControls = true;

    private GraphicRaycaster m_graphicRaycaster;
    private bool m_isControllingJoystick = false;
    private bool m_isStaticJoystick = true;

    void Start()
    {
        m_playerMovement = GetComponent<PlayerMovement>();
        m_controlArea = InGameUIController.Instance.m_controlArea;
        GameObject m_canvas = GameObject.FindGameObjectWithTag(StringConstants.CANVAS);
        if (m_canvas != null)
        {
            m_graphicRaycaster = m_canvas.GetComponent<GraphicRaycaster>();
        }
    }

    void Update()
    {
        if (!m_enableControls)
        {
            return;
        }
        //Check if we are running either in the Unity editor or in a standalone build.
#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
       // PCInputs();
        PCJoystick();
        //Check if we are running on iOS, Android, Windows Phone 8 or Unity iPhone
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE || UNITY_EDITOR
        //TouchInputs();
        MobileJoystickInputs();
#endif //End of mobile platform dependendent compilation section started above with #elif

    }

    void Turning(Vector3 _position)
    {
        Ray camRay = Camera.main.ScreenPointToRay(_position);
        RaycastHit floorHit;
        if (Physics.Raycast(camRay, out floorHit, 100f))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            m_playerMovement.SetRotation(newRotation);
        }
    }

    void Turning(float _angle)
    {
        m_playerMovement.SetRotation(_angle);
    }

    private void ToggleMovement(bool _isEnable)
    {
        m_playerMovement.m_isMove = _isEnable;
    }

    #region Mobile joystick
    void RaycastUIControls(Vector3 _position, bool _isBeginTouch = false)
    {
        if (m_graphicRaycaster == null)
        {
            return;
        }
        PointerEventData m_PointerEventData;
        m_PointerEventData = new PointerEventData(EventSystem.current);
        m_PointerEventData.position = _position;
        List<RaycastResult> m_results = new List<RaycastResult>();
        m_graphicRaycaster.Raycast(m_PointerEventData, m_results);
        foreach (RaycastResult _result in m_results)
        {
            if (_result.gameObject.tag == StringConstants.JOYSTICK_AREA)
            {
                if (_isBeginTouch)
                {
                    InGameUIController.Instance.ToggleJoystick(true);
                    if (m_isStaticJoystick)
                    {
                        m_touchOrigin = InGameUIController.Instance.m_joystickThumb.transform.position;
                    }
                    InGameUIController.Instance.JoystickControl(m_touchOrigin, m_isStaticJoystick, true);
                    m_isControllingJoystick = true;
                }
                LookMove(_position);
            }
        }

        if(m_isControllingJoystick) {
            InGameUIController.Instance.JoystickControl(_position, m_isStaticJoystick);
            LookMove(_position);
        } else {
            ToggleMovement(false);
        }
    }

    private void LookMove(Vector3 _position)
    {
        float m_lookAngle = Utility.AngleTo(m_touchOrigin, _position, -45);
        Turning(m_lookAngle);
        ToggleMovement(true);
    }

    private void MobileJoystickInputs()
    {
        if (Input.touchCount > 0)
        {
            Touch m_tounchInput = Input.touches[0];
            int m_id = m_tounchInput.fingerId;

            if (m_tounchInput.phase == TouchPhase.Began)
            {
                m_touchOrigin = m_tounchInput.position;
                RaycastUIControls(m_tounchInput.position, true);
            }

            if(m_tounchInput.phase == TouchPhase.Moved || m_tounchInput.phase == TouchPhase.Stationary)
            {
                RaycastUIControls(m_tounchInput.position);
            }

            if (m_tounchInput.phase == TouchPhase.Ended)
            {
                m_isControllingJoystick = false;
                ToggleMovement(false);
                InGameUIController.Instance.ToggleJoystick(false);
            }

        }
        
    }
    #endregion

    #region PC inputs

    private void PCJoystick()
    {
       
        if (Input.GetMouseButtonDown(0))
        {
            m_touchOrigin = Input.mousePosition;
            RaycastUIControls(Input.mousePosition, true);
        }
        if (Input.GetMouseButton(0))
        {
            //m_lookAngle = Vector3.Angle(transform.position, Camera.ScreenToWorldPoint(Input.mousePosition));
            RaycastUIControls(Input.mousePosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            m_isControllingJoystick = false;
            ToggleMovement(false);
            InGameUIController.Instance.ToggleJoystick(false);
        }
    }

    private void PCInputs()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetMouseButton(0))
            {
                //m_lookAngle = Vector3.Angle(transform.position, Camera.ScreenToWorldPoint(Input.mousePosition));
                Turning(Input.mousePosition);
                ToggleMovement(true);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                ToggleMovement(false);
            }
        }
    }
    #endregion

    #region Touch Control Inputs
    private void TouchInputs()
    {
        if (Input.touchCount > 0)
        {
            Touch m_tounchInput = Input.touches[0];
            int m_id = m_tounchInput.fingerId;

            if (!EventSystem.current.IsPointerOverGameObject(m_id))
            {
                if (m_tounchInput.phase == TouchPhase.Began
                    || m_tounchInput.phase == TouchPhase.Moved
                    || m_tounchInput.phase == TouchPhase.Stationary)
                {
                    m_touchOrigin = m_tounchInput.position;
                    float m_lookAngle = Vector2.Angle(m_touchOrigin, m_tounchInput.position);
                    transform.localEulerAngles = new Vector3(0, m_lookAngle, 0);
                    Turning(m_tounchInput.position);
                    ToggleMovement(true);
                }
                else
                {
                    ToggleMovement(false);
                }
            }
        }
    }
    #endregion
}
