﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BlueprintsUIController : DynamicUI {

    public static BlueprintsUIController Instance;

    PlayerActions m_player;
    Inventory m_inventory;

    public List<CraftableItem> m_craftableItems = new List<CraftableItem>();

    public RectTransform m_contentContainer; //Should scale according to number of elements
    public RectTransform m_contentItemPrefab;
    private int m_offset = 5;

    public Image m_itemIcon;
    public Text m_itemName;
    public Text m_description;

    public GameObject m_craftRequirementsParent;
    public List<GameObject> m_craftRequirementsUI = new List<GameObject>();

    public CraftableItem m_selectedToCraft;
    public Button m_craftButton;

    void Awake()
    {
        Instance = this;
    }

    public void Initialize(PlayerActions _player)
    {
        m_player = _player;
        m_inventory = m_player.m_inventory;
        InitializeCraftRequirementsUI();
        StartCoroutine(InitializeBlueprints());
        m_selectedToCraft = m_craftableItems[0];
    }

    public void Initialize()
    {
        if (m_craftableItems.Count > 0)
        {
            m_selectedToCraft = m_craftableItems[0];
            OnSelectedItem(m_selectedToCraft);
        }
    }

    private void InitializeCraftRequirementsUI()
    {
        Image[] m_images = m_craftRequirementsParent.GetComponentsInChildren<Image>();
        for (int i = 0; i < m_images.Length; i++)
        {
            if (m_images[i].transform.parent == m_craftRequirementsParent.transform)
            {
                m_craftRequirementsUI.Add(m_images[i].gameObject);
            }

        }
    }

    private IEnumerator InitializeBlueprints()
    {
        int m_currentRow = 0;
        float m_sizeContainerOffset = 0;
        RectTransform m_createdPrefab;

        Image[] m_images = m_contentContainer.GetComponentsInChildren<Image>();
        EventTrigger[] m_triggers = m_contentContainer.GetComponentsInChildren<EventTrigger>();
        int m_rows = Mathf.CeilToInt(m_craftableItems.Count / 5f);
        base.AdjustContentPanelSize(m_contentContainer, m_contentItemPrefab, m_rows, 3, m_offset);

        for (int i = 0; i < m_craftableItems.Count; i++)
        {
            m_currentRow = i / 5;
            m_createdPrefab = GameObject.Instantiate(m_contentItemPrefab, m_contentContainer.transform);
            m_sizeContainerOffset = m_contentContainer.GetComponent<RectTransform>().sizeDelta.y;
            m_createdPrefab.anchoredPosition = new Vector2((m_createdPrefab.sizeDelta.x / 2) + m_createdPrefab.sizeDelta.x * (i % 5) + m_offset * (i % 5),
                m_sizeContainerOffset - (m_createdPrefab.sizeDelta.y / 2) - (m_createdPrefab.sizeDelta.y * m_currentRow) - (m_offset * m_currentRow));
            SetPrefabImage(m_createdPrefab.gameObject, m_craftableItems[i]);
            SetPrefabButtonEvent(m_createdPrefab.gameObject, m_craftableItems[i]);
        }
        yield return new WaitForEndOfFrame();
    }


    private void SetPrefabButtonEvent(GameObject _prefab, CraftableItem _item)
    {
        Button m_button = _prefab.GetComponent<Button>();
        m_button.onClick.AddListener(delegate { OnSelectedItem(_item); });
    }

    private void OnSelectedItem(CraftableItem _item) {
        m_selectedToCraft = _item;
        m_itemIcon.sprite = _item.m_itemSprite;
        m_itemName.text = _item.m_name;
        m_description.text = _item.m_description;
        SetCraftableRequirements();
        SetIfCraftableUI();
    }

    private void SetPrefabImage(GameObject _prefab, CraftableItem _item)
    {
        Image[] m_images = _prefab.GetComponentsInChildren<Image>();
        for (int i = 0; i < m_images.Length; i++)
        {
            if (m_images[i].transform != _prefab.transform)
            {
                m_images[i].sprite = _item.m_itemSprite;
            }
        }
    }

    private void SetCraftableRequirements()
    {
        if(m_selectedToCraft.m_requiredResources.Count == 0)
        {
            return;
        }
        for(int i = 0; i < m_selectedToCraft.m_requiredResources.Count; i++)
        {
            Transform m_transform = GetComponentInChildren(m_craftRequirementsUI[i].transform);
            m_transform.GetComponent<Image>().sprite = m_selectedToCraft.m_requiredResources[i].m_itemSprite;
            m_craftRequirementsUI[i].transform.GetComponentInChildren<Text>().text = m_selectedToCraft.m_requiredResources[i].m_quantity.ToString();

        }
    }

    private void InitializeEvents(EventTrigger _trigger, int _slotNumber)
    {

    }

    public void SetIfCraftableUI()
    {
        if (m_player.IsCraftable(m_selectedToCraft)) {
            m_craftButton.interactable = true;
        }
        else
        {
            m_craftButton.interactable = false;
        }
    }

    public void Craft()
    {
        m_player.Craft(m_selectedToCraft);
        SetIfCraftableUI();
    }
}
