﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetLayerNumber : MonoBehaviour {
    
	void Start () {
        int layerMask = 1 << LayerMask.NameToLayer(StringConstants.SPAWNABLE);
        Debug.Log("Name: " + gameObject.name + " Layer Number: " + gameObject.layer);
	}

}
