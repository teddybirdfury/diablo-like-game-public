﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GlobalMapControls : MonoBehaviour {

    public Transform m_player;
    private PlayerMovement m_playerMovement;
    public Transform m_chosenAreaObject;
    public Transform m_currentAreaObject;
    private bool m_isMoving = false;

    private float m_mapTimer = 3;
    private float m_currentMapTimer = 0;
    private Vector3 m_startPosition;

    public float m_lerpTime = 0;

    void Start()
    {
        m_player = GameObject.FindGameObjectWithTag(StringConstants.PLAYER).transform;
        m_playerMovement = m_player.GetComponent<PlayerMovement>();
        m_currentMapTimer = PlayerPrefs.GetFloat(StringConstants.GLOBAL_MAP_TIMER);
        MovePlayerToCurrentMap();
        m_currentAreaObject = GetCurrentMap();
        m_player.position = m_currentAreaObject.transform.position;

        if (IsPlayerOnChosenMap(m_currentAreaObject))
        {
            GlobalMapUIController.Instance.ToggleEnterButton(true);
        }
    }

    private Transform GetCurrentMap()
    {
        string m_chosenMapName = PlayerPrefs.GetString(StringConstants.CURRENT_AREA);
        foreach(GameObject _mapObject in GameObject.FindGameObjectsWithTag(StringConstants.MAP_OBJECT))
        {
            LocalMapAreaObject m_mapAreaObject = _mapObject.GetComponent<LocalMapAreaObject>();
            if (m_mapAreaObject != null && m_mapAreaObject.m_mapName == m_chosenMapName)
            {
                return _mapObject.transform;
            }
        }
        return null;
    }

    private void MovePlayerToCurrentMap()
    {
        m_player.position = GetCurrentMap().position;
    }

    void Update () {
        if (!m_isMoving)
        {
            RaycastHit hit;
#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)
                    && hit.transform.tag == StringConstants.MAP_OBJECT)
                {
                    ChooseArea(hit.transform);
                } else if(!EventSystem.current.IsPointerOverGameObject())
                {
                   // GlobalMapUIController.Instance.ToggleEnterButton(false);
                }
            }
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE || UNITY_EDITOR
         if (Input.touchCount > 0)
        {
            //Store the first touch detected.
            Touch m_tounchInput = Input.touches[0];

            //Check if the phase of that touch equals Began
            if (m_tounchInput.phase == TouchPhase.Began)
            {
                int m_id = m_tounchInput.fingerId;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(m_tounchInput.position), out hit)
                    && hit.transform.tag == StringConstants.MAP_OBJECT)
                {
                    ChooseArea(hit.transform);
                }
                else if (!EventSystem.current.IsPointerOverGameObject(m_id))
                {
                    //GlobalMapUIController.Instance.ToggleEnterButton(false);
                }
            }
        }
#endif
        }
        else
        {
            m_currentMapTimer += Time.deltaTime;
            m_lerpTime = m_currentMapTimer / m_mapTimer;
            m_player.transform.position = Vector3.Lerp(m_startPosition, m_chosenAreaObject.transform.position, m_lerpTime);
            UpdateTravelTimeUI();
            if (m_currentMapTimer >= m_mapTimer)
            {
                m_isMoving = false;
                m_currentAreaObject = m_chosenAreaObject;
                GlobalMapUIController.Instance.m_currentTravelTime.text = "";
                GlobalMapUIController.Instance.ToggleEnterButton(true);
            }
        }
    }

    private void UpdateTravelTimeUI()
    {
        float m_remainingTime = m_mapTimer - m_currentMapTimer;
        if (m_remainingTime > 0)
        {
            GlobalMapUIController.Instance.m_currentTravelTime.text = m_remainingTime.ToString("F2");
        }
    }

    public bool IsPlayerOnChosenMap(Transform m_map)
    {
        string m_currentMapName = PlayerPrefs.GetString(StringConstants.CURRENT_AREA);
        string m_chosenMapName = m_map.GetComponent<LocalMapAreaObject>().m_mapName;
       // Debug.Log("Current map: " + m_currentMapName + " Chosen map: " + m_chosenMapName + " Same: " + (m_currentMapName == m_chosenMapName));
        return m_currentMapName == m_chosenMapName;
    }

    public void ChooseArea(Transform _chosenArea)
    {
        m_chosenAreaObject = _chosenArea;
        m_startPosition = m_player.transform.position;
        if (IsPlayerOnChosenMap(m_chosenAreaObject))
        {
            GlobalMapUIController.Instance.ToggleMovePanel(false);
        } else
        {
            GlobalMapUIController.Instance.m_walkTravelTime.text = m_mapTimer.ToString("F2");
            GlobalMapUIController.Instance.ToggleMovePanel(true);
        }
    }

    public void MoveToMap()
    {
        Transform m_chosenObj = m_chosenAreaObject.transform;
        m_playerMovement.LookTowards(m_chosenObj.position);
        m_currentMapTimer = 0;
        m_isMoving = true;
        PlayerPrefs.SetString(StringConstants.CURRENT_AREA, m_chosenObj.GetComponentInChildren<LocalMapAreaObject>().m_mapName);
        GlobalMapUIController.Instance.ToggleEnterButton(false);
    }

    public void LoadLocalMap()
    {
        SceneManager.LoadScene("main"); //Just add parameters to load other level details
        //SceneManager.LoadScene(m_chosenAreaObject.GetComponentInChildren<LocalMapAreaObject>().m_mapName);
    }

    void OnDestroy()
    {
        PlayerPrefs.SetFloat(StringConstants.GLOBAL_MAP_TIMER, m_mapTimer);
    }
}
