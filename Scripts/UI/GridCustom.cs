﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GridCustom : MonoBehaviour
{
    [SerializeField]
    public float size = 1f;

    public static GridCustom Instance;

    public GameObject m_originPosition;
    public GameObject terrainBlock;

    private Vector3 result;
    private int xCount = 0;
    private int yCount = 0;
    private int zCount = 0;
    private Vector3 offsetPosition;

    private List<GameObject> m_gridTiles;

    public bool m_isBuildMode = false;

    public enum DIRECTION
    {
        NORTH,
        SOUTH,
        WEST,
        EAST,
        NONE
    }

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        m_gridTiles = new List<GameObject>();
        DrawGrid();
        ToggleGridView(false);
    }

    public Vector3 GetOffsetDirection(DIRECTION _direction = DIRECTION.NONE)
    {
        if (_direction != DIRECTION.NONE)
        {
            switch (_direction)
            {
                case DIRECTION.NORTH:
                    offsetPosition = new Vector3(size / 2, 0, 0);
                    break;
                case DIRECTION.SOUTH:
                    offsetPosition = new Vector3(-size / 2, 0, 0);
                    break;
                case DIRECTION.WEST:
                    offsetPosition = new Vector3(0, 0, size / 2);
                    break;
                case DIRECTION.EAST:
                    offsetPosition = new Vector3(0, 0, -size / 2);
                    break;
            }
        }
        else
        {
            offsetPosition = Vector3.zero;
        }
        return offsetPosition;
    }

    public Vector3 GetNearestPointOnGrid(Vector3 position, DIRECTION _direction = DIRECTION.NONE)
    {
        // position -= transform.position;
        position -= m_originPosition.transform.position;
        xCount = Mathf.RoundToInt(position.x / size);
        yCount = Mathf.RoundToInt(position.y / size);
        zCount = Mathf.RoundToInt(position.z / size);

        offsetPosition = GetOffsetDirection(_direction);

        result = new Vector3(
        (float)xCount * size,
        (float)yCount * size,
        (float)zCount * size) + offsetPosition;

        // result += transform.position;
        position += m_originPosition.transform.position;
        return result;
    }

    public void ToggleGridView(bool m_enable)
    {
        m_isBuildMode = m_enable;
        foreach (GameObject _gridTile in m_gridTiles)
        {
            _gridTile.SetActive(m_enable);
        }
    }

    public void DrawGrid()
    {
        Gizmos.color = Color.yellow;
        Vector3 point;
        GameObject _block;
        for (float x = 0; x < 40; x += size)
        {
            for (float z = 0; z < 40; z += size)
            {
                point = GetNearestPointOnGrid(new Vector3(x, 0f, z));
                _block = GameObject.Instantiate(terrainBlock);
                _block.transform.position = point;
                m_gridTiles.Add(_block);
               // Gizmos.DrawSphere(point, 0.1f);
            }

        }
    }

   /* private void OnDrawGizmos()
    {
        Vector3 point;
        Gizmos.color = Color.yellow;
        for (float x = 0; x < 40; x += size)
        {
            for (float z = 0; z < 40; z += size)
            {
                point = GetNearestPointOnGrid(new Vector3(x, 0f, z));
                Gizmos.DrawSphere(point, 0.1f);
            }

        }
    }*/
}