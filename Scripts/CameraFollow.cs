﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    GameObject m_player;

    public float xDistance = 12;
    public float yDistance = 10;
    public float zDistance = 0;

    Vector3 m_defaultRotation;
    private Vector3 velocity = Vector3.zero;

    void Start()
    {
        m_player = GameObject.FindGameObjectWithTag(StringConstants.PLAYER);
        m_defaultRotation = new Vector3(60, -45, 0);

    }

    void Update()
    {
        //transform.position = new Vector3(xDistance, yDistance, zDistance) + player.transform.position;
        transform.position  = Vector3.SmoothDamp(transform.position, new Vector3(xDistance, yDistance, zDistance) + m_player.transform.position, ref velocity, 0.3f);
    }
}
