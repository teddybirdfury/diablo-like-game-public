﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyMovement : CharacterMovement
{
    public GameObject m_target;

    public override void Start()
    {
        base.m_movementSpeed = 3;
    }

    public override void Update()
    {
        if (base.m_enableMovement)
        {
            TurnTowardsTarget();
            transform.Translate(Vector3.forward * m_movementSpeed * Time.deltaTime);
        }
    }

    void TurnTowardsTarget()
    {
        if (m_target != null)
        {
            Vector3 m_lookRotation = m_target.transform.position - transform.position;
            Quaternion m_newRotation = Quaternion.LookRotation(m_lookRotation);
            Vector3 m_angle = m_newRotation.eulerAngles;
            Vector3 m_modifiedRotation = new Vector3(0, m_angle.y, 0);
            transform.localEulerAngles = m_modifiedRotation;
        }
    }
}
