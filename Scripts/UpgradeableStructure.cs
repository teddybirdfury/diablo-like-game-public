﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeableStructure : MonoBehaviour {

    public int m_id = -1;
    public int m_level = 1;

    public List<InventoryItem> m_requiredResources = new List<InventoryItem>();
    public string m_name;
    public bool m_maxedOut = false;

    [Serializable]
    public class RequiredResourcesHierarchy
    {
        public string m_structureName = "";
        public int m_levelRequirement = 1;
        public Sprite m_sprite;
        public List<InventoryItem> m_requiredResources = new List<InventoryItem>();
    }

    public List<RequiredResourcesHierarchy> m_requiredResourcesHierarchy = new List<RequiredResourcesHierarchy>();
    public Sprite m_itemSprite;

    public delegate void UpgradeHandler();

    public event UpgradeHandler UpgradeHandle;

    public void UpgradeStructure()
    {
        if (UpgradeHandle != null)
        {
            UpgradeHandle.Invoke();
        }
    }

    private void Start()
    {
        GetRequireResourcesByLevel();
    }
    
    private void GetRequireResourcesByLevel()
    {
        if (m_level > m_requiredResourcesHierarchy.Count)
        {
            m_maxedOut = true;
            return;
        }
        foreach(RequiredResourcesHierarchy _currentResourceHierarchy in m_requiredResourcesHierarchy)
        {
            if (m_level == _currentResourceHierarchy.m_levelRequirement)
            {
                m_name = _currentResourceHierarchy.m_structureName;
                m_requiredResources = _currentResourceHierarchy.m_requiredResources;
                m_itemSprite = _currentResourceHierarchy.m_sprite;
                return;
            }
        }
    }

    public void Upgrade(bool _isChangeColor = true)
    {
        m_level++;
        if (_isChangeColor)
        {
            GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
            GetRequireResourcesByLevel();
        }
        UpgradeStructure();
    }


}
