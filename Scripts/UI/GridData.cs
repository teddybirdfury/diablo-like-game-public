﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

[Serializable]
public class GridData : MonoBehaviour {

    public static GridData Instance;

    public List<GameObject> m_spawnedObjects;
    public List<string> jsonData;
    public List<GameObject> m_spawnableObjects;

    [Serializable]
    public class GridObjectInfo
    {
        public string m_name;
        public Vector3 m_position;
        public Vector3 m_rotation;
        public Vector3 m_scale;

        public GridObjectInfo(GameObject _gameObject)
        {
            m_name = GetCloneObjectName(_gameObject.transform.name);
            m_position = _gameObject.transform.position;
            m_rotation = _gameObject.transform.eulerAngles;
            m_scale = _gameObject.transform.localScale;
        }

    
    }

    public static string GetCloneObjectName(string objectName)
    {
        return objectName.Replace("(Clone)", "");
    }

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        jsonData = new List<string>();
        GetAllSpawnableObjects();
        LoadObjects();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            //SaveObjects();
        }
    }

    public void AddSpawnedObject(GameObject _obj)
    {
        m_spawnedObjects.Add(_obj);
        SaveObjects();
    }

    public void RemoveSpawnedObject(GameObject _obj)
    {
        GameObject m_temp = _obj;
        m_spawnedObjects.Remove(_obj);
        GameObject.Destroy(m_temp);
        SaveObjects();
    }

    private void OnDestroy()
    {
       // SaveObjects(m_spawnedObjects);
    }

    void SaveObjects(List<GameObject> _spawnedObjects = null)
    {
        jsonData = new List<string>();
        if (_spawnedObjects == null)
        {
            _spawnedObjects = new List<GameObject>(GameObject.FindGameObjectsWithTag(StringConstants.SPAWNABLE));
        }
        foreach(GameObject _spawnedObject in _spawnedObjects)
        {
            GridObjectInfo _gridObject = new GridObjectInfo(_spawnedObject);
            //Debug.Log("_gridObject : " + _gridObject.m_scale);
            jsonData.Add(JsonUtility.ToJson(_gridObject));
        }
        //Debug.Log("Saved Objects : " + jsonData.Count);
        PlayerPrefsX.SetStringArray(StringConstants.GRID_OBJECTS, jsonData.ToArray());
    }

    void GetAllSpawnableObjects()
    {
        var m_spawnable = Resources.LoadAll(StringConstants.SPAWNABLE, typeof(GameObject));
        foreach (GameObject _loadedObj in m_spawnable)
        {
            m_spawnableObjects.Add(_loadedObj);
        }
    }

    void LoadObjects()
    {
        string[] _loadedData = PlayerPrefsX.GetStringArray(StringConstants.GRID_OBJECTS);
        jsonData = new List<string>(_loadedData);
      //  Debug.Log("_loadedData : " + _loadedData.Length + " Content: " + _loadedData);
        if(jsonData != null && jsonData.Count > 0)
        {
            GridObjectInfo _gridObject = null;
            foreach(string _jsonData in jsonData)
            {
                _gridObject = JsonUtility.FromJson<GridObjectInfo>(_jsonData);

                GameObject _loadedObject = null;
                foreach (GameObject _obj in m_spawnableObjects)
                {
                    if(_obj.name == _gridObject.m_name)
                    {
                        _loadedObject = GameObject.Instantiate(_obj);
                        _loadedObject.transform.name = GetCloneObjectName(_loadedObject.transform.name);
                        _loadedObject.transform.position = _gridObject.m_position;
                        _loadedObject.transform.eulerAngles = _gridObject.m_rotation;
                        _loadedObject.transform.localScale = _gridObject.m_scale;
                        break;
                    }
                }

               
                //Instantiate(_loadedObject);
            }
        } else
        {
            Debug.Log("No saved grid data yet");
        }
    }

}
