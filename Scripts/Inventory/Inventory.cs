﻿using System.Collections.Generic;
using UnityEngine;
using System;
using FJL;

[Serializable]
public class Inventory {
    
    public List<InventoryItem> m_inventoryItems = new List<InventoryItem>();
    public List<InventoryItem> m_equipmentItems = new List<InventoryItem>();

    private CharacterActions m_character;

    public Inventory() { }

    public Inventory(CharacterActions _player) {
        m_character = _player;
    }

    //Use for loot system
    public void SetPlayer(CharacterActions _player)
    {
        m_character = _player;
    }

    public bool IsItemExistOnSlot(int _slotNumber, bool _isEquipment = false)
    {
        InventoryItem _item = null;
        if (_isEquipment && m_equipmentItems.Count > _slotNumber)
        {
            _item = m_equipmentItems[_slotNumber];
        } else if(m_inventoryItems.Count > _slotNumber)
        {
            _item = m_inventoryItems[_slotNumber];
        }

        if (_item != null &&_item.m_id != 0)
        {
            return true;
        }
        return false;
    }

    public bool IsEquipmentFromInventory(int _slotNumber)
    {
        if(m_inventoryItems[_slotNumber].m_itemType == InventoryItem.ITEM_TYPE.EQUIPMENT)
        {
            return true;
        }
        return false;
    }

    public List<int> GetEquipmentSlotsWithType(int _slotNumber)
    {
        List<int> m_slotNumbers = new List<int>();
        InventoryItem m_item = m_inventoryItems[_slotNumber];
        InventoryItem.EQUIPMENT_TYPE m_chosenItemEquipmentType = m_item.m_equipmentType;

       // Debug.Log("Slot Number: " + _slotNumber);
        for(int i = 0; i < m_equipmentItems.Count; i++)
        {
            if(m_equipmentItems[i].m_equipmentType == m_chosenItemEquipmentType)
            {
                m_slotNumbers.Add(i);
            }
        }
        return m_slotNumbers;
    }

    public void AddInventoryItem()
    {
        m_inventoryItems.Add(new InventoryItem());
    }

    private bool AddExistingInventoryItem(InventoryItem _item)
    {
        for(int i = 0; i < m_inventoryItems.Count; i++)
        {
            if (m_inventoryItems[i].m_id == _item.m_id)
            {
                m_inventoryItems[i].m_quantity += _item.m_quantity;
                return true;
            }
        }
      
        return false;
    }

    /// <summary>
    /// Adds an item and adds to quantity if it exists
    /// </summary>
    /// <param name="_item"></param>
    public void AddInventoryItem(InventoryItem _item)
    {
       /* if (AddExistingInventoryItem(_item))
        {
            return;
        }*/

		bool m_added = false;

        for(int i = 0; i < m_inventoryItems.Count; i++)
        {
            if(m_inventoryItems[i].m_itemType == InventoryItem.ITEM_TYPE.NONE)
            {
				if (InventoryItem.IsEmptySprite(_item))
				{
					_item.m_itemSprite = InventoryUIController.Instance.m_defaultEmptySprite;
				}

				m_inventoryItems[i] = _item;
				m_added = true;
				break;
            }
			/*
            */
        }

		if (!m_added)
		{
			if (InventoryItem.IsEmptySprite(_item))
			{
				_item.m_itemSprite = InventoryUIController.Instance.m_defaultEmptySprite;
			}

			m_inventoryItems.Add(new InventoryItem(_item));
			//m_inventoryItems[m_inventoryItems.Count-1] = _item;
		}

        return;
    }

    /// <summary>
    /// Adds the item regardless if its existing
    /// </summary>
    /// <param name="_item"></param>
    private void AddNewInventoryItem(InventoryItem _item)
    {
        if (InventoryItem.IsEmptySprite(_item))
        {
            _item.m_itemSprite = InventoryUIController.Instance.m_defaultEmptySprite;
        }

        m_inventoryItems.Add(new InventoryItem(_item));
    }

    public List<Stats> GetTotalStatsFromEquipment()
    {
        List<Stats> m_stats = new List<Stats>();
        foreach (InventoryItem _item in m_equipmentItems)
        {
            if(_item.m_itemStats == null) { continue; }
            foreach(Stats _stats in _item.m_itemStats)
            {
                if (_stats.m_statValue != 0)
                {
                    m_stats.Add(new Stats(_stats.m_statName, _stats.m_statValue));
                }
            }
        }
        return m_stats;
    }

    public void AddEquipmentItem(InventoryItem.EQUIPMENT_TYPE _equipmentType)
    {
        m_equipmentItems.Add(new InventoryItem(_equipmentType));
    }
    
    public bool UseOrEquip(int _slotNumber)
    {
        bool m_itemHasBeenRemoved = false;
        InventoryItem m_inventoryItem = m_inventoryItems[_slotNumber];
        InventoryItem.ITEM_TYPE m_itemType = m_inventoryItem.m_itemType;
        if(m_itemType == InventoryItem.ITEM_TYPE.USEABLE)
        {
            m_character.UseItem(m_inventoryItem);
            if (m_inventoryItem.m_quantity - 1 == 0)
            {
                Debug.Log("Remove inventory item");
                m_inventoryItems.RemoveAt(_slotNumber);
                m_itemHasBeenRemoved = true;
            }
            else
            {
                m_inventoryItem.m_quantity -= 1;
            }
           
        } else if (m_itemType == InventoryItem.ITEM_TYPE.EQUIPMENT)
        {
            InventoryItem.EQUIPMENT_TYPE m_chosenEquipmentType = m_inventoryItems[_slotNumber].m_equipmentType;
            foreach (InventoryItem _equipmentSlot in m_equipmentItems) {
                if(m_chosenEquipmentType == _equipmentSlot.m_equipmentType)
                {
                    MoveItem(_slotNumber, (int)m_chosenEquipmentType, false, true);
                }
            }
        }
        return m_itemHasBeenRemoved;
    }

    public void Split(int _slotNumber)
    {
		InventoryItem m_chosenItem;
		int m_originalQuantity;

        for(int i = 0; i < m_inventoryItems.Count; i++)
        {
            if (m_inventoryItems[i].m_itemType == InventoryItem.ITEM_TYPE.NONE)
            {
                m_chosenItem = m_inventoryItems[_slotNumber];
                m_originalQuantity = m_chosenItem.m_quantity;
				if(m_originalQuantity > 1)
				{
                	int m_splitQuantity = m_originalQuantity / 2;
	                if (m_originalQuantity % 2 != 0)
	                {
	                    m_inventoryItems[_slotNumber].m_quantity = m_splitQuantity + 1;
						m_inventoryItems[i] = new InventoryItem(m_inventoryItems[_slotNumber]);
						m_inventoryItems[i].m_quantity -= 1;
	                }
	                else
	                {
	                    m_inventoryItems[_slotNumber].m_quantity = m_splitQuantity;
						m_inventoryItems[i] = new InventoryItem(m_inventoryItems[_slotNumber]);
	                }
				}
                //Debug.Log("Created item on slot : " + i);
                return;
            }
        }

		m_chosenItem = m_inventoryItems[_slotNumber];
		m_originalQuantity = m_chosenItem.m_quantity;
		if(m_originalQuantity > 1)
		{
			int m_splitQuantity = m_originalQuantity / 2;
			AddInventoryItem();
			if (m_originalQuantity % 2 != 0)
			{
				m_inventoryItems[_slotNumber].m_quantity = m_splitQuantity + 1;
				m_inventoryItems[m_inventoryItems.Count-1] = new InventoryItem(m_inventoryItems[_slotNumber]);
				m_inventoryItems[m_inventoryItems.Count-1].m_quantity -= 1;
			}
			else
			{
				m_inventoryItems[_slotNumber].m_quantity = m_splitQuantity;
				m_inventoryItems[m_inventoryItems.Count-1] = new InventoryItem(m_inventoryItems[_slotNumber]);
			}
		}


    }

    public bool IsItemUsable(int _slotNumber)
    {
        if(GetQuantity(_slotNumber) > 0 && m_inventoryItems[_slotNumber].m_itemType == InventoryItem.ITEM_TYPE.USEABLE)
        {
            return true;
        }
        return false;
    }

    public bool IsItemCanSplit(int _slotNumber)
    {
        if (GetQuantity(_slotNumber) > 0 
            && (m_inventoryItems[_slotNumber].m_itemType == InventoryItem.ITEM_TYPE.USEABLE
            || m_inventoryItems[_slotNumber].m_itemType == InventoryItem.ITEM_TYPE.RESOURCE))
        {
            return true;
        }
        return false;
    }

    public int GetQuantity(int _slotNumber)
    {
        return m_inventoryItems[_slotNumber].m_quantity;
    }

    public void RemoveItem(int _slotNumber)
    {
        m_inventoryItems.RemoveAt(_slotNumber);
    }

    public bool IsItemEquipment(int _slotNumber)
    {
        return (m_inventoryItems[_slotNumber].m_itemType == InventoryItem.ITEM_TYPE.EQUIPMENT);
    }

    public void RemoveEquipment(int _slotNumber)
    {
        m_equipmentItems[_slotNumber] = new InventoryItem();
    }

    public bool MoveItem(int _slotNumberFrom, int _slotNumberTo, bool _isFromEquipmentSlot, bool _isToEquipmentDrop = false)
    {
     
        InventoryItem m_temp = m_inventoryItems[_slotNumberFrom];
        bool m_isSuccess = false;
        if (!_isToEquipmentDrop)
        {
            if (_isFromEquipmentSlot)
            {
                if (m_inventoryItems[_slotNumberTo].m_itemType == InventoryItem.ITEM_TYPE.EQUIPMENT ||
                   m_inventoryItems[_slotNumberTo].m_name.Length <= 0)
                {
                    m_temp = m_equipmentItems[_slotNumberFrom];
                    m_equipmentItems[_slotNumberFrom] = m_inventoryItems[_slotNumberTo];
                    m_inventoryItems[_slotNumberTo] = m_temp;
                    m_isSuccess = true;
                }
            } else
            {
                if (m_inventoryItems[_slotNumberTo].m_itemType != InventoryItem.ITEM_TYPE.EQUIPMENT &&
                    m_inventoryItems[_slotNumberFrom].m_id == m_inventoryItems[_slotNumberTo].m_id)
                {
                    m_inventoryItems[_slotNumberTo].m_quantity += m_inventoryItems[_slotNumberFrom].m_quantity;
                    m_inventoryItems[_slotNumberFrom] = new InventoryItem();
                }
                else
                {
                    m_temp = m_inventoryItems[_slotNumberFrom];
                    m_inventoryItems[_slotNumberFrom] = m_inventoryItems[_slotNumberTo];
                    m_inventoryItems[_slotNumberTo] = m_temp;
                }
                m_isSuccess = true;
            }
        } else if(m_temp.m_itemType == InventoryItem.ITEM_TYPE.EQUIPMENT && _isToEquipmentDrop)
        {
            m_temp = m_equipmentItems[_slotNumberTo];
            m_equipmentItems[_slotNumberTo] = m_inventoryItems[_slotNumberFrom];
            m_inventoryItems[_slotNumberFrom] = m_temp;
            if(m_inventoryItems[_slotNumberFrom].m_id == 0)
            {
                RemoveItem(_slotNumberFrom);
            }
            m_isSuccess = true;
        }
        
        return m_isSuccess;
    }

    public bool IsCraftable(CraftableItem _item)
    {
        for (int i = 0; i < _item.m_requiredResources.Count; i++)
        {
            bool m_isCraftable = false;
            int m_totalQuantity = 0;

            foreach (InventoryItem _inventoryItem in m_inventoryItems)
            {
                if (_item.m_requiredResources[i].m_id == m_inventoryItems[i].m_id)
                {
                    if (_item.m_requiredResources[i].m_quantity <= m_inventoryItems[i].m_quantity)
                    {
                        m_isCraftable = true;
                    } else
                    {
                        m_totalQuantity += m_inventoryItems[i].m_quantity;
                    }
                }
            }

            if(m_totalQuantity != 0 && m_totalQuantity >= _item.m_requiredResources[i].m_quantity)
            {
                m_isCraftable = true;
            }

            if(m_isCraftable == false)
            {
                return false;
            }
        }
        return true;
    }

    public bool IsItemExistInInventory(int _id)
    {
        foreach(InventoryItem _item in m_inventoryItems)
        {
            if(_item.m_id == _id)
            {
                return true;
            }
        }
        return false;
    }

    public bool CraftItem(CraftableItem _item, bool _craftAsNewItem = false)
    {
        if (!CanCraft(_item.m_requiredResources))
        {
            return false;
        }
        RemoveFromInventory(_item.m_requiredResources);
        if (!_craftAsNewItem)
        {
            AddInventoryItem(_item.m_item);
        } else
        {
            AddNewInventoryItem(_item.m_item);
        }
        return true;
        /*for (int i = 0; i < _item.m_requiredResources.Count; i++)
        {
            int m_quantityNeeded = _item.m_requiredResources[i].m_quantity;
            List<int> m_itemsToRemove = new List<int>();

            for (int j = 0; j < m_inventoryItems.Count; j++)
            {
                if (_item.m_requiredResources[i].m_id == m_inventoryItems[j].m_id)
                {
                    int m_quantityTemp = m_inventoryItems[j].m_quantity - m_quantityNeeded;
                    if(m_quantityTemp > 0)
                    {
                        m_inventoryItems[j].m_quantity -= m_quantityNeeded;
                        if(m_itemsToRemove.Count > 1)
                        {
                            foreach(int _toRemove in m_itemsToRemove)
                            {
                                RemoveItem(_toRemove);
                            }
                        }
                        AddInventoryItem(_item.m_item);
                    } else if(m_quantityTemp == 0)
                    {
                        AddInventoryItem(_item.m_item);
                        if (m_itemsToRemove.Count > 1)
                        {
                            foreach (int _toRemove in m_itemsToRemove)
                            {
                                RemoveItem(_toRemove);
                            }
                        }
                        RemoveItem(j);
                    } else
                    {
                        m_quantityNeeded -= m_inventoryItems[j].m_quantity;
                        m_itemsToRemove.Add(j);
                        RemoveItem(i);
                    }
                        
                }
            }
            
        }*/
    }
    
    public bool CanCraft(List<InventoryItem> _requiredItems)
    {
        for (int i = 0; i < _requiredItems.Count; i++)
        {
            bool m_canCraft = false;
            for (int j = 0; j < m_inventoryItems.Count; j++)
            {
                if(m_inventoryItems[j].m_id == _requiredItems[i].m_id && m_inventoryItems[j].m_quantity >= _requiredItems[i].m_quantity)
                {
                    m_canCraft = true;
                    break;
                }
            }
            if (!m_canCraft)
            {
                return false;
            }
        }
        return true;
    }

    public void RemoveFromInventory(List<InventoryItem> _requiredItems)
    {
        for (int i = 0; i < _requiredItems.Count; i++)
        {
            for (int j = 0; j < m_inventoryItems.Count; j++)
            {
                if (m_inventoryItems[j].m_id == _requiredItems[i].m_id && m_inventoryItems[j].m_quantity >= _requiredItems[i].m_quantity)
                {
                    if (m_inventoryItems[j].m_quantity - _requiredItems[i].m_quantity == 0)
                    {
                        //Debug.Log("Quantity: " + m_inventoryItems[j].m_quantity + " name: " + m_inventoryItems[j].m_name + " req quantity: " + _requiredItems[i].m_quantity);
                        m_inventoryItems.RemoveAt(j);
                    }
                    else
                    {
                        m_inventoryItems[j].m_quantity -= _requiredItems[i].m_quantity;
                    }
                }
            }
        }
    }

    public bool CanCraft(InventoryItem _requiredItem)
    {
            for (int j = 0; j < m_inventoryItems.Count; j++)
            {
                if (m_inventoryItems[j].m_id == _requiredItem.m_id && m_inventoryItems[j].m_quantity >= _requiredItem.m_quantity)
                {
                    if (m_inventoryItems[j].m_quantity - _requiredItem.m_quantity >= 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        return false;
    }

 

    public InventoryItem GetItemFromSlot(int _chosenItemSlotNumber, bool _isEquipment = false)
    {
        if (_isEquipment)
        {
            return m_equipmentItems[_chosenItemSlotNumber];
        }
        return m_inventoryItems[_chosenItemSlotNumber];
    }
}
