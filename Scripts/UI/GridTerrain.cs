﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GridTerrain : MonoBehaviour
{
    [SerializeField]
    public float size = 1f;

    public static GridTerrain Instance;

    public GameObject m_originObject;
    public GameObject m_terrainBlock;

    private Vector3 m_result;
    private int xCount = 0;
    private int yCount = 0;
    private int zCount = 0;
    private Vector3 m_offsetPosition;
    private Vector3 m_rotation;

    private List<GameObject> m_gridTiles;

    public enum DIRECTION
    {
        NORTH,
        SOUTH,
        WEST,
        EAST,
        NONE
    }

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        m_gridTiles = new List<GameObject>();
        DrawGrid();
        ToggleGridView(false);
    }

    public Vector3 GetOffsetDirection(DIRECTION _direction = DIRECTION.NONE)
    {
        if (_direction != DIRECTION.NONE)
        {
            switch (_direction)
            {
                case DIRECTION.NORTH:
                    m_offsetPosition = new Vector3(size / 2, 0, 0);
                    break;
                case DIRECTION.SOUTH:
                    m_offsetPosition = new Vector3(-size / 2, 0, 0);
                    break;
                case DIRECTION.WEST:
                    m_offsetPosition = new Vector3(0, 0, size / 2);
                    break;
                case DIRECTION.EAST:
                    m_offsetPosition = new Vector3(0, 0, -size / 2);
                    break;
            }
        }
        else
        {
            m_offsetPosition = Vector3.zero;
        }
        return m_offsetPosition;
    }

    public Vector3 GetRotationDirection(DIRECTION _direction = DIRECTION.NONE)
    {
        if (_direction != DIRECTION.NONE)
        {
            switch (_direction)
            {
                case DIRECTION.NORTH:
                    m_rotation = new Vector3(0, 90, 0);
                    break;
                case DIRECTION.SOUTH:
                    m_rotation = new Vector3(0, 90, 0);
                    break;
                case DIRECTION.WEST:
                    m_rotation = new Vector3(0, 0, 0);
                    break;
                case DIRECTION.EAST:
                    m_rotation = new Vector3(0, 0, 0);
                    break;
            }
        }
        else
        {
            m_rotation = Vector3.zero;
        }
        return m_rotation;
    }

    public Vector3 GetNearestPointOnGrid(Vector3 position, DIRECTION _direction = DIRECTION.NONE)
    {
        // position -= transform.position;
        //position -= m_originObject.transform.position;
        xCount = Mathf.RoundToInt(position.x / size);
        yCount = Mathf.RoundToInt(position.y / size);
        zCount = Mathf.RoundToInt(position.z / size);

        m_offsetPosition = GetOffsetDirection(_direction);

        m_result = new Vector3(
        (float)xCount * size,
        (float)yCount * size,
        (float)zCount * size) + m_offsetPosition;

        // result += transform.position;
        //position += m_originObject.transform.position;
        //return position;
        return m_result;
    }

    public void ToggleGridView(bool m_enable)
    {
        foreach (GameObject _gridTile in m_gridTiles)
        {
            _gridTile.SetActive(m_enable);
        }
    }

    public void DrawGrid()
    {
        Vector3 point;
        GameObject _block;
        Vector3 m_originSize = m_originObject.GetComponent<Renderer>().bounds.size;
        Vector3 m_offset = new Vector3(m_originSize.x / 2, 0, m_originSize.z / 2);

        m_terrainBlock.transform.localScale = new Vector3(size, 0.5f, size);
        Vector3 m_tileSize = m_terrainBlock.GetComponent<Renderer>().bounds.size;
        Vector3 m_gridTileOffset = new Vector3(m_tileSize.x / 2, 0, m_tileSize.z / 2);

        for (float x = 0; x < m_originSize.x; x += size)
        {
            for (float z = 0; z < m_originSize.z; z += size)
            {
                point = new Vector3(x, 0f, z) - m_offset;
                _block = GameObject.Instantiate(m_terrainBlock);
                _block.transform.localScale = new Vector3(size, 0.5f, size);
                _block.transform.position = point + m_gridTileOffset;
                m_gridTiles.Add(_block);
            }

        }
    }

   /* private void OnDrawGizmos()
    {
        Vector3 point;
        Gizmos.color = Color.yellow;
        Vector3 m_originSize = m_originObject.GetComponent<Renderer>().bounds.size;
        Vector3 m_offset = new Vector3(m_originSize.x / 2, 0, m_originSize.z / 2);
        for (float x = 0; x < m_originSize.x; x += size)
        {
            for (float z = 0; z < m_originSize.z; z += size)
            {
                point = new Vector3(x, 0f, z) - m_offset;
                Gizmos.DrawSphere(point, 0.1f);
            }

        }
    }*/
}