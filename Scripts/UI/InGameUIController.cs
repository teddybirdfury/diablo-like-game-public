﻿using FJL;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameUIController : MonoBehaviour {

    public static InGameUIController Instance;

    private Vector3 m_joystickMaxMovement = new Vector3(40, 40, 0);

    public GameObject m_enemyPanel;
    public GameObject m_deathPanel;
    public GameObject m_dialoguePanel;

    public Text m_dialogueText;

    public Button m_buildButton;
    private bool m_buildMode;

    private PlayerActions m_player;

    public Image m_playerHealthBar;
    public Text m_playerHealth;
    public Text m_playerName;

    public Image m_enemyHealthBar;
    public Text m_enemyHealth;
    public Text m_enemyName;

    public GameObject m_joystick;
    public GameObject m_joystickThumb;

    public Image m_controlArea;
    

    void Awake()
    {
        Instance = this;
    }

    public void Initialize(PlayerActions _playerActions)
    {
        m_player = _playerActions;
        InitializeHandlers();
    }
    private void InitializeHandlers()
    {
        m_player.PlayerHealthHandle += () =>
        {
            UpdatePlayerHealth();
        };
    }

    private void UpdatePlayerHealth()
    {
        float m_currentHealth = Stats.GetStatValueByName(m_player.m_characterStats.m_currentStats, PlayerStats.STATS.HEALTH.ToString());
        float m_maxHealth = Stats.GetStatValueByName(m_player.m_characterStats.m_finalStats, PlayerStats.STATS.HEALTH.ToString());

        float m_healthPercent = m_currentHealth / m_maxHealth;
        m_playerHealth.text = m_currentHealth.ToString();
        m_playerHealthBar.fillAmount = m_healthPercent;
    }

    public void SetEnemyAttributes(string _name)
    {
        m_enemyName.text = _name;
    }

    public void UpdateEnemyHealth(float _maxHealth, float _currentHealth)
    {
        float m_healthPercent = _currentHealth / _maxHealth;
        m_enemyHealth.text = _currentHealth.ToString();
        m_enemyHealthBar.fillAmount = m_healthPercent;
    }

    public void ToggleInventoryMenu(bool _isEnable)
    {
        m_player.ToggleControls(!_isEnable);
        if (_isEnable)
        {
            BaseUIController.Instance.EnableMenu(1);
        } else
        {
            BaseUIController.Instance.DisableMenu(1);
            BaseUIController.Instance.EnableMenu(0);
            //BaseUIController.Instance.DisableCurrentMenu();
        }
    }

    public void ToggleJoystick(bool m_isEnable)
    {
        m_joystick.SetActive(m_isEnable);
    }

    public void JoystickControl(Vector3 _position, bool _isJoystickStatic, bool _isBegan = false)
    {
        if (_isBegan)
        {
            if (!_isJoystickStatic)
            {
                m_joystick.transform.position = _position;
            }
            m_joystickThumb.transform.localPosition = Vector3.zero;
        } else
        {
            ThumbMovement(_position);
        }
    }

    private void ThumbMovement(Vector3 _position)
    {
        m_joystickThumb.transform.position = _position;
        Vector3 m_joystickPosition = m_joystick.transform.position;
        Vector3 m_joystickMaxMovement = m_joystick.transform.position + this.m_joystickMaxMovement;
        Vector3 m_joystickMinMovement = m_joystick.transform.position - this.m_joystickMaxMovement;
     
        if (m_joystickThumb.transform.position.x > m_joystickMaxMovement.x)
        {
            m_joystickThumb.transform.position = new Vector2(m_joystickMaxMovement.x, m_joystickThumb.transform.position.y);
        } else if(m_joystickThumb.transform.position.x < m_joystickMinMovement.x)
        {
            m_joystickThumb.transform.position = new Vector2(m_joystickMinMovement.x, m_joystickThumb.transform.position.y);
        }

        if(m_joystickThumb.transform.position.y > m_joystickMaxMovement.y)
        {
            m_joystickThumb.transform.position = new Vector2(m_joystickThumb.transform.position.x, m_joystickMaxMovement.y);
        } else if (m_joystickThumb.transform.position.y < m_joystickMinMovement.y)
        {
            m_joystickThumb.transform.position = new Vector2(m_joystickThumb.transform.position.x, m_joystickMinMovement.y);
        }
    }

    public void ToggleDeathUI(bool _isEnable)
    {
        m_deathPanel.SetActive(_isEnable);
    }

    public void ToggleDialogueUI(bool _isEnable)
    {
        m_dialoguePanel.SetActive(_isEnable);
    }

    public void SetDialogue(string _dialogueText)
    {
        m_dialogueText.text = _dialogueText;
    }

    public void Respawn()
    {
        SceneManager.LoadScene("main");
    }

    public void Interact()
    {
        m_player.Interact();
    }

    public void Attack()
    {
        m_player.Attack();
    }

    public void ToggleBuild()
    {
        m_buildMode = !m_buildMode;
        if (m_buildMode)
        {
            m_buildButton.GetComponentInChildren<Text>().text = "Close";
            GridCustom.Instance.ToggleGridView(true);
        } else
        {
            m_buildButton.GetComponentInChildren<Text>().text = "Build";
            GridCustom.Instance.ToggleGridView(false);
        }
    }

    public void ToggleEnemyPanel(bool _isEnable)
    {
        m_enemyPanel.SetActive(_isEnable);
    }
}
