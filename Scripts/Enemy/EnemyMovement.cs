﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : CharacterMovement {
    
    public GameObject m_target;

    private bool m_isMoveDelay = false;
    private float m_delayEachMove = 0.5f;

    public override void Start()
    {
        base.m_movementSpeed = 1;
        m_target = GameObject.FindGameObjectWithTag(StringConstants.PLAYER);//Only player for now
    }

    public override void Update()
    {
        if (m_isMove && base.m_enableMovement)
        {
            if (Utility.IsDistanceClose(transform.position, m_target.transform.position, 10))
            {
                TurnTowardsPlayer();
                transform.Translate(Vector3.forward * m_movementSpeed * Time.deltaTime);
            }
        }
        else if (!m_isMoveDelay)
        {
            StartCoroutine(MoveDelay());
        }
    }

    private IEnumerator MoveDelay()
    {
        m_isMoveDelay = true;
        yield return new WaitForSeconds(m_delayEachMove);
        m_isMove = true;
        yield return new WaitForSeconds(m_delayEachMove);
        m_isMove = false;
        m_isMoveDelay = false;
    }

    void TurnTowardsPlayer()
    {
        Vector3 m_lookRotation = m_target.transform.position - transform.position;
        Quaternion m_newRotation = Quaternion.LookRotation(m_lookRotation);
        Vector3 m_angle = m_newRotation.eulerAngles;
        Vector3 m_modifiedRotation = new Vector3(0, m_angle.y, 0);
        transform.localEulerAngles = m_modifiedRotation;
    }
}
