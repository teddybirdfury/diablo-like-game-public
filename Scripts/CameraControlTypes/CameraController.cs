﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public static CameraController Instance;

    private CameraFollow m_cameraFollow;
    private IsometricCameraView m_isoCamera;
    
	void Awake () {
        Instance = this;
	}
	
	void Start () {
        m_cameraFollow = GetComponent<CameraFollow>();
        m_isoCamera = GetComponent<IsometricCameraView>();
    }
}
