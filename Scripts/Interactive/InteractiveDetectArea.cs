﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveDetectArea : BaseInteractiveDetectArea
{
    void Start()
    {
        base.m_interactiveTypeToDetect.Add(InteractiveObject.InteractiveTypes.ITEM);
        base.m_interactiveTypeToDetect.Add(InteractiveObject.InteractiveTypes.LOOT);
        base.m_interactiveTypeToDetect.Add(InteractiveObject.InteractiveTypes.NPC);
        base.m_interactiveTypeToDetect.Add(InteractiveObject.InteractiveTypes.STRUCTURE);
    }
   
}
