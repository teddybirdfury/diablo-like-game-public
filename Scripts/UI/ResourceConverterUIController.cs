﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResourceConverterUIController : BaseDraggableInventoryUIController {

    public static ResourceConverterUIController Instance;

    public GameObject m_resourceDropObject;
    public GameObject m_resourceCovertedObject;
    public Sprite m_defaultEmptySprite;
    public Text m_timer;
    public Button m_convertButton;

    public UpgradeableStructure m_chosenStructure;
    private CraftableItem m_resourceBeingCrafted;
    private int m_chosenResourceInventoryIndex = -1;
    private float m_currentBuildTimer = -1;

    private InventoryItem m_itemUsedForConvert;

    private bool m_canCraft = false;
    private bool m_isStartConvert = false;

    public GameObject m_upgradeButton;
    private bool m_hasCrafted = false;

    private void Awake()
    {
        Instance = this;
    }

    public override void Initialize(PlayerActions _player)
    {
        m_player = _player;
        m_playerInventory = m_player.m_inventory;
        StartCoroutine(InitializeProcess());
        InitializeResourceDropEvent();
        InitializeCraftedObjectEvent();
       // Reinitialize(); //Remove later, only for testing
    }

    public void Reinitialize(UpgradeableStructure _chosenStructure)
    {
        m_hasCrafted = false;
        m_chosenStructure = _chosenStructure;
        RefreshInventory(m_playerItemSlotUI, m_playerInventory, m_playerItemSlotParent, true);
        SetQuantityDisplay(m_playerItemSlotUI, m_playerInventory);
        RemoveDropableUIScript();
        m_isInputsEnabled = true;

        if (m_chosenStructure.GetComponent<InteractiveStructure>().m_hasUpgrade)
        {
            m_upgradeButton.SetActive(true);
        } else {
            m_upgradeButton.SetActive(false);
        }
           
    }

    public void UpgradeScreen()
    {
        BaseUIController.Instance.EnableMenu(7);
        StructureUpgradeUIController.Instance.Reinitialize(m_chosenStructure);
    }

    protected override void Update()
    {
        base.Update();
        if (m_currentBuildTimer > 0 && m_isStartConvert)
        {
            m_currentBuildTimer -= Time.deltaTime;
            m_timer.text = m_currentBuildTimer.ToString("F2");
        } else if(m_currentBuildTimer <= 0 && m_chosenResourceInventoryIndex != -1 && m_isStartConvert)
        {
            m_isStartConvert = false;
            m_convertButton.interactable = false;
            if (m_resourceBeingCrafted != null && m_resourceBeingCrafted.m_id != -1)
            {
                int m_currentInventoryIndex = m_chosenResourceInventoryIndex;
                int m_previousInventoryCount = m_playerInventory.m_inventoryItems.Count;

                m_playerInventory.CraftItem(m_resourceBeingCrafted, true);

                if (!m_playerInventory.IsItemExistInInventory(m_itemUsedForConvert.m_id))
                {
                    GameObject.Destroy(m_playerItemSlotUI[m_currentInventoryIndex]);
                    m_playerItemSlotUI.RemoveAt(m_currentInventoryIndex);
                    ClearUISlot(m_resourceDropObject, m_defaultEmptySprite);
                    ReindexEventArraySlots(m_playerItemSlotUI, true);
                }
              
                m_hasCrafted = true;
                //m_objectBeingDragged.SetActive(true);
                //InitializeItemPositionOnUI(m_objectBeingDragged, m_playerItemSlotParent);

                //RefreshInventory(m_playerItemSlotUI, m_playerInventory, m_playerItemSlotParent, true);
                SetQuantityDisplay(m_playerItemSlotUI, m_playerInventory);
                //OnResourceEndDrag(null);
                m_timer.text = "";
            }
        } else
        {
            m_timer.text = "";
        }
    }

    private void OnResourceDrop()
    {
        if (m_objectBeingDragged == null || !m_canCraft)
        {
            m_convertButton.interactable = false;
            return;
        }
        
        m_resourceDropObject.GetComponentsInChildren<Image>()[1].sprite = m_objectBeingDragged.GetComponentsInChildren<Image>()[1].sprite;
        m_resourceDropObject.GetComponentInChildren<Text>().text = m_resourceBeingCrafted.m_requiredResources[0].m_quantity.ToString();

        m_playerItemSlotUI[m_chosenItemSlotNumber].SetActive(false);
        m_itemUsedForConvert = m_playerInventory.m_inventoryItems[m_chosenItemSlotNumber];
        m_chosenResourceInventoryIndex = m_chosenItemSlotNumber;
        SetSpriteConvertedResource(false);
        m_chosenItemSlotNumber = -1;
        
        m_convertButton.interactable = true;
    }


    /*
       private bool CanCraftItem(int _inventoryItemIndex, int _slotNumber)
    {
        if (m_structureToFinalize.m_requiredResources[_slotNumber].m_id == m_playerInventory.m_inventoryItems[_inventoryItemIndex].m_id
           && m_playerInventory.CanCraft(m_structureToFinalize.m_requiredResources[_slotNumber]))
        {
            return true;
        }
        return false;
    }
    */

   /* protected override void OnDragEnd(PointerEventData data, int _slotNumber)
    {
        base.OnDragEnd(data, _slotNumber);
      
    }*/

    private void SetSpriteConvertedResource(bool m_isSetDefault)
    {
        if (m_isSetDefault)
        {
            m_resourceCovertedObject.GetComponentsInChildren<Image>()[1].sprite = m_defaultEmptySprite;
        }
        else
        {
            InventoryItem m_resource = m_playerInventory.m_inventoryItems[m_chosenResourceInventoryIndex];
            CraftableItem m_craftableItem = CraftableItemsList.Instance.m_list.FindItemWithExactRequirements(m_resource.m_id);

            if (m_craftableItem == null)
            {
                m_playerItemSlotUI[m_chosenResourceInventoryIndex].SetActive(true);
                m_playerItemSlotUI[m_chosenResourceInventoryIndex].transform.SetParent(m_playerItemSlotParent.transform);
                m_resourceDropObject.GetComponentsInChildren<Image>()[1].sprite = m_defaultEmptySprite;

                m_eventSystem.SetSelectedGameObject(null);
            }
            else
            {
                //m_resourceBeingCrafted = m_craftableItem;
                m_currentBuildTimer = m_craftableItem.m_timeToBuild;
                m_timer.text = m_currentBuildTimer.ToString();
                try
                {
                    m_resourceCovertedObject.GetComponentsInChildren<Image>()[1].sprite = m_craftableItem.m_itemSprite;
                } catch (UnityException e)
                {
                    Debug.LogError("Sprite was not set in crafted resource");
                }
            }
        }
    }

    private void RemoveDropableUIScript()
    {
        foreach (GameObject _gameObject in m_playerItemSlotUI)
        {
            Destroy(_gameObject.GetComponent<DropableUI>());
        }
    }

    public void EmptyItemSlot(int _slotNumber)
    {
        m_playerItemSlotUI[_slotNumber].GetComponentsInChildren<Image>()[1].sprite = null;
    }

    protected override void OnDown(PointerEventData data, GameObject _slotObj, int _slotNumber, bool _isPlayerInventory)
    {
        base.OnDown(data, _slotObj, _slotNumber, _isPlayerInventory);
        InventoryItem m_resource = m_playerInventory.m_inventoryItems[_slotNumber];
        CraftableItem m_itemToCraft = CraftableItemsList.Instance.m_list.FindItemWithExactRequirements(m_resource.m_id);
        m_resourceBeingCrafted = m_itemToCraft;
        if (m_itemToCraft != null && CanCraftItem(_slotNumber))
        {
            //m_resourceBeingCrafted = m_itemToCraft;
            m_resourceDropObject.GetComponent<Button>().Select();
            m_canCraft = true;
        } else
        {
            m_canCraft = false;
        }
    }

    private bool CanCraftItem(int _inventoryItemIndex)
    {
        if (m_resourceBeingCrafted.m_requiredResources[0].m_id == m_playerInventory.m_inventoryItems[_inventoryItemIndex].m_id
           && m_playerInventory.CanCraft(m_resourceBeingCrafted.m_requiredResources[0]))
        {
            return true;
        }
        return false;
    }

    private void ImageAlphaAdjustResourceDrop(bool _isMakeHalfTransparent){
        Color m_slotUIColor = m_resourceDropObject.GetComponentsInChildren<Image>()[1].color;
        // m_resourceDropUISlots[m_chosenResourceInventoryIndex].GetComponentsInChildren<Image>()[1].color = 

        if (_isMakeHalfTransparent)
        {
            m_resourceDropObject.GetComponentsInChildren<Image>()[1].color = new Color(m_slotUIColor.r, m_slotUIColor.g, m_slotUIColor.b, .5f);
        }
        else
        {
            m_resourceDropObject.GetComponentsInChildren<Image>()[1].color = new Color(m_slotUIColor.r, m_slotUIColor.g, m_slotUIColor.b, 1);
        }
    }

    private void OnResourceDown(PointerEventData data)
    {
        if (m_chosenResourceInventoryIndex == -1)
        {
            return;
        }
        m_objectBeingDragged = m_playerItemSlotUI[m_chosenResourceInventoryIndex];
        m_popUpFlag = true;
    }

    private void OnResourceBeginDrag(PointerEventData data)
    {
        if(m_chosenResourceInventoryIndex == -1)
        {
            return;
        }
        m_playerItemSlotUI[m_chosenResourceInventoryIndex].SetActive(true);
        m_objectBeingDragged.SetActive(true);
        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 0;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        m_objectBeingDragged = m_playerItemSlotUI[m_chosenResourceInventoryIndex];
    }

    private void OnResourceDrag(PointerEventData data)
    {
        if (m_chosenResourceInventoryIndex == -1)
        {
            return;
        }
        m_objectBeingDragged.transform.position = m_inputPosition;
    }

    private void OnResourceEndDrag(PointerEventData data)
    {
        if (m_chosenResourceInventoryIndex == -1)
        {
            return;
        }

        m_objectBeingDragged.transform.SetParent(m_playerItemSlotParent.transform);
        m_objectBeingDragged.GetComponent<Image>().raycastTarget = true;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 1;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        m_objectBeingDragged.SetActive(true);

        ClearUISlot(m_resourceDropObject, m_defaultEmptySprite);

        if (!m_hasCrafted)
        {
            SetSpriteConvertedResource(true);
        } else
        {
            m_resourceDropObject.GetComponent<EventTrigger>().enabled = false;
        }

        ResetParamsCraftEndDrag();
        m_chosenResourceInventoryIndex = -1;
        m_resourceBeingCrafted = null;
    }
    

   /* protected override IEnumerator ReindexEventArraySlots(List<GameObject> _sourceUI, bool _isPlayerInventory)
    {
        for (int i = 0; i < _sourceUI.Count; i++)
        {
            Destroy(_sourceUI[i].GetComponent<EventTrigger>());
            _sourceUI[i].AddComponent(typeof(EventTrigger));
            yield return new WaitForEndOfFrame();
            InitializeEvents(_sourceUI[i], i, _isPlayerInventory);
        }
    }*/

    public void StartConvert()
    {
        m_isStartConvert = true;
        m_resourceCovertedObject.GetComponentsInChildren<Image>()[1].sprite = m_resourceBeingCrafted.m_itemSprite;
    }

    private void OnCraftedObjectDown()
    {
        if (!m_hasCrafted)
        {
            return;
        }
        // m_chosenItemSlotNumber = m_playerItemSlotUI.Count;
        base.CreateUISlotItem(m_playerItemSlotUI, m_playerItemSlotParent, m_chosenItemSlotNumber);
        m_objectBeingDragged = m_playerItemSlotUI[m_playerItemSlotUI.Count - 1];
        m_objectBeingDragged.GetComponentsInChildren<Image>()[1].sprite = m_resourceCovertedObject.GetComponentsInChildren<Image>()[1].sprite;
        m_objectBeingDragged.SetActive(false);
        m_objectBeingDragged.transform.position = m_inputPosition;
    }

    private void OnCraftedObjectDrag()
    {
        if (!m_hasCrafted && m_objectBeingDragged != null)
        {
            return;
        }
        m_objectBeingDragged.SetActive(true);
        m_objectBeingDragged.transform.position = m_inputPosition;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 0;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
    }

    private void OnCraftedObjectEndDrag()
    {
        if (!m_hasCrafted && m_objectBeingDragged != null)
        {
            return;
        }

        m_objectBeingDragged.transform.SetParent(m_playerItemSlotParent.transform);
        m_objectBeingDragged.GetComponent<Image>().raycastTarget = true;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 1;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        m_objectBeingDragged.SetActive(true);

        ClearUISlot(m_resourceCovertedObject, m_defaultEmptySprite);

        ResetParamsCraftEndDrag();
        m_hasCrafted = false;
        m_convertButton.interactable = true;
        m_resourceDropObject.GetComponent<EventTrigger>().enabled = true;
    }

    private void ResetParamsCraftEndDrag()
    {
        m_eventSystem.SetSelectedGameObject(null);
        m_convertButton.interactable = false;
        m_isStartConvert = false;
        m_canCraft = false;
        m_currentBuildTimer = -1;
        m_timer.text = "";
        m_popUpFlag = false;
    }

    private void InitializeCraftedObjectEvent()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        EventTrigger _trigger = m_resourceCovertedObject.GetComponent<EventTrigger>();

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => {
            OnCraftedObjectDown();
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drag;
        entry.callback.AddListener((data) => {
            OnCraftedObjectDrag();
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.EndDrag;
        entry.callback.AddListener((data) => {
            OnCraftedObjectEndDrag();
        });
        _trigger.triggers.Add(entry);
    }

    private void InitializeResourceDropEvent()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        EventTrigger _trigger = m_resourceDropObject.GetComponent<EventTrigger>();

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drop;
        entry.callback.AddListener((data) =>
        {
            OnResourceDrop();
        });

        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => {
            OnResourceDown((PointerEventData)data);
        });
        _trigger.triggers.Add(entry);


        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.BeginDrag;
        entry.callback.AddListener((data) => {
            OnResourceBeginDrag((PointerEventData)data);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drag;
        entry.callback.AddListener((data) => {
            OnResourceDrag((PointerEventData)data);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.EndDrag;
        entry.callback.AddListener((data) => {
            OnResourceEndDrag((PointerEventData)data);
        });
        _trigger.triggers.Add(entry);
    }
}
