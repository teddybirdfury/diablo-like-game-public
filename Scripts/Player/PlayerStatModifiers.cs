﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FJL
{
    public class PlayerStatModifiers {
        
        public static float GetModifier(CharacterStats.STATS _statType, float _value)
        {
            switch (_statType)
            {
                case CharacterStats.STATS.ATTACK:
                return AttackModifier(_value);
                    
                case CharacterStats.STATS.DEFENSE:
                return AttackModifier(_value);
            }
            return 1;
        }

        public static float GetModifier(string _statType, float _value)
        {
            return GetModifier((CharacterStats.STATS)Enum.Parse(typeof(CharacterStats.STATS), _statType), _value);
        }

        private static float AttackModifier(float _value) {
          //  Debug.Log("Passed Value: " + _value + " Modified: " + (_value / 20));
            return _value / 20;
        }

    }
}
