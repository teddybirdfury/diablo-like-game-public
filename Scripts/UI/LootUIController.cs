﻿using FJL;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LootUIController : BaseDraggableInventoryUIController
{
    public static LootUIController Instance;
    
    public List<GameObject> m_lootItemSlotUI;
    public GameObject m_lootItemSlotParent;
    public Button m_takeAllFromLoot;
    public Button m_takeAllFromPlayer;

    Inventory m_lootInventory;

    private void Awake()
    {
        Instance = this;
    }

    private void SaveItemPositions()
    {
        Vector2[] m_positions = new Vector2[m_playerItemSlotUI.Count];

        for (int i = 0; i < m_playerItemSlotUI.Count; i++)
        {
            m_positions[i] = m_playerItemSlotUI[i].transform.localPosition;

           // Debug.Log("Save item positions : " + m_positions[i] + " name: " + m_playerItemSlotUI[i].transform.name);

        }

        PlayerPrefsX.SetVector2Array(StringConstants.ITEM_POSITIONS, m_positions);
    }
    

    protected override void InitializeContainerEvents()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drop;
        entry.callback.AddListener((data) => {
            ContainerOnDrop(true);
        });

        m_playerItemSlotParent.GetComponent<EventTrigger>().triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drop;
        entry.callback.AddListener((data) => {
            ContainerOnDrop(false);
        });
        m_lootItemSlotParent.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    protected override void ContainerOnDrop(bool _isDropToPlayerContainer)
    {
        if(m_objectBeingDragged == null)
        {
            return;
        }
      
        m_objectBeingDragged.GetComponent<Image>().raycastTarget = true;

        //Debug.Log("_isDropToPlayerContainer: " + _isDropToPlayerContainer + " m_isChosenItemFromPlayerInventory: " + !m_isChosenItemFromPlayerInventory);
        if (_isDropToPlayerContainer && !m_isChosenItemFromPlayerInventory)
        {
            TransferParentUI(m_lootItemSlotUI[m_chosenItemSlotNumber], m_playerItemSlotParent);
            TransferItem(m_lootInventory, m_playerInventory);
            TransferUISlot(m_lootItemSlotUI, m_playerItemSlotUI);
            m_chosenItemSlotNumber = m_playerItemSlotUI.Count - 1;
            m_isChosenItemFromPlayerInventory = true;
            RefreshInventory();
            StartCoroutine(ReindexEventArraySlots());
        } else if (!_isDropToPlayerContainer && m_isChosenItemFromPlayerInventory)
        {
            TransferParentUI(m_playerItemSlotUI[m_chosenItemSlotNumber], m_lootItemSlotParent);
            TransferItem(m_playerInventory, m_lootInventory);
            TransferUISlot(m_playerItemSlotUI, m_lootItemSlotUI);
            m_chosenItemSlotNumber = m_lootItemSlotUI.Count - 1;
            m_isChosenItemFromPlayerInventory = false;
            RefreshInventory();
            StartCoroutine(ReindexEventArraySlots());
        }
      
    }

    private void TransferItem(Inventory _source, Inventory _destination)
    {
        InventoryItem m_item = _source.GetItemFromSlot(m_chosenItemSlotNumber);
        _destination.AddInventoryItem(m_item);
        _source.RemoveItem(m_chosenItemSlotNumber);
    }

    private IEnumerator ReindexEventArraySlots()
    {
        StartCoroutine(base.ReindexEventArraySlots(m_playerItemSlotUI, true));
        StartCoroutine(base.ReindexEventArraySlots(m_lootItemSlotUI, false));
        yield return null;
    }

    public override void DisableUI()
    {
        m_isInputsEnabled = false;
        DestroyLootUI();
    }

    private void DestroyLootUI()
    {
        foreach(GameObject _lootUI in m_lootItemSlotUI)
        {
            GameObject.Destroy(_lootUI);
        }
        m_lootItemSlotUI.Clear();
    }

    public override void Reinitialize()
    {

    }

    public void Reinitialize(Inventory _loot)
    {
        ResetUI(m_playerItemSlotUI);
        ResetUI(m_lootItemSlotUI);
        m_lootInventory = _loot;
        RefreshInventory();
        SetQuantityDisplay();
        InitializeItemPositionsOnUI(m_playerItemSlotUI, m_playerItemSlotParent);
        InitializeItemPositionsOnUI(m_lootItemSlotUI, m_lootItemSlotParent);
        GetInventoryItemPositions(m_playerItemSlotUI);
        m_isInputsEnabled = true; 
    }

    private void GetInventoryItemPositions(List<GameObject> _sourceUI) {
        List<GameObject> m_inventoryUI = InventoryUIController.Instance.m_playerItemSlotUI;
        for (int i = 0; i < _sourceUI.Count; i++) {
            if (m_inventoryUI.Count > i)
            {
                _sourceUI[i].transform.localPosition = m_inventoryUI[i].transform.localPosition;
            }
        }
    }

    private void ResetUI(List<GameObject> _sourceUI)
    {
        foreach (GameObject _obj in _sourceUI)
        {
            GameObject.Destroy(_obj);
        }
        _sourceUI.Clear();
    }

    public void RefreshInventory()
    {
        RefreshInventory(m_playerItemSlotUI, m_playerInventory, m_playerItemSlotParent, true);
        RefreshInventory(m_lootItemSlotUI, m_lootInventory, m_lootItemSlotParent, false);
        RemoveDropableUIScript();
        m_takeAllFromLoot.interactable = (m_lootItemSlotUI.Count != 0);
        m_takeAllFromPlayer.interactable = (m_playerItemSlotUI.Count != 0);
    }

    private void RemoveDropableUIScript()
    {
        foreach(GameObject _gameObject in m_playerItemSlotUI)
        {
            Destroy(_gameObject.GetComponent<DropableUI>());
        }
        foreach (GameObject _gameObject in m_lootItemSlotUI)
        {
            Destroy(_gameObject.GetComponent<DropableUI>());
        }
    }

    private Vector2 RandomizePosition(GameObject _parentContainer)
    {
        float m_maxX = (_parentContainer.GetComponent<RectTransform>().sizeDelta.magnitude * 2) - m_randomizePositionXOffset;
        float m_minX = -(_parentContainer.GetComponent<RectTransform>().sizeDelta.magnitude * 2) + m_randomizePositionXOffset;
        float m_value = UnityEngine.Random.Range(m_minX, m_maxX);
        return new Vector2(m_value, m_itemSlotPrefab.transform.position.y);
    }

    private void InitializeItemsOnUI()
    {
        foreach (GameObject _gameObject in m_playerItemSlotUI)
        {
            _gameObject.transform.localPosition = RandomizePosition(m_playerItemSlotParent);
        }

        foreach (GameObject _gameObject in m_lootItemSlotUI)
        {
            _gameObject.transform.localPosition = RandomizePosition(m_lootItemSlotParent);
        }
    }

    private void TogglePopUp(bool _isEnable)
    {
        if (!_isEnable)
        {
            m_popUp.SetActive(false);
            return;
        }

        InventoryItem m_item = null;

        if (m_isChosenItemFromPlayerInventory)
        {
            m_item = m_playerInventory.GetItemFromSlot(m_chosenItemSlotNumber);
            m_popUp.transform.position = m_playerItemSlotUI[m_chosenItemSlotNumber].transform.position;
        } else
        {
            m_item = m_lootInventory.GetItemFromSlot(m_chosenItemSlotNumber);
            m_popUp.transform.position = m_lootItemSlotUI[m_chosenItemSlotNumber].transform.position;
        }

        if (m_item.m_id == 0)
        {
            return;
        }
        
        m_popUp.GetComponentInChildren<Text>().text = GenerateText(m_item);
        m_popUp.SetActive(true);
        
    }

    protected override void InitializeEvents(GameObject _slotObj, int _slotNumber, bool _isPlayerInventory)
    {
        EventTrigger _trigger = _slotObj.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => {
            OnDown((PointerEventData)data, _slotNumber, _isPlayerInventory);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.BeginDrag;
        entry.callback.AddListener((data) => {
            OnBeginDrag((PointerEventData)data, _slotNumber);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drag;
        entry.callback.AddListener((data) => {
            OnDrag((PointerEventData)data, _slotNumber);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drop;
        entry.callback.AddListener((data) => {
            OnDrop((PointerEventData)data, _slotNumber);
        });
        _trigger.triggers.Add(entry);
        
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.EndDrag;
        entry.callback.AddListener((data) => {
            OnDragEnd((PointerEventData)data, _slotNumber);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((data) => {
            OnPointerUp();
        });
        _trigger.triggers.Add(entry);
    }

    public void OnDown(PointerEventData data, int _slotNumber, bool _isPlayerInventory)
    {
        m_tapTime = 0;
        m_chosenItemSlotNumber = _slotNumber;
        m_isChosenItemFromPlayerInventory = _isPlayerInventory;
        Inventory m_inventory;

        if (m_isChosenItemFromPlayerInventory)
        {
            m_inventory = m_playerInventory;
        } else
        {
            m_inventory = m_lootInventory;
        }

        
        m_useButton.interactable = false;
        m_splitButton.interactable = false;
        m_deleteButton.interactable = true;

        if (m_isChosenItemFromPlayerInventory)
        {
            m_objectBeingDragged = m_playerItemSlotUI[_slotNumber];
        } else
        {
            m_objectBeingDragged = m_lootItemSlotUI[_slotNumber];
        }
       
        m_oldParent = m_objectBeingDragged.transform.parent;

        if (m_inventory.IsItemUsable(_slotNumber))
        {
            m_useButton.interactable = true;
        }
        if (m_inventory.IsItemCanSplit(_slotNumber) && m_inventory.GetQuantity(_slotNumber) > 1)
        {
            m_splitButton.interactable = true;
        }

        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 0;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
    }

   /* private void CreateInventoryUIItem(Transform _parent, InventoryItem _item = null)
    {
        GameObject temp = GameObject.Instantiate(m_itemSlotPrefab);
        temp.transform.SetParent(_parent, false);
        m_playerItemSlotUI.Add(temp);
        if (_item != null)
        {
            temp.GetComponentsInChildren<Image>()[1].sprite = _item.m_itemSprite;
        }
        InitializeEvents(temp.GetComponent<EventTrigger>().gameObject, m_playerItemSlotUI.Count - 1, m_isChosenItemFromPlayerInventory);
    }*/

   /* public void OnDrop(PointerEventData data, int _slotNumber)
    {
        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 0;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        m_objectBeingDragged.GetComponent<Image>().raycastTarget = true;
        m_eventSystem.SetSelectedGameObject(null);
    }*/

    private void UseItemProcess(List<GameObject> _slotUI, Inventory _inventory)
    {
        if (_inventory.UseOrEquip(m_chosenItemSlotNumber))
        {
            RemoveUISlot(_slotUI);
            m_useButton.interactable = false;
            m_splitButton.interactable = false;
        }
    }

    public void UseItem()
    {
        if (m_isChosenItemFromPlayerInventory)
        {
            UseItemProcess(m_playerItemSlotUI, m_playerInventory);
        } else
        {
            UseItemProcess(m_lootItemSlotUI, m_lootInventory);
        }

        SetQuantityDisplay();
        DisableAllButtons();
        StartCoroutine(ReindexEventArraySlots());
    }

    private void SplitProcess(Inventory _inventory)
    {
        _inventory.Split(m_chosenItemSlotNumber);
        if (_inventory.GetQuantity(m_chosenItemSlotNumber) <= 1)
        {
            m_splitButton.interactable = false;
        }
    }

    public void Split()
    {
        if (m_isChosenItemFromPlayerInventory)
        {
            SplitProcess(m_playerInventory);
        } else
        {
            SplitProcess(m_lootInventory);
        }

        RefreshInventory();
        SetQuantityDisplay();
        StartCoroutine(ReindexEventArraySlots());
    }

    public void Delete()
    {
        if (m_isChosenItemFromPlayerInventory)
        {
            RemoveItemAndUISlot(m_playerItemSlotUI, m_playerInventory);
        } else
        {
            RemoveItemAndUISlot(m_lootItemSlotUI, m_lootInventory);
        }
        StartCoroutine(ReindexEventArraySlots());
    }

    private void TakeAllItemsProcess(Inventory _source, Inventory _destination, List<GameObject> _sourceUI)
    {
        List<InventoryItem> m_items = _source.m_inventoryItems;
        int m_count = _source.m_inventoryItems.Count;
        GameObject _temp;
        if(m_count == 0)
        {
            return;
        }
        for (int i = m_count - 1; i >= 0; i--)
        {
            _temp = _sourceUI[i];
            _destination.AddInventoryItem(_source.m_inventoryItems[i]);
            GameObject.DestroyImmediate(_temp);
        }
        _source.m_inventoryItems.Clear();
        _sourceUI.Clear();
    }

    public void TakeAllItems(bool _isTakeAllFromLoot)
    {
        
        if (_isTakeAllFromLoot)
        {
            TakeAllItemsProcess(m_lootInventory, m_playerInventory, m_lootItemSlotUI);
        } else
        {
            TakeAllItemsProcess(m_playerInventory, m_lootInventory, m_playerItemSlotUI);
        }
        RefreshInventory();
        StartCoroutine(ReindexEventArraySlots());
    }

    private void RemoveItemAndUISlot(List<GameObject> _sourceUI, Inventory _sourceInventory, bool _destroySlotUI = false)
    {
        _sourceInventory.RemoveItem(m_chosenItemSlotNumber);
        RemoveUISlot(_sourceUI);
    }

    private void RemoveUISlot(List<GameObject> _sourceUI)
    {
        GameObject m_temp = _sourceUI[m_chosenItemSlotNumber];
        _sourceUI.Remove(m_temp);
        GameObject.Destroy(m_temp);
    }

    private void SetQuantityDisplayProcess(List<GameObject> _sourceUI, Inventory _sourceInventory)
    {
        List<InventoryItem> m_items = _sourceInventory.m_inventoryItems;
        for (int i = 0; i < _sourceUI.Count; i++)
        {
            if (m_items.Count > i && m_items[i].m_quantity > 0)
            {
                _sourceUI[i].GetComponentInChildren<Text>().text = m_items[i].m_quantity.ToString();
            }
            else
            {
                _sourceUI[i].GetComponentInChildren<Text>().text = "";
            }
        }
    }

    public override void SetQuantityDisplay()
    {
        SetQuantityDisplayProcess(m_playerItemSlotUI, m_playerInventory);
        SetQuantityDisplayProcess(m_lootItemSlotUI, m_lootInventory);
    }

    public void DisableAllButtons()
    {
        m_useButton.interactable = false;
        m_splitButton.interactable = false;
        m_deleteButton.interactable = false;
    }
}
