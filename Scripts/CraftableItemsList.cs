﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public class CraftableItemsList : MonoBehaviour {

    public static CraftableItemsList Instance;

    public TextAsset m_asset;

    [Serializable]
    public class Craftable
    {
        public List<CraftableItem> m_items = new List<CraftableItem>();

        public CraftableItem FindItemWithExactRequirements(int m_id)
        {
            foreach(CraftableItem _item in m_items)
            {
                if (_item.m_requiredResources.Count == 1 
                    && _item.m_requiredResources[0].m_id == m_id)
                {
                    return _item;
                }
            }
            return null;
        }

        public CraftableItem FindItemWithRequirements(int m_id)
        {
            foreach (CraftableItem _item in m_items)
            {
                if (_item.m_requiredResources[0].m_id == m_id)
                {
                    return _item;
                }
            }
            return null;
        }

        public CraftableItem CanCraftItem(InventoryItem m_item)
        {
            foreach (CraftableItem _item in m_items)
            {
                if (_item.m_requiredResources[0].m_id == m_item.m_id && _item.m_requiredResources[0].m_quantity == m_item.m_id)
                {
                    return _item;
                }
            }
            return null;
        }
    }

    public Craftable m_list;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        //LoadItems();
    }

    private void OnDestroy()
    {
       SaveItems();
    }

   /* private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            SaveItems();
        }
    }*/

    private void SaveItems()
    {
        string m_saved = JsonUtility.ToJson(m_list);
        PlayerPrefs.SetString(StringConstants.CRAFTABLE_ITEM, m_saved);
        // PlayerPrefsX.SetStringArray(StringConstants.CRAFTABLE_ITEM, m_jsonData.ToArray());
    }

    private void LoadItems()
    {
        string m_savedJson = m_asset.text;
       // string m_savedJson = PlayerPrefs.GetString(StringConstants.CRAFTABLE_ITEM);
        Craftable m_jsonData = JsonUtility.FromJson<Craftable>(m_savedJson);
        m_list = m_jsonData;
    }
}
