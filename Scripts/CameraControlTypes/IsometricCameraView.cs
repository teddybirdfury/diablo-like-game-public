﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsometricCameraView : MonoBehaviour {

    public GameObject m_player;
    public float dragSpeed = 6f;

    float horizontalMovement = 0;
    float verticalMovement = 0;

    float offsetXMovement = 0;
    float offsetYMovement = 0;

    public Vector3 m_inGameOffsetPosition;

    public bool m_isGlobalMapView = true;
    
    void OnEnable () {
        if (m_isGlobalMapView)
        {
            GlobalMapView();
        } else
        {
            InGameView();
        }
	}

    void GlobalMapView()
    {
        transform.localPosition = new Vector3(25, 24, 6);
        transform.eulerAngles = new Vector3(60, -45, 0);
    }

    void InGameView()
    {
       // Debug.Log("Set view");
       // transform.localPosition = new Vector3(m_startingX, 10, m_startingZ);
        transform.position = m_inGameOffsetPosition + m_player.transform.position;
        transform.eulerAngles = new Vector3(60, -45, 0);
    }

    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            horizontalMovement = -Input.GetAxis("Mouse X") * Time.deltaTime * dragSpeed;
            verticalMovement = -Input.GetAxis("Mouse Y") * Time.deltaTime * dragSpeed;
            offsetYMovement = horizontalMovement;
            offsetXMovement = verticalMovement;

            verticalMovement += offsetYMovement;
            horizontalMovement -= offsetXMovement;
            transform.Translate(horizontalMovement, 0, verticalMovement, Space.World);
        }
    }

}
