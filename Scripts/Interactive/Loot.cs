﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loot : MonoBehaviour {

    public Inventory m_inventory;

    public Mesh m_mesh; //Usually a corpse or chest

    private void Start()
    {
        GetComponent<MeshFilter>().mesh = m_mesh;
    }

    public void AddToInventory(GameObject _target)
    {
        Inventory m_targetInventory = _target.GetComponent<Inventory>();
    }
}
