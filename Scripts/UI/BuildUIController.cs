﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildUIController : DynamicUI {
    
    public static BuildUIController Instance;

    public List<GameObject> m_buildRequirementsUI = new List<GameObject>();
    public List<Structure> m_craftableStructures = new List<Structure>();

    public RectTransform m_contentContainer; //Should scale according to number of elements
    public RectTransform m_contentItemPrefab;

    public RectTransform m_upgradeUI;

    public Structure m_selectedStructure;
    public GameObject m_selectedStructurePrefab;

    PlayerActions m_player;
    Inventory m_inventory;
    GridInteraction m_gridInteraction;
    GridTerrain m_gridTerrain;

    private bool m_isUpgradeMode = false;

    public GameObject m_cameraObject;
    public GameObject m_exitBuildView;

    private Color m_lastObjectColor;
    private GameObject m_lastObjectSelected;
    private Vector2 m_screenPoint = Vector3.zero;
    private RectTransform m_canvasRect;

    public Vector2 m_upgradesOffset = new Vector3(0, 0, 0);

    private int m_offset = 25;

    void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_canvasRect = GetComponent<RectTransform>();
    }

    public void Initialize (PlayerActions _player) {
        m_player = _player;
        m_gridInteraction = GetComponent<GridInteraction>();
        m_gridTerrain = GetComponent<GridTerrain>();
        m_inventory = m_player.m_inventory;
        m_selectedStructure = m_craftableStructures[0];
    }

    public void Reinitialize()
    {
        ToggleUpgradeMode(true);
        StartCoroutine(InitializeStructures());
    }

    private void Update()
    {
        //Disable for now
        /*if (m_isUpgradeMode)
        {
            Vector3 m_position = Vector3.zero;

            if (Input.touchCount > 0)
            {
                Touch m_tounchInput = Input.touches[0];
                if (m_tounchInput.phase == TouchPhase.Began)
                {
                    m_position = m_tounchInput.position;
                }
            } else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    m_position = Input.mousePosition;
                }
               
            }

            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(m_position);
            int layer_mask = LayerMask.GetMask(StringConstants.SPAWNABLE);

            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (Physics.Raycast(ray, out hitInfo, 100, layer_mask))
                {
                    if (m_lastObjectSelected != null)
                    {
                        m_lastObjectSelected.transform.GetComponent<Renderer>().material.SetColor("_Color", m_lastObjectColor);
                    }

                    m_lastObjectSelected = hitInfo.transform.gameObject;
                    m_lastObjectColor = hitInfo.transform.GetComponent<Renderer>().material.GetColor("_Color");
                    m_screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, hitInfo.transform.position);

                    m_upgradeUI.gameObject.SetActive(true);
                    m_upgradeUI.anchoredPosition = m_screenPoint - (m_canvasRect.sizeDelta / 2) + m_upgradesOffset;
                    hitInfo.transform.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
                }
            }
        }*/
    }

    public void Upgrade()
    {
        UpgradeableStructure m_structure = m_lastObjectSelected.GetComponent<UpgradeableStructure>();
        if(m_inventory.CanCraft(m_structure.m_requiredResources))
        {
            m_inventory.RemoveFromInventory(m_structure.m_requiredResources);
            m_lastObjectSelected.GetComponent<UpgradeableStructure>().Upgrade();
            m_lastObjectColor = m_lastObjectSelected.GetComponent<Renderer>().material.GetColor("_Color");
        }
    }

    private IEnumerator InitializeStructures()
    {
        RectTransform m_createdPrefab;

        Image[] m_images = m_contentContainer.GetComponentsInChildren<Image>();
        EventTrigger[] m_triggers = m_contentContainer.GetComponentsInChildren<EventTrigger>();
        base.AdjustContentPanelSize(m_contentContainer, m_contentItemPrefab, m_craftableStructures.Count, m_offset);

        yield return new WaitForEndOfFrame();

        float m_totalDistance = ((m_contentItemPrefab.sizeDelta.x + m_offset) * m_craftableStructures.Count);
        for (int i = 0; i < m_craftableStructures.Count; i++)
        {
            m_createdPrefab = GameObject.Instantiate(m_contentItemPrefab, m_contentContainer.transform);
            m_createdPrefab.anchoredPosition = new Vector2(((-m_contentContainer.sizeDelta.x / 2) + (m_createdPrefab.sizeDelta.x / 2) + m_offset)
               + ((m_createdPrefab.sizeDelta.x + m_offset) * i),
                m_createdPrefab.anchoredPosition.y);
           
            SetPrefabImage(m_createdPrefab.gameObject, m_craftableStructures[i]);
            SetPrefabButtonEvent(m_createdPrefab.gameObject, m_craftableStructures[i]);
            SetRequirements(m_createdPrefab.gameObject, m_craftableStructures[i]);
        }
        yield return new WaitForEndOfFrame();
    }

    private void SetRequirements(GameObject _gameObject, Structure _structure)
    {
        _gameObject.GetComponentInChildren<Text>().text = _structure.m_requiredResources[0].m_quantity.ToString();
        _gameObject.transform.Find("Requirements Panel/Icon").GetComponent<Image>().sprite = _structure.m_requiredResources[0].m_itemSprite;
    }

    private void SetPrefabButtonEvent(GameObject _prefab, Structure _item)
    {
        Button m_button = _prefab.GetComponent<Button>();
        m_button.onClick.AddListener(delegate { OnSelectedItem(_item); });
    }

    private void SetPrefabImage(GameObject _prefab, Structure _item)
    {
        Image[] m_images = _prefab.GetComponentsInChildren<Image>();
        for (int i = 0; i < m_images.Length; i++)
        {
            if (m_images[i].transform != _prefab.transform)
            {
                m_images[i].sprite = _item.m_itemSprite;
            }
        }
    }

    public void ExitBuildUI()
    {
        ToggleBuildView(false);
        ToggleUpgradeMode(false);
    }

    public void ToggleBuildView(bool _isEnable)
    {
        m_exitBuildView.SetActive(_isEnable);
        m_gridTerrain.ToggleGridView(false);
        m_gridInteraction.DisableUI();
        ToggleCameraViews(true);
    }

    private void ToggleUpgradeMode(bool _isEnable)
    {
        m_isUpgradeMode = _isEnable;
        m_upgradeUI.gameObject.SetActive(false);
        if (m_lastObjectSelected != null)
        {
            m_lastObjectSelected.transform.GetComponent<Renderer>().material.SetColor("_Color", m_lastObjectColor);
        }
    }

    private void OnSelectedItem(Structure _item)
    {
        ToggleBuildView(true);
        m_gridTerrain.ToggleGridView(true);
        m_isUpgradeMode = false;
        m_selectedStructure = _item;
        m_selectedStructurePrefab = _item.m_prefab;
        m_gridInteraction.m_objectToCreate = m_selectedStructurePrefab;
        m_gridInteraction.BuildPreviewInitialize(true, m_selectedStructurePrefab);
        m_gridInteraction.m_originalScale = m_selectedStructurePrefab.transform.localScale;
        m_gridInteraction.m_isBuildMode = true;
        ToggleCameraViews(false);

        m_upgradeUI.gameObject.SetActive(false);
        if (m_lastObjectSelected != null)
        {
            m_lastObjectSelected.transform.GetComponent<Renderer>().material.SetColor("_Color", m_lastObjectColor);
        }
    }

    public bool CreateStructure()
    {
        Structure m_structure = m_selectedStructure;
        if (m_inventory.CanCraft(m_structure.m_requiredResources))
        {
            m_inventory.RemoveFromInventory(m_structure.m_requiredResources);
            return true;
        }
        return false;
    }

    private void ToggleCameraViews(bool _isFollow)
    {
        m_cameraObject.GetComponent<IsometricCameraView>().enabled = !_isFollow;
        m_cameraObject.GetComponent<CameraFollow>().enabled = _isFollow;
    }
}
