﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    public float m_movementSpeed = 4;
    public bool m_enableMovement = true;
    public bool m_isMove = false;
    
    public virtual void Start() { }

    public virtual void Update()
    {
        if (m_enableMovement)
        {
            if (m_isMove)
            {
                transform.Translate(Vector3.forward * m_movementSpeed * Time.deltaTime);
            }
        }
    }

    public void LookTowards(Vector3 _position)
    {
        Vector3 m_lookRotation = _position - transform.position;
        Quaternion m_newRotation = Quaternion.LookRotation(m_lookRotation);
        SetRotation(m_newRotation);
    }

    public void SetRotation(Quaternion _rotation)
    {
        Vector3 m_angle = _rotation.eulerAngles;
        Vector3 m_modifiedRotation = new Vector3(0, m_angle.y, 0);
        transform.localEulerAngles = m_modifiedRotation;
    }

    public void SetRotation(Vector3 _euler)
    {
        Vector3 m_angle = _euler;
        Vector3 m_modifiedRotation = new Vector3(0, m_angle.y, 0);
        transform.localEulerAngles = m_modifiedRotation;
    }

    public void SetRotation(float _angle)
    {
        Vector3 m_euler = new Vector3(0, _angle, 0);
        transform.localEulerAngles = m_euler;
    }
}
