﻿using FJL;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BaseDraggableInventoryUIController : MonoBehaviour {
    
    public List<GameObject> m_playerItemSlotUI = new List<GameObject>();

    public GameObject m_popUp;
    public GameObject m_itemSlotPrefab;
    public GameObject m_playerItemSlotParent;

    public EventSystem m_eventSystem;
    public Button m_useButton;
    public Button m_splitButton;
    public Button m_deleteButton;

    protected int m_chosenItemSlotNumber = 0;
    protected int m_randomizePositionXOffset = 100;
    protected Vector3 m_inputPosition;
    protected Transform m_oldParent;
    protected GameObject m_objectBeingDragged;

    protected bool m_isChosenItemFromPlayerInventory = true;
    protected float m_tapTime = 0;
    protected bool m_popUpFlag = false;
    [HideInInspector]
    public bool m_isInputsEnabled = false;

    protected Inventory m_playerInventory;
    protected PlayerActions m_player;

    public virtual void Initialize(PlayerActions _player)
    {
        m_player = _player;
        m_playerInventory = m_player.m_inventory;
        StartCoroutine(InitializeProcess());
    }

    public void SaveItemPositions()
    {
        Vector2[] m_positions = new Vector2[m_playerItemSlotUI.Count];

        for (int i = 0; i < m_playerItemSlotUI.Count; i++)
        {
            m_positions[i] = m_playerItemSlotUI[i].transform.localPosition;
        }

        PlayerPrefsX.SetVector2Array(StringConstants.ITEM_POSITIONS, m_positions);
    }

    protected void ClearUISlot(GameObject _uiSlot, Sprite _defaultEmptySprite)
    {
        _uiSlot.GetComponentsInChildren<Image>()[1].sprite = _defaultEmptySprite;
        _uiSlot.GetComponentInChildren<Text>().text = "";
    }

    public virtual void Reinitialize()
    {
        RefreshInventory(m_playerItemSlotUI, m_playerInventory, m_playerItemSlotParent, true);
        SetQuantityDisplay(m_playerItemSlotUI, m_playerInventory);
        m_isInputsEnabled = true;
    }

    protected virtual IEnumerator InitializeProcess()
    {
        InitializeContainerEvents();
        RefreshInventory(m_playerItemSlotUI, m_playerInventory, m_playerItemSlotParent, true);
        yield return new WaitForEndOfFrame();
        InitializeItemPositionsOnUI(m_playerItemSlotUI, m_playerItemSlotParent);
        LoadItemPositions(m_playerItemSlotUI);
    }

    public virtual void DisableUI()
    {
        m_isInputsEnabled = false;
    }

    public virtual void SetQuantityDisplay()
    {
        SetQuantityDisplay(m_playerItemSlotUI, m_playerInventory);
    }

    public virtual void SetQuantityDisplay(List<GameObject> _sourceUI, Inventory _sourceInventory)
    {
        List<InventoryItem> m_items = _sourceInventory.m_inventoryItems;
        for (int i = 0; i < _sourceUI.Count; i++)
        {
            if (m_items.Count > i && m_items[i].m_quantity > 0)
            {
                _sourceUI[i].GetComponentInChildren<Text>().text = m_items[i].m_quantity.ToString();
            }
            else
            {
                _sourceUI[i].GetComponentInChildren<Text>().text = "";
            }
        }
    }

    protected virtual void LoadItemPositions(List<GameObject> _sourceUI)
    {
        Vector2[] m_itemPositions = PlayerPrefsX.GetVector2Array(StringConstants.ITEM_POSITIONS);
        if (m_itemPositions.Length == 0)
        {
            return;
        }

        for (int i = 0; i < m_itemPositions.Length; i++)
        {
            if (i < _sourceUI.Count)
            {
                _sourceUI[i].transform.localPosition = m_itemPositions[i];
            }
        }
    }

    protected virtual void InitializeItemPositionOnUI(GameObject _sourceUI, GameObject _sourceParent)
    {
        _sourceUI.transform.localPosition = RandomizePosition(_sourceParent);
    }

    protected virtual void InitializeItemPositionsOnUI(List<GameObject> _sourceUI, GameObject _sourceParent)
    {
        foreach (GameObject _gameObject in _sourceUI)
        {
            InitializeItemPositionOnUI(_gameObject, _sourceParent);
        }
    }

    private Vector2 RandomizePosition(GameObject _slotParent)
    {
        float m_maxX = (_slotParent.transform.GetComponent<RectTransform>().sizeDelta.magnitude * 2) - m_randomizePositionXOffset;
        float m_minX = -(_slotParent.transform.GetComponent<RectTransform>().sizeDelta.magnitude * 2) + m_randomizePositionXOffset;
        float m_value = UnityEngine.Random.Range(m_minX, m_maxX);
        return new Vector2(m_value, m_itemSlotPrefab.transform.position.y);
    }

    private void RemoveUIObjects(List<GameObject> _sourceUI)
    {
        foreach (GameObject _ui in _sourceUI)
        {
            GameObject.Destroy(_ui);
        }
        _sourceUI.Clear();
    }

    protected virtual void RefreshInventory(List<GameObject> _sourceUI, Inventory _sourceInventory, GameObject _itemSlotParent, bool _isPlayerInventory)
    {
        if (_sourceUI.Count > _sourceInventory.m_inventoryItems.Count)
        {
            RemoveUIObjects(_sourceUI);
        }

        //Creates a new slot ui if slot UI does not match items in inventory
        for (int i = _sourceUI.Count; i < _sourceInventory.m_inventoryItems.Count; i++)
        {
            CreateUISlotItem(_sourceUI, _itemSlotParent, i);
        }
        
        for (int i = 0; i < _sourceUI.Count; i++)
        {
            _sourceUI[i].GetComponentsInChildren<Image>()[1].sprite = _sourceInventory.m_inventoryItems[i].m_itemSprite;
        }
    }

    protected virtual void CreateUISlotItem(List<GameObject> _sourceUI, GameObject _itemSlotParent, int _slotItemIndex = -1)
    {
        GameObject m_temp = Instantiate(m_itemSlotPrefab) as GameObject;
        if (_slotItemIndex != -1)
        {
            m_temp.GetComponent<DropableUI>().m_itemSlotNum = _slotItemIndex;
        } else
        {
            m_temp.GetComponent<DropableUI>().m_itemSlotNum = _sourceUI.Count;
        }
        m_temp.transform.SetParent(_itemSlotParent.transform, false);
        _sourceUI.Add(m_temp);
        InitializeEvents(m_temp, _sourceUI.Count - 1, true);
    }

    protected virtual void Update()
    {
        if (m_isInputsEnabled)
        {
            if (Input.touchCount > 0 || Input.GetMouseButton(0))
            {
                if (Input.touchCount > 0)
                {
                    m_inputPosition = Input.GetTouch(0).position;
                }
                else if (Input.GetMouseButton(0))
                {
                    m_inputPosition = Input.mousePosition;
                }
                if (!m_popUpFlag)
                {
                    m_tapTime += Time.deltaTime;

                    if (m_tapTime >= 0.5f)
                    {
                        m_popUpFlag = true;
                        TogglePopUp(true);
                    }
                }
            }

            if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) || Input.GetMouseButtonUp(0))
            {
                TogglePopUp(false);
                m_popUpFlag = false;
                m_tapTime = 0;
            }
        }
    }

    protected virtual Vector2 RandomizePosition(Transform _itemSlotParent)
    {
        float m_maxX = (_itemSlotParent.GetComponent<RectTransform>().sizeDelta.magnitude * 2) - m_randomizePositionXOffset;
        float m_minX = -(_itemSlotParent.GetComponent<RectTransform>().sizeDelta.magnitude * 2) + m_randomizePositionXOffset;
        float m_value = Random.Range(m_minX, m_maxX);
        return new Vector2(m_value, m_itemSlotPrefab.transform.position.y);
    }


    protected virtual void TogglePopUp(bool _isEnable, string _text = "")
    {
        if (!_isEnable || m_chosenItemSlotNumber == -1)
        {
            m_popUp.SetActive(false);
            return;
        }
        
        InventoryItem m_item = m_playerInventory.GetItemFromSlot(m_chosenItemSlotNumber);
        m_popUp.transform.position = m_playerItemSlotUI[m_chosenItemSlotNumber].transform.position;

        if (m_item.m_id == 0)
        {
            return;
        }

        m_popUp.GetComponentInChildren<Text>().text = GenerateText(m_item);
        m_popUp.SetActive(true);

    }

    protected virtual string GenerateText(InventoryItem _item)
    {
        string m_toShow = "";

        m_toShow += _item.m_name + "\n";
        m_toShow += _item.m_description + "\n";

        foreach (Stats _stat in _item.m_itemStats)
        {
            int m_statValue = (int)_stat.m_statValue;
            m_toShow += Utility.SetUpperCaseFirstLetter(_stat.m_statName) + ": " + m_statValue + "\n";
        }

        return m_toShow;
    }

    protected virtual void InitializeContainerEvents()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drop;
        entry.callback.AddListener((data) => {
            ContainerOnDrop(true);
        });

        m_playerItemSlotParent.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    protected virtual void ContainerOnDrop(bool _isDropToPlayerContainer)
    {
        if (m_objectBeingDragged == null)
        {
            return;
        }
        m_objectBeingDragged.GetComponent<Image>().raycastTarget = true;

        //Debug.Log("_isDropToPlayerContainer: " + _isDropToPlayerContainer + " m_isChosenItemFromPlayerInventory: " + !m_isChosenItemFromPlayerInventory);
       /* if (_isDropToPlayerContainer && !m_isChosenItemFromPlayerInventory)
        {
            TransferParentUI(m_lootItemSlotUI[m_chosenItemSlotNumber], m_playerItemSlotParent);
            TransferInventoryItem(m_lootInventory, m_playerInventory);
            TransferUISlot(m_lootItemSlotUI, m_playerItemSlotUI);
            m_chosenItemSlotNumber = m_playerItemSlotUI.Count - 1;
            m_isChosenItemFromPlayerInventory = true;
            RefreshInventory();
            StartCoroutine(ReindexEventArraySlots(m_playerItemSlotUI));
        }*/
    }

    protected virtual IEnumerator ReindexEventArraySlots(List<GameObject> _sourceUI, bool _isPlayerInventory)
    {
        for (int i = 0; i < _sourceUI.Count; i++)
        {
            Destroy(_sourceUI[i].GetComponent<EventTrigger>());
            _sourceUI[i].AddComponent(typeof(EventTrigger));
            yield return new WaitForEndOfFrame();
            InitializeEvents(_sourceUI[i], i, _isPlayerInventory);
        }
    }

    protected void TransferParentUI(GameObject _objectToMove, GameObject _parentContainer)
    {
        m_oldParent = _parentContainer.transform; //We will have a hard time handling the functions call time on OnDrag vs OnDrop so lets set both to be sure
        _objectToMove.transform.SetParent(m_oldParent);
    }

    protected void TransferUISlot(List<GameObject> _source, List<GameObject> _destination)
    {
        GameObject _obj = _source[m_chosenItemSlotNumber];
        _destination.Add(_obj);
        _source.RemoveAt(m_chosenItemSlotNumber);
    }

    protected virtual void OnDown(PointerEventData data, GameObject _slotObj, int _slotNumber, bool _isPlayerInventory)
    {
        m_tapTime = 0;
        m_chosenItemSlotNumber = _slotNumber;
        m_isChosenItemFromPlayerInventory = _isPlayerInventory;
        m_objectBeingDragged = _slotObj;
        m_oldParent = m_objectBeingDragged.transform.parent;

        m_objectBeingDragged.GetComponent<Image>().raycastTarget = false;

        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 0;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;

        m_objectBeingDragged.transform.SetParent(m_objectBeingDragged.transform.parent.transform.parent);
        m_objectBeingDragged.transform.SetSiblingIndex(m_objectBeingDragged.transform.parent.childCount - 2);
    }

    protected virtual void OnPointerUp()
    {
        if (m_objectBeingDragged == null)
        {
            return;
        }

        m_objectBeingDragged.GetComponent<Image>().raycastTarget = true;
        m_objectBeingDragged.transform.SetParent(m_oldParent);
        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 1;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }

    protected virtual void OnBeginDrag(PointerEventData data, int _slotNumber)
    {
        if (m_objectBeingDragged == null)
        {
            return;
        }
        m_popUpFlag = true;
        TogglePopUp(false);

        m_objectBeingDragged.GetComponent<Image>().raycastTarget = false;
        m_objectBeingDragged.transform.SetParent(m_objectBeingDragged.transform.parent.transform.parent);
        m_objectBeingDragged.transform.SetSiblingIndex(m_objectBeingDragged.transform.parent.childCount - 2);
    }

    protected virtual void OnDrag(PointerEventData data, int _slotNumber)
    {
        if (m_objectBeingDragged == null)
        {
            return;
        }
        m_objectBeingDragged.transform.position = m_inputPosition;
    }

    protected virtual void OnDragEnd(PointerEventData data, int _slotNumber)
    {
        if(m_objectBeingDragged == null)
        {
            return;
        }
        m_objectBeingDragged.transform.SetParent(m_oldParent);

        m_objectBeingDragged.GetComponent<Image>().raycastTarget = true;

        m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 1;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

        m_objectBeingDragged.SetActive(true);
        m_eventSystem.SetSelectedGameObject(null);
    }

    public void OnDrop(PointerEventData data, int _slotNumber)
    {
       /* m_objectBeingDragged.GetComponent<Rigidbody2D>().gravityScale = 0;
        m_objectBeingDragged.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        m_objectBeingDragged.GetComponent<Image>().raycastTarget = true;
        m_eventSystem.SetSelectedGameObject(null);*/
    }

    protected virtual void InitializeEvents(GameObject _slotObj, int _slotNumber, bool _isPlayerInventory)
    {
        EventTrigger _trigger = _slotObj.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => {
            OnDown((PointerEventData)data, _slotObj, _slotNumber, _isPlayerInventory);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((data) => {
            OnPointerUp();
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.BeginDrag;
        entry.callback.AddListener((data) => {
            OnBeginDrag((PointerEventData)data, _slotNumber);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drag;
        entry.callback.AddListener((data) => {
            OnDrag((PointerEventData)data, _slotNumber);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.EndDrag;
        entry.callback.AddListener((data) => {
            OnDragEnd((PointerEventData)data, _slotNumber);
        });
        _trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.Drop;
        entry.callback.AddListener((data) => {
            OnDrop((PointerEventData)data, _slotNumber);
        });
        _trigger.triggers.Add(entry);

    }
}
