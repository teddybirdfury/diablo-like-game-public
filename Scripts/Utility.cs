﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Utility {

    public static bool HasComponent<T>(this GameObject flag) where T : Component
    {
        return flag.GetComponent<T>() != null;
    }

    public static bool IsDistanceClose(Vector3 _positionStart, Vector3 _positionEnd, float _distanceToInteract)
    {
        Vector3 m_modifiedPositionStart = new Vector3(_positionStart.x, 0, _positionStart.z);
        Vector3 m_modifiedPositionEnd = new Vector3(_positionEnd.x, 0, _positionEnd.z);
        return (Vector3.Distance(m_modifiedPositionStart, m_modifiedPositionEnd) < _distanceToInteract);
    }

    public static float AngleTo(Vector2 _from, Vector2 _to, float _cameraRotation = 0)
    {
        Vector2 direction = _to - _from;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        angle -= 90;
        if (angle < 0)
        {
            angle = 360 + angle;
        }
        angle = 360 - angle;
        angle += _cameraRotation;
        return angle;
    }
    public static string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }

    public static string SetUpperCaseFirstLetter(string _statName)
    {
        return Utility.UppercaseFirst(_statName.ToLower());
    }
}
